require('dotenv').config();

const path = require('path');
const fs = require('fs');
const express = require('express');
const compression = require('compression');
const favicon = require('serve-favicon');
const helmet = require('helmet');
const cookieParser = require('cookie-parser');
const { createBundleRenderer } = require('vue-server-renderer');

const isProduction = process.env.NODE_ENV === 'production';

// TODO: cache
// https://github.com/vuejs/vue-hackernews-2.0/commit/6c70f41f49946ae67bd8fc201637cb26fa2c00e9
// https://ssr.vuejs.org/ru/guide/caching.html

const resolve = file => path.resolve(__dirname, file);

function createRenderer(bundle, options) {
  return createBundleRenderer(bundle, Object.assign(options, {
    basedir: resolve('./dist'),
    runInNewContext: false,
  }));
}

const server = express();

let renderer;
let readyPromise;
const templatePath = resolve('src/index.template.html');

if (isProduction) {
  const serverBundle = require('./dist/vue-ssr-server-bundle.json');
  const clientManifest = require('./dist/vue-ssr-client-manifest.json');
  const template = fs.readFileSync(templatePath, 'utf-8');
  renderer = createRenderer(serverBundle, { template, clientManifest });
} else {
  readyPromise = require('./build/setup-dev-server')(server, templatePath, (bundle, options) => {
    renderer = createRenderer(bundle, options);
  });
}

function render(req, res) {
  res.setHeader('Content-Type', 'text/html');

  const langs = process.env.ACCEPTED_LANGS.split(',');
  const lang = req.cookies.lang || req.acceptsLanguages(...langs) || langs[0];
  res.cookie('lang', lang, { expires: new Date(Date.now() + (1000 * 60 * 60 * 24 * 365)) });

  const context = {
    url: req.url,
    cookies: req.cookies,
    lang,
    state: {},
    error: null,
    req,
    res,
  };

  renderer.renderToString(context, (err, html) => {
    if (err) {
      // TODO: render error page
      // TODO: write error log, notify monitoring system
      console.error(`error during render : ${req.url}`);
      console.error(err.stack);
      return res.status(500).send('500 | Internal Server Error');
    }

    const error = context.state.error;
    return res.status(error ? error.statusCode : 200).send(html);
  });
}

const serve = (path, cache) => express.static(resolve(path), {
  maxAge: cache && isProduction ? 1000 * 60 * 60 * 24 * 30 : 0,
});

server.use(helmet());
server.use(cookieParser());
server.use(compression({ threshold: 0 }));
server.use(favicon('public/favicons/favicon.ico'));
server.use('/public', serve('./public', true));
server.use('/static', serve('./dist/static', true));

server.get('*', isProduction ? render : (req, res) => {
  readyPromise.then(() => render(req, res));
});

const port = process.env.PORT || 8080;

server.listen(port, () => {
  console.log(`Server started at http://localhost:${port}`);
});
