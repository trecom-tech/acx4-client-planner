/**
 * Webpack config for SSR mode - client
 */

const merge = require('webpack-merge');
const VueSSRClientPlugin = require('vue-server-renderer/client-plugin');

const baseClientConfig = require('./webpack.base.client.conf');

const config = merge(baseClientConfig, {
  plugins: [
    new VueSSRClientPlugin(),
  ],
});

module.exports = config;
