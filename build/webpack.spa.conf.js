/**
 * Webpack config for SPA mode
 */

const path = require('path');
const merge = require('webpack-merge');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const baseClientConfig = require('./webpack.base.client.conf');
const env = require('./env');
const utils = require('./utils');

const isProduction = process.env.NODE_ENV === 'production';

const config = merge(baseClientConfig, {
  devServer: {
    clientLogLevel: 'warning',
    historyApiFallback: {
      rewrites: [
        {
          from: /.*/,
          to: path.posix.join('/', 'index.html'),
        },
      ],
    },
    hot: true,
    contentBase: false, // since we use CopyWebpackPlugin.
    compress: true,
    host: '0.0.0.0',
    port: env.PORT,
    open: false,
    overlay: { warnings: false, errors: true },
    publicPath: '/',
    quiet: true, // necessary for FriendlyErrorsPlugin
    watchOptions: {
      poll: false,
    },
  },
  plugins: [
    new CopyWebpackPlugin([
      {
        from: utils.resolve('public'),
        to: utils.resolve('dist/public'),
        ignore: ['.*'],
      },
    ]),
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: utils.resolve('src/index.html'),
      inject: true,
      chunksSortMode: 'dependency',
      minify: isProduction
        ? {
          removeComments: true,
          collapseWhitespace: true,
          removeAttributeQuotes: true,
        }
        : false,
    }),
  ],
});

module.exports = config;
