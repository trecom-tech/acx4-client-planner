const chalk = require('chalk');
const dotenv = require('dotenv');

// Load environment variables from .env
dotenv.config();

const schema = {
  PORT: {
    default: 8081,
  },
  HTTPS: {
    default: false,
  },
  ROUTER_BASE: {
    default: '/',
  },
  API_URL: {
    required: true,
  },
  MAPBOX_ACCESS_TOKEN: {
    required: true,
  },
  GOOGLE_MAPS_API_KEY: {
    required: true,
  },
};
const env = {};
const missed = [];

Object.keys(schema).forEach((key) => {
  const isDefined = process.env.hasOwnProperty(key);
  if (!isDefined && schema[key].required) {
    missed.push(key);
    return;
  }
  env[key] = isDefined ? process.env[key] : schema[key].default;
});

if (missed.length) {
  const rule = chalk.grey('================================');
  const output = [
    chalk.red('Missing environment variables:'),
    rule,
  ];
  missed.forEach((key) => {
    output.push(`  ${chalk.blue(key)}`);
  });
  output.push(rule);
  console.log(output.join('\n'));
  process.exit(1);
}

module.exports = env;
