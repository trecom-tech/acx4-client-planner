/**
 * Base Webpack config for client
 */

const webpack = require('webpack');
const merge = require('webpack-merge');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const baseConfig = require('./webpack.base.conf');
const utils = require('./utils');

const isProduction = process.env.NODE_ENV === 'production';

const config = merge(baseConfig, {
  target: 'web',
  entry: {
    app: utils.resolve('src/entry-client.js'),
  },
  devtool: isProduction ? 'source-map' : 'cheap-module-source-map',
  plugins: [
    new webpack.DefinePlugin({
      'process.env.VUE_ENV': JSON.stringify('client'),
    }),
    new CopyWebpackPlugin([
      {
        from: utils.resolve('src/static'),
        to: utils.resolve('dist/static'),
        ignore: ['.*'],
      },
    ]),
  ],
  optimization: {
    splitChunks: {
      minChunks: 1,
      name: true,
      cacheGroups: {
        vendors: {
          name: 'vendor',
          test: /[\\/]node_modules[\\/]/,
          priority: -10,
          minChunks: 2,
        },
        common: {
          name: 'common',
          minChunks: 2,
          priority: -20,
          reuseExistingChunk: true
        },
      },
    },
  },
});

if (!isProduction) {
  config.plugins.push(new webpack.HotModuleReplacementPlugin());
}

module.exports = config;
