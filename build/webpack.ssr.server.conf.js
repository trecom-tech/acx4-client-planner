/**
 * Webpack config for SSR mode - server
 */

const webpack = require('webpack');
const merge = require('webpack-merge');
const VueSSRServerPlugin = require('vue-server-renderer/server-plugin');
const nodeExternals = require('webpack-node-externals');

const utils = require('./utils');
const baseConfig = require('./webpack.base.conf');

const config = merge(baseConfig, {
  target: 'node',
  devtool: 'source-map',
  entry: utils.resolve('src/entry-server.js'),
  output: {
    filename: 'server-bundle.js',
    libraryTarget: 'commonjs2',
  },
  externals: nodeExternals({
    whitelist: /\.css$/,
  }),
  plugins: [
    new webpack.DefinePlugin({
      'process.env.VUE_ENV': JSON.stringify('server'),
    }),
    new VueSSRServerPlugin(),
  ],
  optimization: {
    splitChunks: false,
  },
});

module.exports = config;
