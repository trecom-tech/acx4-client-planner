const { stringifyRequest } = require('loader-utils');
const { stringifySymbol, stringify } = require('svg-sprite-loader/lib/utils');

// https://github.com/kisenka/svg-sprite-loader/tree/master/examples/custom-runtime-generator

module.exports = function runtimeGenerator(options) {
  const {
    symbol,
    config,
    context,
  } = options;

  const { spriteModule, symbolModule, runtimeOptions } = config;

  const iconComponent = stringify(runtimeOptions.iconComponent);

  const spriteRequest = stringifyRequest({ context }, spriteModule);
  const symbolRequest = stringifyRequest({ context }, symbolModule);

  return `
    import SpriteSymbol from ${symbolRequest};
    import sprite from ${spriteRequest};
    import Icon from ${iconComponent};
    
    const symbol = new SpriteSymbol(${stringifySymbol(symbol)});
    sprite.add(symbol);
    
    export default {
      components: {
        Icon,
      },
      render(h) {
        return h('icon', {
          props: {
            name: symbol.id,
            viewBox: symbol.viewBox,
          },
        });
      },
    };
  `;
};
