/**
 * Webpack config for SPA bundle analyze
 */

const merge = require('webpack-merge');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

const spaConfig = require('./webpack.spa.conf');

const config = merge(spaConfig, {
  plugins: [
    new BundleAnalyzerPlugin({
      analyzerMode: 'static',
      generateStatsFile: false,
    }),
  ],
});

module.exports = config;
