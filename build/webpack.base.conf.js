/**
 * Base webpack config
 */

const webpack = require('webpack');
const FriendlyErrorsPlugin = require('friendly-errors-webpack-plugin');
const eslintFriendlyFormatter = require('eslint-friendly-formatter');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');

const ENV = require('./env');
const utils = require('./utils');

const isProduction = process.env.NODE_ENV === 'production';
const iconsPath = utils.resolve('src/assets/icons');

const config = {
  mode: isProduction ? 'production' : 'development',
  output: {
    path: utils.resolve('dist'),
    publicPath: '/',
    filename: utils.assetsPath('js/[name].[hash].js'),
  },
  module: {
    noParse: /^(vue|vue-router|vuex|vuex-router-sync)$/,
    rules: [
      {
        test: /\.(js|vue)$/,
        exclude: /node_modules/,
        loader: 'eslint-loader',
        options: {
          formatter: eslintFriendlyFormatter,
          emitWarning: true,
        },
        enforce: 'pre',
      },
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        options: {
          compilerOptions: {
            whitespace: 'condense',
          },
          transformAssetUrls: {
            video: ['src', 'poster'],
            source: 'src',
            img: 'src',
            image: 'xlink:href',
          },
        },
      },
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: file => (
          /node_modules/.test(file) &&
          !/\.vue\.js/.test(file)
        ),
      },
      {
        test: /\.s?css$/,
        use: [
          !isProduction ? 'vue-style-loader' : MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              sourceMap: !isProduction,
              importLoaders: 2,
            },
          },
          {
            loader: 'postcss-loader',
            options: {
              ident: 'postcss',
              plugins: [
                require('postcss-import')(),
                require('autoprefixer')(),
              ],
              sourceMap: !isProduction,
            },
          },
          {
            loader: 'sass-loader',
            options: {
              sourceMap: !isProduction,
            },
          },
          {
            loader: 'sass-resources-loader',
            options: {
              resources: [
                utils.resolve('src/assets/styles/_variables.scss'),
                utils.resolve('src/assets/styles/mixins/**/*.scss'),
              ],
            },
          },
        ],
      },
      {
        test: /\.svg$/,
        include: iconsPath,
        use: [
          'babel-loader',
          {
            loader: 'svg-sprite-loader',
            options: {
              symbolId: 'icon-[name]',
              runtimeGenerator: require.resolve('./svg-to-icon-component-runtime-generator'),
              runtimeOptions: {
                iconComponent: '@/core/components/Icon.vue',
              },
            },
          },
          'svgo-loader',
        ],
      },
      {
        test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
        exclude: iconsPath,
        loader: 'url-loader',
        options: {
          limit: 8192,
          name: utils.assetsPath('images/[name].[hash:7].[ext]'),
        },
      },
      {
        test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
        loader: 'url-loader',
        options: {
          limit: 8192,
          name: utils.assetsPath('fonts/[name].[hash:7].[ext]'),
        },
      },
      {
        test: /\.(mp4|webm|ogg|mp3|wav|flac|aac)(\?.*)?$/,
        loader: 'url-loader',
        options: {
          limit: 1,
          name: utils.assetsPath('media/[name].[hash:7].[ext]'),
        },
      },
    ],
  },
  resolve: {
    extensions: ['.js', '.vue', '.json'],
    alias: {
      '@': utils.resolve('src'),
      'vue$': 'vue/dist/vue.esm.js',
    },
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env': Object.assign(
        Object.keys(ENV).reduce((acc, key) => {
          acc[key] = JSON.stringify(ENV[key]);
          return acc;
        }, {}),
        {
          NODE_ENV: JSON.stringify(process.env.NODE_ENV || 'development'),
        },
      ),
    }),
    new VueLoaderPlugin(),
    new MiniCssExtractPlugin({
      filename: utils.assetsPath('css/[name].[contenthash].css'),
      chunkFilename: utils.assetsPath('css/[name].[contenthash].css'),
    }),
  ],
  performance: {
    hints: false,
  },
  node: {
    console: false,
    global: true,
    process: true,
    __filename: 'mock',
    __dirname: 'mock',
    Buffer: true,
    setImmediate: false,
    dgram: 'empty',
    net: 'empty',
    tls: 'empty',
    dns: 'mock',
    fs: 'empty',
    path: true,
    url: false,
    child_process: 'empty',
  },
  optimization: {
    noEmitOnErrors: isProduction,
    concatenateModules: isProduction,
    namedModules: !isProduction,
    minimize: isProduction,
    occurrenceOrder: isProduction,
  },
  stats: {
    colors: true,
    children: false,
  },
};

if (!isProduction) {
  config.plugins.push(new FriendlyErrorsPlugin({
    compilationSuccessInfo: {
      messages: [`Your application is running here: http://localhost:${ENV.PORT}`],
    },
  }));
} else {
  config.plugins.push(new webpack.HashedModuleIdsPlugin());
  config.plugins.push(new OptimizeCssAssetsPlugin({
    cssProcessor: require('cssnano'),
    cssProcessorOptions: {
      autoprefixer: false,
      discardComments: {
        removeAll: true,
      },
      colormin: false,
      convertValues: false,
      zindex: false,
      discardDuplicates: true,
      reduceIdents: false,
      mergeIdents: false,
    },
    canPrint: true,
  }));
}

module.exports = config;
