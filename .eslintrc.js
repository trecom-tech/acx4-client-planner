const isProduction = process.env.NODE_ENV === 'production';

module.exports = {
  root: true,
  parserOptions: {
    parser: 'babel-eslint',
    ecmaVersion: 2017,
    sourceType: 'module',
  },
  env: {
    browser: true,
  },
  extends: [
    'plugin:vue/recommended',
    'airbnb-base',
  ],
  plugins: [
    'vue',
  ],
  settings: {
    'import/resolver': {
      webpack: {
        config: 'build/webpack.base.conf.js',
      },
    },
  },
  rules: {
    'class-methods-use-this': 0,
    'comma-dangle': isProduction ? 2 : 1,
    'function-paren-newline': 0,
    'global-require': 0,
    'import/extensions': ['error', 'always', {
      'js': 'never',
    }],
    'import/prefer-default-export': 0,
    'indent': 'off',
    'linebreak-style': 0,
    'no-alert': 0,
    'no-bitwise': 0,
    'no-console': [
      isProduction ? 1 : 0,
      {
        allow: ['warn', 'error'],
      },
    ],
    'no-debugger': isProduction ? 1 : 0,
    'no-multiple-empty-lines': isProduction ? 2 : 1,
    'max-len': ['warn', 100, 1, {
      ignoreUrls: true,
      ignoreComments: false,
      ignoreRegExpLiterals: true,
      ignoreStrings: true,
      ignoreTemplateLiterals: true,
    }],
    'no-new': 0,
    'no-param-reassign': 0,
    'no-plusplus': 0,
    'no-shadow': 0,
    'no-trailing-spaces': isProduction ? 2 : 1,
    'no-underscore-dangle': 0,
    'no-unused-vars': isProduction ? 2 : 1,
    'operator-linebreak': 0,
    'padded-blocks': 1,
    'prefer-destructuring': 0,
    'semi': isProduction ? 2 : 1,
    'vue/html-closing-bracket-spacing': 0,
    'vue/no-v-html': 0,
    'vue/singleline-html-element-content-newline': 0,
  },
  globals: {
    window: {},
    document: {},
    navigator: {},
  },
};

