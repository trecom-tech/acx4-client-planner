# WARNING! Project moved to vasanda-ui, check components update before reuse parts!

# Vasanda UI

## Requirements

[Node.js](nodejs.org) 7.9.0+ with [NPM](https://www.npmjs.com/get-npm)

> Best way to install Node.js - using [NVM](https://github.com/creationix/nvm) ([nvm-windows](https://github.com/coreybutler/nvm-windows) for Windows users). It allows to have multiple version of Node.js together and switch between it.

## Install

Run `npm i` in project root directory at initial setup (and after each `package.json` update).


> If you using IDE like WebStorm make sure that directories `node_modules` and `dist` marked as excluded to avoid high CPU usage.

 Create file `.env` in root directory and fill it with environment variables (like in `.env.example`).

 > Please, use [EditorConfig](https://editorconfig.org/) plugin for your IDE to follow code style.

## Run project locally 

Project allows to use 2 modes: SPA (only client-side) and Universal (server-side rendering + client-side).

### SPA mode

`npm run dev` - run development version with webpack web server and modules reloading

`npm run build` - build production distributive

### Universal mode

`npm run dev:ssr` - run development version with local express.js web server

`npm run build:ssr` - build production client and server bundles

### Analyze bundle

`npm run analyze-bundle` - build production distributive and analyze bundle size

## Deployment

### SPA mode

For SPA mode just upload `dist` folder on the server and setup you web server to return `index.html` for all requests.

### Universal mode

TODO: write about isomorphic app deployment, add Nginx config examples.

## Development

### Project structure

### Icons

Icons need to be placed in `src/assets/icons` directory.

Export and use each icon as component. There is special runtime generator that transforms svg to Vue-component.

Prefer to use icons from [Material Icons](https://material.io/tools/icons/) (keep original icon names).

### Backend API

[Documentation](http://developer.axioncore.com/)
