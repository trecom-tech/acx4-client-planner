import afterRegistration from './hooks/afterRegistration';
import beforeRegistration from './hooks/beforeRegistration';
import afterEach from './router/afterEach';
import beforeEach from './router/beforeEach';
import routes from './router/routes';
import store from './store';

export default {
  name: 'example',
  afterRegistration,
  beforeRegistration,
  router: {
    routes,
    afterEach,
    beforeEach,
  },
  store,
};
