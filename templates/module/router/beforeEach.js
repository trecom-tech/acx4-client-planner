export default function beforeEach(to, from, next) {
  console.log(`Entering ${to.name} from ${from.name}.`);
  next();
}
