export default function afterEach(to, from) {
  console.log(`Entered ${to.name} from ${from.name}.`);
}
