import { exampleGuard } from './guards';

const Index = () => import(/* webpackChunkName: "modules/example" */ '@/modules/warehouse/pages/Index.vue');

const NAMESPACE = 'Example';

export default [
  {
    name: NAMESPACE,
    path: '/example',
    meta: {
      guard: [exampleGuard],
      asideNav: [],
    },
    component: Index,
  },
];
