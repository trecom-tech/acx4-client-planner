import store from './store';

export default {
  name: 'ui',
  store: {
    module: store,
  },
};
