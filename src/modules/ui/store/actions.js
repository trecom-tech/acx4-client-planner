import { getPathValue } from 'pathval';

import * as types from './mutation-types';

export default {
  setProperty({ state, commit }, { path, value }) {
    if (getPathValue(state, path) === undefined) return;

    commit(types.UI_SET_PROPERTY, { path, value });
  },
};
