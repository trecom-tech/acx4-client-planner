import { getPathValue } from 'pathval';

export default {
  getProperty: state => path => getPathValue(state, path),
};
