import mutations from './mutations';
import actions from './actions';
import getters from './getters';

export default {
  state: () => ({
    account: {
      invitations: {
        pageSize: 25,
      },
      users: {
        pageSize: 25,
      },
      warehouses: {
        pageSize: 25,
      },
    },
    hauler: {
      drivers: {
        pageSize: 25,
      },
      trucks: {
        pageSize: 25,
      },
    },
    logistic: {
      carriers: {
        pageSize: 25,
      },
      jobs: {
        pageSize: 25,
      },
      shipments: {
        pageSize: 25,
      },
    },
    purchasing: {
      orders: {
        pageSize: 25,
      },
      suppliers: {
        pageSize: 25,
      },
    },
    sales: {
      customers: {
        pageSize: 25,
      },
      orders: {
        pageSize: 25,
      },
      prices: {
        pageSize: 25,
      },
    },
    warehouse: {
      inbounds: {
        pageSize: 25,
      },
      loading: {
        pageSize: 25,
      },
      picks: {
        pageSize: 25,
      },
      products: {
        pageSize: 25,
      },
      putaways: {
        pageSize: 25,
      },
    },
  }),
  actions,
  getters,
  mutations,
  namespaced: true,
};
