import { setPathValue } from 'pathval';

import * as types from './mutation-types';

export default {
  [types.UI_SET_PROPERTY](state, { path, value }) {
    setPathValue(state, path, value);
  },
};
