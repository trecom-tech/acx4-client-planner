import routes from './router/routes';

export default {
  name: 'design',
  router: {
    routes,
  },
};
