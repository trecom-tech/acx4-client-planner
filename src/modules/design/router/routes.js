import IconCode from '@/assets/icons/material/code.svg';

const Index = () => import(/* webpackChunkName: "modules/design" */ '@/modules/design/pages/Index.vue');
const Identity = () => import(/* webpackChunkName: "modules/design" */ '@/modules/design/pages/Identity.vue');
const Badge = () => import(/* webpackChunkName: "modules/design" */ '@/modules/design/pages/Badge.vue');
const Notice = () => import(/* webpackChunkName: "modules/design" */ '@/modules/design/pages/Notice.vue');
const Barcode = () => import(/* webpackChunkName: "modules/design" */ '@/modules/design/pages/Barcode.vue');
const DataTable = () => import(/* webpackChunkName: "modules/design" */ '@/modules/design/pages/DataTable.vue');
const InputAddress = () => import(/* webpackChunkName: "modules/design" */ '@/modules/design/pages/InputAddress.vue');
const InputText = () => import(/* webpackChunkName: "modules/design" */ '@/modules/design/pages/InputText.vue');
const InputDateTime = () => import(/* webpackChunkName: "modules/design" */ '@/modules/design/pages/InputDateTime.vue');
const InputNumber = () => import(/* webpackChunkName: "modules/design" */ '@/modules/design/pages/InputNumber.vue');
const Checkbox = () => import(/* webpackChunkName: "modules/design" */ '@/modules/design/pages/Checkbox.vue');
const Radio = () => import(/* webpackChunkName: "modules/design" */ '@/modules/design/pages/Radio.vue');
const Multiselect = () => import(/* webpackChunkName: "modules/design" */ '@/modules/design/pages/Multiselect.vue');
const VButton = () => import(/* webpackChunkName: "modules/design" */ '@/modules/design/pages/VButton.vue');
const VLink = () => import(/* webpackChunkName: "modules/design" */ '@/modules/design/pages/VLink.vue');
const DropMenu = () => import(/* webpackChunkName: "modules/design" */ '@/modules/design/pages/DropMenu.vue');
const Tabs = () => import(/* webpackChunkName: "modules/design" */ '@/modules/design/pages/Tabs.vue');
const Tag = () => import(/* webpackChunkName: "modules/design" */ '@/modules/design/pages/Tag.vue');
const Paginator = () => import(/* webpackChunkName: "modules/design" */ '@/modules/design/pages/Paginator.vue');

const NAMESPACE = 'Design';

export default [
  {
    name: NAMESPACE,
    path: '/design',
    redirect: '/design/identity',
    meta: {
      asideNav: [
        {
          icon: IconCode,
          label: 'Identity',
          to: {
            name: `${NAMESPACE}/Identity`,
          },
        },
        {
          icon: IconCode,
          label: 'InputText',
          to: {
            name: `${NAMESPACE}/InputText`,
          },
        },
        {
          icon: IconCode,
          label: 'InputNumber',
          to: {
            name: `${NAMESPACE}/InputNumber`,
          },
        },
        {
          icon: IconCode,
          label: 'InputDateTime',
          to: {
            name: `${NAMESPACE}/InputDateTime`,
          },
        },
        {
          icon: IconCode,
          label: 'Input address',
          to: {
            name: `${NAMESPACE}/InputAddress`,
          },
        },
        {
          icon: IconCode,
          label: 'Multiselect',
          to: {
            name: `${NAMESPACE}/Multiselect`,
          },
        },
        {
          icon: IconCode,
          label: 'Checkbox',
          to: {
            name: `${NAMESPACE}/Checkbox`,
          },
        },
        {
          icon: IconCode,
          label: 'Radio',
          to: {
            name: `${NAMESPACE}/Radio`,
          },
        },
        {
          icon: IconCode,
          label: 'VLink',
          to: {
            name: `${NAMESPACE}/VLink`,
          },
        },
        {
          icon: IconCode,
          label: 'VButton',
          to: {
            name: `${NAMESPACE}/VButton`,
          },
        },
        {
          icon: IconCode,
          label: 'DataTable',
          to: {
            name: `${NAMESPACE}/DataTable`,
          },
        },
        {
          icon: IconCode,
          label: 'DropMenu',
          to: {
            name: `${NAMESPACE}/DropMenu`,
          },
        },
        {
          icon: IconCode,
          label: 'Badge',
          to: {
            name: `${NAMESPACE}/Badge`,
          },
        },
        {
          icon: IconCode,
          label: 'Tag',
          to: {
            name: `${NAMESPACE}/Tag`,
          },
        },
        {
          icon: IconCode,
          label: 'Notice',
          to: {
            name: `${NAMESPACE}/Notice`,
          },
        },
        {
          icon: IconCode,
          label: 'Barcode',
          to: {
            name: `${NAMESPACE}/Barcode`,
          },
        },
        {
          icon: IconCode,
          label: 'Tabs',
          to: {
            name: `${NAMESPACE}/Tabs`,
          },
        },
        {
          icon: IconCode,
          label: 'Paginator',
          to: {
            name: `${NAMESPACE}/Paginator`,
          },
        },
      ],
    },
    component: Index,
    children: [
      {
        name: `${NAMESPACE}/Identity`,
        path: 'identity',
        component: Identity,
      },
      {
        name: `${NAMESPACE}/Badge`,
        path: 'badge',
        component: Badge,
      },
      {
        name: `${NAMESPACE}/Notice`,
        path: 'notice',
        component: Notice,
      },
      {
        name: `${NAMESPACE}/Barcode`,
        path: 'barcode',
        component: Barcode,
      },
      {
        name: `${NAMESPACE}/DataTable`,
        path: 'dataTable',
        component: DataTable,
      },
      {
        name: `${NAMESPACE}/InputAddress`,
        path: 'input-address',
        component: InputAddress,
      },
      {
        name: `${NAMESPACE}/InputText`,
        path: 'input-text',
        component: InputText,
      },
      {
        name: `${NAMESPACE}/InputDateTime`,
        path: 'input-date-time',
        component: InputDateTime,
      },
      {
        name: `${NAMESPACE}/InputNumber`,
        path: 'input-number',
        component: InputNumber,
      },
      {
        name: `${NAMESPACE}/Checkbox`,
        path: 'checkbox',
        component: Checkbox,
      },
      {
        name: `${NAMESPACE}/Radio`,
        path: 'radio',
        component: Radio,
      },
      {
        name: `${NAMESPACE}/Multiselect`,
        path: 'multiselect',
        component: Multiselect,
      },
      {
        name: `${NAMESPACE}/VButton`,
        path: 'vbutton',
        component: VButton,
      },
      {
        name: `${NAMESPACE}/VLink`,
        path: 'vlink',
        component: VLink,
      },
      {
        name: `${NAMESPACE}/Tabs`,
        path: 'tabs',
        component: Tabs,
      },
      {
        name: `${NAMESPACE}/DropMenu`,
        path: 'drop-menu',
        component: DropMenu,
      },
      {
        name: `${NAMESPACE}/Tag`,
        path: 'tag',
        component: Tag,
      },
      {
        name: `${NAMESPACE}/Paginator`,
        path: 'paginator',
        component: Paginator,
      },
    ],
  },
];
