import routes from './router/routes';

export default {
  name: 'dashboard',
  router: {
    routes,
  },
};
