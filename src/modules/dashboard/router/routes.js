import { authGuard } from '@/core/router/guards';

const Index = () => import(/* webpackChunkName: "modules/dashboard" */ '@/modules/dashboard/pages/Index.vue');

const NAMESPACE = 'Dashboard';

export default [
  {
    name: NAMESPACE,
    path: '/dashboard',
    component: Index,
    meta: {
      guard: authGuard,
      asideNav: [],
    },
    children: [],
  },
];
