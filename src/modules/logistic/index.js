import routes from './router/routes';

export default {
  name: 'logistic',
  router: {
    routes,
  },
};
