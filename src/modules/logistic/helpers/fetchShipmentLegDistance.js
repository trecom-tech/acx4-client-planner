import axios from 'axios';

import UOM from '@/core/constants/uom';

export default function fetchShipmentLegDistance(leg) {
  return new Promise((resolve, reject) => {
    if (!leg.startLocation.address.verified || !leg.startLocation.address.verified) {
      reject(new Error('Unverified leg address'));
      return;
    }

    const coords = ['startLocation', 'endLocation'].map((key) => {
      const { lng, lat } = leg[key].address.location;
      return [lng, lat].join(',');
    }).join(';');

    axios.get(`https://api.mapbox.com/directions/v5/mapbox/driving/${coords}`, {
      params: {
        access_token: process.env.MAPBOX_ACCESS_TOKEN,
      },
    })
      .then(({ data }) => {
        if (data.code !== 'Ok' && !data.routes[0]) {
          reject(new Error('Invalid Mapbox response'));
          return;
        }

        resolve({
          value: data.routes[0].distance,
          uom: UOM.METER,
        });
      })
      .catch((err) => {
        reject(err);
      });
  });
}
