import { authGuard } from '@/core/router/guards';
import { warehouseGuard } from '@/modules/warehouse/router/guards';

import IconCarrier from '@/assets/icons/material/local_shipping.svg';
import IconGroup from '@/assets/icons/material/group.svg';
import IconMap from '@/assets/icons/material/map.svg';
import IconShipment from '@/assets/icons/shipment.svg';

const Index = () => import(/* webpackChunkName: "modules/logistic" */ '@/modules/logistic/pages/Index.vue');
const CarriersIndex = () => import(/* webpackChunkName: "modules/logistic" */ '@/modules/logistic/pages/carriers/Index.vue');
const CarriersList = () => import(/* webpackChunkName: "modules/logistic" */ '@/modules/logistic/pages/carriers/List.vue');
const CarriersCreate = () => import(/* webpackChunkName: "modules/logistic" */ '@/modules/logistic/pages/carriers/Create.vue');
const CarriersEdit = () => import(/* webpackChunkName: "modules/logistic" */ '@/modules/logistic/pages/carriers/Edit.vue');
const JobsIndex = () => import(/* webpackChunkName: "modules/logistic" */ '@/modules/logistic/pages/jobs/Index.vue');
const JobsList = () => import(/* webpackChunkName: "modules/logistic" */ '@/modules/logistic/pages/jobs/List.vue');
const JobsCreate = () => import(/* webpackChunkName: "modules/logistic" */ '@/modules/logistic/pages/jobs/Create.vue');
const JobsEdit = () => import(/* webpackChunkName: "modules/logistic" */ '@/modules/logistic/pages/jobs/Edit.vue');
const ShipmentsIndex = () => import(/* webpackChunkName: "modules/logistic" */ '@/modules/logistic/pages/shipments/Index.vue');
const ShipmentsList = () => import(/* webpackChunkName: "modules/logistic" */ '@/modules/logistic/pages/shipments/List.vue');
const ShipmentsCreate = () => import(/* webpackChunkName: "modules/logistic" */ '@/modules/logistic/pages/shipments/Create.vue');
const ShipmentsEdit = () => import(/* webpackChunkName: "modules/logistic" */ '@/modules/logistic/pages/shipments/Edit.vue');
const RoutePlanner = () => import(/* webpackChunkName: "modules/logistic" */ '@/modules/logistic/pages/RoutePlanner.vue');

const NAMESPACE = 'Logistic';

export default [
  {
    name: NAMESPACE,
    path: '/logistic',
    redirect: '/logistic/shipments',
    component: Index,
    meta: {
      guard: [authGuard, warehouseGuard],
      asideNav: [
        {
          icon: IconShipment,
          label: 'Shipments',
          to: {
            name: `${NAMESPACE}/Shipments/List`,
          },
        },
        {
          icon: IconMap,
          label: 'Planner',
          to: {
            name: `${NAMESPACE}/Route/Planner`,
          },
        },
        {
          icon: IconCarrier,
          label: 'Jobs',
          to: {
            name: `${NAMESPACE}/Jobs/List`,
          },
        },
        {
          icon: IconGroup,
          label: 'Carriers',
          to: {
            name: `${NAMESPACE}/Carriers/List`,
          },
        },
      ],
    },
    children: [
      {
        path: 'carriers',
        component: CarriersIndex,
        children: [
          {
            name: `${NAMESPACE}/Carriers/Create`,
            path: 'new',
            component: CarriersCreate,
          },
          {
            name: `${NAMESPACE}/Carriers/Edit`,
            path: ':carrierId/edit',
            component: CarriersEdit,
          },
          {
            name: `${NAMESPACE}/Carriers/List`,
            path: '',
            component: CarriersList,
          },
        ],
      },
      {
        path: 'jobs',
        component: JobsIndex,
        children: [
          {
            name: `${NAMESPACE}/Jobs/Create`,
            path: 'new',
            component: JobsCreate,
          },
          {
            name: `${NAMESPACE}/Jobs/Edit`,
            path: ':jobId/edit',
            component: JobsEdit,
          },
          {
            name: `${NAMESPACE}/Jobs/List`,
            path: '',
            component: JobsList,
          },
        ],
      },
      {
        path: 'shipments',
        component: ShipmentsIndex,
        children: [
          {
            name: `${NAMESPACE}/Shipments/Create`,
            path: 'new',
            component: ShipmentsCreate,
          },
          {
            name: `${NAMESPACE}/Shipments/Edit`,
            path: ':shipmentId/edit',
            component: ShipmentsEdit,
          },
          {
            name: `${NAMESPACE}/Shipments/List`,
            path: '',
            component: ShipmentsList,
          },
        ],
      },
      {
        name: `${NAMESPACE}/Route/Planner`,
        path: 'planner',
        component: RoutePlanner,
      },
    ],
  },
];
