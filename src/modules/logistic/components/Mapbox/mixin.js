export default {
  inject: [
    'getMap',
    'getMapboxgl',
  ],
  data() {
    return {
      map: null,
      mapEventBus: null,
    };
  },
  mounted() {
    this.map = this.getMap();
    this.mapboxgl = this.getMapboxgl();
  },
};
