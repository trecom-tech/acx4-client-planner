import Map from './Map.vue';
import MapPanel from './MapPanel.vue';

export {
  Map,
  MapPanel,
};
