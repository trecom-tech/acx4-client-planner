import CURRENCIES from '@/core/constants/currencies';
import { STATUSES } from '@/modules/logistic/constants/job';

export default {
  id: '',
  accountId: '',
  status: STATUSES.PENDING,
  number: '',
  date: '',
  warehouse: null,
  shipments: [],
  route: [],
  carrier: null,
  size: null,
  mapData: {
    source: 'MAPBOX',
    route: [],
    distance: null,
    time: null,
  },
  costs: {
    estimate: {
      value: 0,
      currency: CURRENCIES.USD,
    },
    quote: {
      value: 0,
      currency: CURRENCIES.USD,
    },
    actual: {
      value: 0,
      currency: CURRENCIES.USD,
    },
  },
};
