import markerDefault from '@/assets/images/map/marker/default.svg';
import markerDefaultBlue from '@/assets/images/map/marker/default-blue.svg';
import markerDefaultGray from '@/assets/images/map/marker/default-gray.svg';
import markerDefaultRed from '@/assets/images/map/marker/default-red.svg';
import markerComplete from '@/assets/images/map/marker/complete.svg';
import markerCompleteBlue from '@/assets/images/map/marker/complete-blue.svg';
import markerCompleteGray from '@/assets/images/map/marker/complete-gray.svg';
import markerBox from '@/assets/images/map/marker/box.svg';
import markerBoxBlue from '@/assets/images/map/marker/box-blue.svg';
import markerBoxGray from '@/assets/images/map/marker/box-gray.svg';

import clusterBlue from '@/assets/images/map/cluster/blue.svg';
import clusterDefault from '@/assets/images/map/cluster/default.svg';
import clusterDefaultGray from '@/assets/images/map/cluster/default-gray.svg';
import clusterGray from '@/assets/images/map/cluster/gray.svg';
import clusterRed from '@/assets/images/map/cluster/red.svg';
import clusterRedDefault from '@/assets/images/map/cluster/red-default.svg';
import clusterRedGray from '@/assets/images/map/cluster/red-gray.svg';
import clusterRedDefaultGray from '@/assets/images/map/cluster/red-default-gray.svg';

export default {
  markerDefault,
  markerDefaultBlue,
  markerDefaultGray,
  markerDefaultRed,
  markerComplete,
  markerCompleteBlue,
  markerCompleteGray,
  markerBox,
  markerBoxBlue,
  markerBoxGray,
  clusterBlue,
  clusterDefault,
  clusterDefaultGray,
  clusterGray,
  clusterRed,
  clusterRedDefault,
  clusterRedGray,
  clusterRedDefaultGray,
};
