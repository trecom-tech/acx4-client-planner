import URI from 'urijs';

import RoutePlanner from '@/modules/logistic/components/RoutePlanner';
import pageWithForm from '@/core/mixins/pageWithForm';
import { isNumber } from '@/core/helpers/utils';

export default {
  components: {
    RoutePlanner,
  },
  mixins: [pageWithForm],
  data() {
    return {
      center: null,
      zoom: null,
      scheduledFrom: null,
      scheduledTo: null,
      onlyPending: null,
    };
  },
  beforeMount() {
    this.parsePlannerUrl();
  },
  methods: {
    onPlannerMapState({ center, zoom }) {
      if (this.center === center && this.zoom === zoom) return;

      this.center = center;
      this.zoom = zoom;
      this.updatePlannerUrl();
    },
    onPlannerLegsFilterState(state) {
      const {
        scheduledFrom,
        scheduledTo,
        onlyPending,
      } = state;

      if (
        this.scheduledFrom !== scheduledFrom
        && this.scheduledTo !== scheduledTo
        && this.onlyPending !== onlyPending
      ) return;

      this.scheduledFrom = scheduledFrom;
      this.scheduledTo = scheduledTo;
      this.onlyPending = onlyPending;

      this.updatePlannerUrl();
    },
    onPlannerDirty(dirty) {
      this.dirty = dirty;
    },
    parsePlannerUrl() {
      let center = this.$route.query.center;
      let zoom = this.$route.query.zoom;
      const onlyPending = this.$route.query.onlyPending;

      ['scheduledFrom', 'scheduledTo'].forEach((key) => {
        const timestamp = parseInt(this.$route.query[key], 10);
        if (
          !isNumber(timestamp)
          || !isNumber(new Date(timestamp).getTime())
        ) {
          return;
        }

        this[key] = timestamp;
      });

      this.onlyPending = onlyPending !== 'false';

      if (center) {
        center = center.split(',');

        const lat = parseFloat(center[0]);
        const lng = parseFloat(center[1]);

        if (isNumber(lat) && isNumber(lng)) {
          this.center = { lat, lng };
        }
      }

      if (zoom) {
        zoom = parseFloat(zoom);

        if (isNumber(zoom)) {
          this.zoom = zoom;
        }
      }
    },
    updatePlannerUrl() {
      const uri = new URI(this.$route.fullPath);
      const query = uri.search(true);

      query.center = [this.center.lat, this.center.lng].join(',');
      query.zoom = this.zoom;
      query.scheduledFrom = this.scheduledFrom || undefined;
      query.scheduledTo = this.scheduledTo || undefined;
      query.onlyPending = this.onlyPending;

      uri.search(query);

      window.history.replaceState({}, document.title, uri.toString());
    },
  },
};
