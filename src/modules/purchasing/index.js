import routes from './router/routes';

export default {
  name: 'purchasing',
  router: {
    routes,
  },
};
