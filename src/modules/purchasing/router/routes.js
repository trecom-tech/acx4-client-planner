import { authGuard } from '@/core/router/guards';
import { warehouseGuard } from '@/modules/warehouse/router/guards';

import IconDoc from '@/assets/icons/material/insert_drive_file.svg';
import IconGroup from '@/assets/icons/material/group.svg';

const Index = () => import(/* webpackChunkName: "modules/purchasing" */ '@/modules/purchasing/pages/Index.vue');
const SuppliersIndex = () => import(/* webpackChunkName: "modules/purchasing" */ '@/modules/purchasing/pages/suppliers/Index.vue');
const SuppliersList = () => import(/* webpackChunkName: "modules/purchasing" */ '@/modules/purchasing/pages/suppliers/List.vue');
const SuppliersCreate = () => import(/* webpackChunkName: "modules/purchasing" */ '@/modules/purchasing/pages/suppliers/Create.vue');
const SuppliersEdit = () => import(/* webpackChunkName: "modules/purchasing" */ '@/modules/purchasing/pages/suppliers/Edit.vue');
const OrdersIndex = () => import(/* webpackChunkName: "modules/purchasing" */ '@/modules/purchasing/pages/orders/Index.vue');
const OrdersList = () => import(/* webpackChunkName: "modules/purchasing" */ '@/modules/purchasing/pages/orders/List.vue');
const OrdersCreate = () => import(/* webpackChunkName: "modules/purchasing" */ '@/modules/purchasing/pages/orders/Create.vue');
const OrdersEdit = () => import(/* webpackChunkName: "modules/purchasing" */ '@/modules/purchasing/pages/orders/Edit.vue');

const NAMESPACE = 'Purchasing';

export default [
  {
    name: NAMESPACE,
    path: '/purchasing',
    redirect: '/purchasing/orders',
    meta: {
      guard: [authGuard, warehouseGuard],
      asideNav: [
        {
          icon: IconDoc,
          label: 'Orders',
          to: {
            name: `${NAMESPACE}/Orders/List`,
          },
        },
        {
          icon: IconGroup,
          label: 'Suppliers',
          to: {
            name: `${NAMESPACE}/Suppliers/List`,
          },
        },
      ],
    },
    component: Index,
    children: [
      {
        path: 'suppliers',
        component: SuppliersIndex,
        children: [
          {
            name: `${NAMESPACE}/Suppliers/Create`,
            path: 'new',
            component: SuppliersCreate,
          },
          {
            name: `${NAMESPACE}/Suppliers/Edit`,
            path: ':supplierId/edit',
            component: SuppliersEdit,
          },
          {
            name: `${NAMESPACE}/Suppliers/List`,
            path: '',
            component: SuppliersList,
          },
        ],
      },
      {
        path: 'orders',
        component: OrdersIndex,
        children: [
          {
            name: `${NAMESPACE}/Orders/Create`,
            path: 'new',
            component: OrdersCreate,
          },
          {
            name: `${NAMESPACE}/Orders/Edit`,
            path: ':orderId/edit',
            component: OrdersEdit,
          },
          {
            name: `${NAMESPACE}/Orders/List`,
            path: '',
            component: OrdersList,
          },
        ],
      },
    ],
  },
];
