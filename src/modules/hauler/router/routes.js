import { authGuard } from '@/core/router/guards';
import { warehouseGuard } from '@/modules/warehouse/router/guards';

import IconShipping from '@/assets/icons/material/local_shipping.svg';
import IconPerson from '@/assets/icons/material/person.svg';

const Index = () => import(/* webpackChunkName: "modules/hauler" */ '@/modules/hauler/pages/Index.vue');
const DriversIndex = () => import(/* webpackChunkName: "modules/hauler" */ '@/modules/hauler/pages/drivers/Index.vue');
const DriversList = () => import(/* webpackChunkName: "modules/hauler" */ '@/modules/hauler/pages/drivers/List.vue');
const DriversCreate = () => import(/* webpackChunkName: "modules/hauler" */ '@/modules/hauler/pages/drivers/Create.vue');
const DriversEdit = () => import(/* webpackChunkName: "modules/hauler" */ '@/modules/hauler/pages/drivers/Edit.vue');
const TrucksIndex = () => import(/* webpackChunkName: "modules/hauler" */ '@/modules/hauler/pages/trucks/Index.vue');
const TrucksList = () => import(/* webpackChunkName: "modules/hauler" */ '@/modules/hauler/pages/trucks/List.vue');
const TrucksCreate = () => import(/* webpackChunkName: "modules/hauler" */ '@/modules/hauler/pages/trucks/Create.vue');
const TrucksEdit = () => import(/* webpackChunkName: "modules/hauler" */ '@/modules/hauler/pages/trucks/Edit.vue');

const NAMESPACE = 'Hauler';

export default [
  {
    name: NAMESPACE,
    path: '/hauler',
    redirect: '/hauler/drivers',
    meta: {
      guard: [authGuard, warehouseGuard],
      asideNav: [
        {
          icon: IconPerson,
          label: 'Drivers',
          to: {
            name: `${NAMESPACE}/Drivers/List`,
          },
        },
        {
          icon: IconShipping,
          label: 'Trucks',
          to: {
            name: `${NAMESPACE}/Trucks/List`,
          },
        },
      ],
    },
    component: Index,
    children: [
      {
        path: 'drivers',
        component: DriversIndex,
        children: [
          {
            name: `${NAMESPACE}/Drivers/Create`,
            path: 'new',
            component: DriversCreate,
          },
          {
            name: `${NAMESPACE}/Drivers/Edit`,
            path: ':driverId/edit',
            component: DriversEdit,
          },
          {
            name: `${NAMESPACE}/Drivers/List`,
            path: '',
            component: DriversList,
          },
        ],
      },
      {
        path: 'trucks',
        component: TrucksIndex,
        children: [
          {
            name: `${NAMESPACE}/Trucks/Create`,
            path: 'new',
            component: TrucksCreate,
          },
          {
            name: `${NAMESPACE}/Trucks/Edit`,
            path: ':truckId/edit',
            component: TrucksEdit,
          },
          {
            name: `${NAMESPACE}/Trucks/List`,
            path: '',
            component: TrucksList,
          },
        ],
      },
    ],
  },
];
