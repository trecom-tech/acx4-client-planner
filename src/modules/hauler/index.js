import routes from './router/routes';

export default {
  name: 'hauler',
  router: {
    routes,
  },
};
