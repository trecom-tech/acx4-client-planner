export const TYPES = {
  ZONE: 'ZONE',
  RACK: 'RACK',
  PALLET: 'PALLET',
  DOCK: 'DOCK',
  BIN: 'BIN',
};
