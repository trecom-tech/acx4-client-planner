import routes from './router/routes';
import store from './store';

export default {
  name: 'warehouse',
  router: {
    routes,
  },
  store: {
    module: store,
  },
};
