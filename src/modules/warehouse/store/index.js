import mutations from './mutations';
import actions from './actions';
import getters from './getters';

export default {
  state: () => ({
    items: [],
    activeId: '',
    loading: false,
    fetchedAt: 0,
  }),
  actions,
  getters,
  mutations,
  namespaced: true,
};
