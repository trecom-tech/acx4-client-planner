export default {
  active(state) {
    return state.activeId
      ? state.items.find(item => item.id === state.activeId)
      : null;
  },
  items(state) {
    return state.items;
  },
  getById: state => id => state.items.find(item => item.id === id),
};
