import createPromisedAction from '@/core/store/helpers/createPromisedAction';
import * as types from './mutation-types';

const FETCH_TIMEOUT = 1000 * 60; // 1 min

function fixActiveWarehouse({ state, commit }) {
  const active = (
    (state.activeId && state.items.find(item => item.id === state.activeId))
    || state.items[0]
  );

  commit(types.SET_ACTIVE_WAREHOUSE, active ? active.id : null);
}

export default {
  fetchList: createPromisedAction({
    action({ state, commit }, force) {
      // Fetch warehouses not more often than every FETCH_TIMEOUT
      if (state.fetchedAt && !force && Date.now() - state.fetchedAt < FETCH_TIMEOUT) {
        return Promise.resolve(state.items);
      }

      commit(types.SET_WAREHOUSES_LOADING, true);

      return new Promise((resolve, reject) => {
        this.context.api.get('/warehouse/warehouses', {
          params: {
            active: true,
          },
        })
          .then(({ data }) => {
            commit(types.SET_WAREHOUSES, data.data);

            fixActiveWarehouse({ state, commit });

            commit(types.SET_WAREHOUSES_LOADING, false);
            resolve(state.items);
          })
          .catch(reject);
      });
    },
  }),

  fetchSingle: createPromisedAction({
    action({ commit }, id) {
      return new Promise((resolve, reject) => {
        this.context.api.get(`/warehouse/warehouses/${id}`)
          .then(({ data }) => {
            commit(types.UPDATE_WAREHOUSE, { id: data.id, data });
            resolve(data);
          })
          .catch(reject);
      });
    },
    key(id) {
      return id;
    },
  }),

  create({ state, commit }, warehouse) {
    return new Promise((resolve, reject) => {
      this.context.api.post('/warehouse/warehouses', warehouse)
        .then(({ data }) => {
          commit(types.CREATE_WAREHOUSE, data);
          fixActiveWarehouse({ state, commit });
          resolve(data);
        })
        .catch(reject);
    });
  },

  update({ commit }, { id, data }) {
    return new Promise((resolve, reject) => {
      this.context.api.put(`/warehouse/warehouses/${id}`, data)
        .then(({ data }) => {
          commit(types.UPDATE_WAREHOUSE, { id: data.id, data });
          resolve(data);
        })
        .catch(reject);
    });
  },

  remove: createPromisedAction({
    action({ state, commit }, warehouse) {
      return new Promise((resolve, reject) => {
        this.context.api.delete(`/warehouse/warehouses/${warehouse.id}`)
          .then(() => {
            commit(types.REMOVE_WAREHOUSE, warehouse);
            fixActiveWarehouse({ state, commit });
            resolve();
          })
          .catch(reject);
      });
    },
    key(warehouse) {
      return warehouse.id;
    },
  }),

  setActive({ commit }, id) {
    commit(types.SET_ACTIVE_WAREHOUSE, id);
    return Promise.resolve();
  },
};
