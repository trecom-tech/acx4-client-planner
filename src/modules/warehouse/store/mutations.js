import * as types from './mutation-types';

export default {
  [types.SET_WAREHOUSES](state, warehouses) {
    state.items.splice(0, state.items.length, ...warehouses);
    state.fetchedAt = Date.now();
  },

  [types.SET_WAREHOUSES_LOADING](state, loading) {
    state.loading = loading;
  },

  [types.SET_ACTIVE_WAREHOUSE](state, id) {
    state.activeId = id;
  },

  [types.CREATE_WAREHOUSE](state, warehouse) {
    state.items.push(warehouse);
  },

  [types.UPDATE_WAREHOUSE](state, { id, data }) {
    const index = state.items.findIndex(item => item.id === id);
    if (index > -1) {
      state.items.splice(index, 1, data);
    } else {
      state.items.push(data);
    }
  },

  [types.REMOVE_WAREHOUSE](state, warehouse) {
    const index = state.items.findIndex(item => item.id === warehouse.id);

    if (index > -1) {
      state.items.splice(index, 1);
    }
  },
};
