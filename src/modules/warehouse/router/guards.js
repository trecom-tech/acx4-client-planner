export async function warehouseGuard(context, next) {
  // After login need to wait warehouses list fetching
  if (!context.store.state.warehouse.fetchedAt || context.store.state.warehouse.loading) {
    await context.store.dispatch('warehouse/fetchList');
  }

  if (!context.store.getters['warehouse/items'].length) {
    return next({ name: 'Account/Warehouses' });
  }

  return next();
}
