import { authGuard } from '@/core/router/guards';
import { warehouseGuard } from '@/modules/warehouse/router/guards';

import IconBox from '@/assets/icons/box.svg';
import IconBoxes from '@/assets/icons/boxes.svg';
import IconCheckList from '@/assets/icons/check-list.svg';
import IconStorage from '@/assets/icons/storage.svg';
import IconTrolley from '@/assets/icons/trolley.svg';
import IconTruck from '@/assets/icons/truck.svg';
import IconUpload from '@/assets/icons/material/cloud_upload.svg';
import IconZones from '@/assets/icons/material/view_quilt.svg';

const Index = () => import(/* webpackChunkName: "modules/warehouse" */ '@/modules/warehouse/pages/Index.vue');
const ProductsIndex = () => import(/* webpackChunkName: "modules/warehouse" */ '@/modules/warehouse/pages/products/Index.vue');
const ProductsList = () => import(/* webpackChunkName: "modules/warehouse" */ '@/modules/warehouse/pages/products/List.vue');
const ProductsCreate = () => import(/* webpackChunkName: "modules/warehouse" */ '@/modules/warehouse/pages/products/Create.vue');
const ProductsEdit = () => import(/* webpackChunkName: "modules/warehouse" */ '@/modules/warehouse/pages/products/Edit.vue');
const ProductsStock = () => import(/* webpackChunkName: "modules/warehouse" */ '@/modules/warehouse/pages/products/Stock.vue');
const InboundsIndex = () => import(/* webpackChunkName: "modules/warehouse" */ '@/modules/warehouse/pages/inbounds/Index.vue');
const InboundsList = () => import(/* webpackChunkName: "modules/warehouse" */ '@/modules/warehouse/pages/inbounds/List.vue');
const InboundsCreate = () => import(/* webpackChunkName: "modules/warehouse" */ '@/modules/warehouse/pages/inbounds/Create.vue');
const InboundsEdit = () => import(/* webpackChunkName: "modules/warehouse" */ '@/modules/warehouse/pages/inbounds/Edit.vue');
const PutawaysIndex = () => import(/* webpackChunkName: "modules/warehouse" */ '@/modules/warehouse/pages/putaways/Index.vue');
const PutawaysList = () => import(/* webpackChunkName: "modules/warehouse" */ '@/modules/warehouse/pages/putaways/List.vue');
const PutawaysEdit = () => import(/* webpackChunkName: "modules/warehouse" */ '@/modules/warehouse/pages/putaways/Edit.vue');
const PicksIndex = () => import(/* webpackChunkName: "modules/warehouse" */ '@/modules/warehouse/pages/picks/Index.vue');
const PicksList = () => import(/* webpackChunkName: "modules/warehouse" */ '@/modules/warehouse/pages/picks/List.vue');
const PicksEdit = () => import(/* webpackChunkName: "modules/warehouse" */ '@/modules/warehouse/pages/picks/Edit.vue');
const LoadingIndex = () => import(/* webpackChunkName: "modules/warehouse" */ '@/modules/warehouse/pages/loading/Index.vue');
const LoadingList = () => import(/* webpackChunkName: "modules/warehouse" */ '@/modules/warehouse/pages/loading/List.vue');
const LoadingEdit = () => import(/* webpackChunkName: "modules/warehouse" */ '@/modules/warehouse/pages/loading/Edit.vue');
const Zones = () => import(/* webpackChunkName: "modules/warehouse" */ '@/modules/warehouse/pages/Zones.vue');
const Import = () => import(/* webpackChunkName: "modules/warehouse" */ '@/modules/warehouse/pages/Import.vue');

const NAMESPACE = 'Warehouse';

export default [
  {
    name: NAMESPACE,
    path: '/warehouse',
    redirect: '/warehouse/products',
    meta: {
      guard: [authGuard, warehouseGuard],
      asideNav: [
        {
          icon: IconBox,
          label: 'Products',
          to: {
            name: `${NAMESPACE}/Products/List`,
          },
        },
        {
          icon: IconStorage,
          label: 'Stock',
          to: '/warehouse/stock',
          // to: {
          //   name: `${NAMESPACE}/Stock/List`,
          // },
        },
        {
          icon: IconCheckList,
          label: 'Inbounds',
          to: {
            name: `${NAMESPACE}/Inbounds/List`,
          },
        },
        {
          icon: IconBoxes,
          label: 'Putaways',
          to: {
            name: `${NAMESPACE}/Putaways/List`,
          },
        },
        {
          icon: IconTrolley,
          label: 'Picks',
          to: {
            name: `${NAMESPACE}/Picks/List`,
          },
        },
        {
          icon: IconTruck,
          label: 'Loading',
          to: {
            name: `${NAMESPACE}/Loading/List`,
          },
        },
        {
          icon: IconZones,
          label: 'Zones',
          to: {
            name: `${NAMESPACE}/Zones`,
          },
        },
        {
          icon: IconUpload,
          label: 'Import',
          to: {
            name: `${NAMESPACE}/Import`,
          },
        },
      ],
    },
    component: Index,
    children: [
      {
        path: 'inbounds',
        component: InboundsIndex,
        children: [
          {
            name: `${NAMESPACE}/Inbounds/Create`,
            path: 'new',
            component: InboundsCreate,
          },
          {
            name: `${NAMESPACE}/Inbounds/Edit`,
            path: ':inboundId/edit',
            component: InboundsEdit,
          },
          {
            name: `${NAMESPACE}/Inbounds/List`,
            path: '',
            component: InboundsList,
          },
        ],
      },
      {
        path: 'loading',
        component: LoadingIndex,
        children: [
          {
            name: `${NAMESPACE}/Loading/Edit`,
            path: ':jobId/edit',
            component: LoadingEdit,
          },
          {
            name: `${NAMESPACE}/Loading/List`,
            path: '',
            component: LoadingList,
          },
        ],
      },
      {
        path: 'picks',
        component: PicksIndex,
        children: [
          {
            name: `${NAMESPACE}/Picks/Edit`,
            path: ':pickId/edit',
            component: PicksEdit,
          },
          {
            name: `${NAMESPACE}/Picks/List`,
            path: '',
            component: PicksList,
          },
        ],
      },
      {
        path: 'products',
        component: ProductsIndex,
        children: [
          {
            name: `${NAMESPACE}/Products/Create`,
            path: 'new',
            component: ProductsCreate,
          },
          {
            name: `${NAMESPACE}/Products/Edit`,
            path: ':productId/edit',
            component: ProductsEdit,
          },
          {
            name: `${NAMESPACE}/Products/Stock`,
            path: ':productId/stock',
            component: ProductsStock,
          },
          {
            name: `${NAMESPACE}/Products/List`,
            path: '',
            component: ProductsList,
          },
        ],
      },
      {
        path: 'putaways',
        component: PutawaysIndex,
        children: [
          {
            name: `${NAMESPACE}/Putaways/Edit`,
            path: ':putawayId/edit',
            component: PutawaysEdit,
          },
          {
            name: `${NAMESPACE}/Putaways/List`,
            path: '',
            component: PutawaysList,
          },
        ],
      },
      {
        name: `${NAMESPACE}/Zones`,
        path: 'zones',
        component: Zones,
      },
      {
        name: `${NAMESPACE}/Import`,
        path: 'import',
        component: Import,
      },
    ],
  },
];
