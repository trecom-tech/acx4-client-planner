import Vue from 'vue';
import Zones from './Zones.vue';
import ZonesItem from './ZonesItem.vue';
import ZonesTree from './ZonesTree.vue';

Vue.component('ZonesTree', ZonesTree);
Vue.component('ZonesItem', ZonesItem);

// TODO: optimize reactivity from deepest children to top

export default Zones;
