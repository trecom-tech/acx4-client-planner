import routes from './router/routes';

export default {
  name: 'account',
  router: {
    routes,
  },
};
