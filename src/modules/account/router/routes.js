import { authGuard } from '@/core/router/guards';

import IconBusiness from '@/assets/icons/material/business.svg';
import IconCard from '@/assets/icons/material/credit_card.svg';
import IconGroup from '@/assets/icons/material/group.svg';
import IconInvite from '@/assets/icons/material/contact_mail.svg';
import IconSettings from '@/assets/icons/material/settings.svg';

const Index = () => import(/* webpackChunkName: "modules/account" */ '@/modules/account/pages/Index.vue');
const Billing = () => import(/* webpackChunkName: "modules/account" */ '@/modules/account/pages/Billing.vue');
const Invitations = () => import(/* webpackChunkName: "modules/account" */ '@/modules/account/pages/Invitations.vue');
const Settings = () => import(/* webpackChunkName: "modules/account" */ '@/modules/account/pages/Settings.vue');
const Warehouses = () => import(/* webpackChunkName: "modules/account" */ '@/modules/account/pages/Warehouses.vue');
const UsersIndex = () => import(/* webpackChunkName: "modules/account" */ '@/modules/account/pages/users/Index.vue');
const UsersCreate = () => import(/* webpackChunkName: "modules/account" */ '@/modules/account/pages/users/Create.vue');
const UsersEdit = () => import(/* webpackChunkName: "modules/account" */ '@/modules/account/pages/users/Edit.vue');
const UsersList = () => import(/* webpackChunkName: "modules/account" */ '@/modules/account/pages/users/List.vue');

const NAMESPACE = 'Account';

export default [
  {
    name: NAMESPACE,
    path: '/account',
    component: Index,
    redirect: '/account/settings',
    meta: {
      guard: authGuard,
      asideNav: [
        {
          icon: IconSettings,
          label: 'Settings',
          to: {
            name: `${NAMESPACE}/Settings`,
          },
        },
        {
          icon: IconGroup,
          label: 'Users',
          to: {
            name: `${NAMESPACE}/Users/List`,
          },
        },
        {
          icon: IconBusiness,
          label: 'Warehouses',
          to: {
            name: `${NAMESPACE}/Warehouses`,
          },
        },
        {
          icon: IconInvite,
          label: 'Invitations',
          to: {
            name: `${NAMESPACE}/Invitations`,
          },
        },
        {
          icon: IconCard,
          label: 'Billing',
          to: {
            name: `${NAMESPACE}/Billing`,
          },
        },
      ],
    },
    children: [
      {
        name: `${NAMESPACE}/Billing`,
        path: 'billing',
        component: Billing,
      },
      {
        name: `${NAMESPACE}/Invitations`,
        path: 'invitations',
        component: Invitations,
      },
      {
        name: `${NAMESPACE}/Settings`,
        path: 'settings',
        component: Settings,
      },
      {
        name: `${NAMESPACE}/Warehouses`,
        path: 'warehouses',
        component: Warehouses,
      },
      {
        path: 'users',
        component: UsersIndex,
        children: [
          {
            name: `${NAMESPACE}/Users/Create`,
            path: 'new',
            component: UsersCreate,
          },
          {
            name: `${NAMESPACE}/Users/Edit`,
            path: ':userId/edit',
            component: UsersEdit,
          },
          {
            name: `${NAMESPACE}/Users/List`,
            path: '',
            component: UsersList,
          },
        ],
      },
    ],
  },
];
