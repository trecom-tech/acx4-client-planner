import * as types from './mutation-types';

function setUserData(state, data = {}) {
  state.id = data.userId || '';
  state.accessToken = data.accessToken || '';
  state.refreshToken = data.refreshToken || '';
  state.firstName = data.firstName || '';
  state.lastName = data.lastName || '';
  state.email = data.email || '';
  state.emailVerified = !!data.emailVerified;
  state.tokenExpires = data.tokenExpires || '';
}

export default {
  [types.LOGIN](state, data) {
    setUserData(state, data);
  },

  [types.LOGOUT](state) {
    setUserData(state, {});
  },

  [types.UPDATE_AUTHORIZATION](state, data) {
    setUserData(state, data);
  },

  [types.REMEMBER_USER_EMAIL](state, email) {
    state.lastUsedEmail = email || '';
  },

  [types.SET_LOGOUT_TIMER_ID](state, id) {
    state.logoutTimerId = id || null;
  },
};
