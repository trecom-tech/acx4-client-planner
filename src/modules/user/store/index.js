import mutations from './mutations';
import actions from './actions';
import getters from './getters';

export default {
  state: () => ({
    userId: '',
    accessToken: '',
    refreshToken: '',
    firstName: '',
    lastName: '',
    email: '',
    emailVerified: false,
    tokenExpires: null,
    branches: [],
    lastUsedEmail: '',
    logoutTimerId: null,
  }),
  actions,
  getters,
  mutations,
  namespaced: true,
};
