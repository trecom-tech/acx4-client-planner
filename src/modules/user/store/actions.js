import createPromisedAction from '@/core/store/helpers/createPromisedAction';
import * as types from './mutation-types';

function parseJwtToken(token) {
  if (!token) return false;

  const base64Url = token.split('.')[1];
  const base64 = base64Url.replace('-', '+').replace('_', '/');

  try {
    const data = JSON.parse(window.atob(base64));
    return {
      userId: data['custom:userId'],
      firstName: data.given_name,
      lastName: data.family_name,
      email: data.email,
      emailVerified: data.email_verified,
      tokenExpires: data.exp * 1000,
    };
  } catch (err) {
    return false;
  }
}

function setupAutoLogout({ commit, state }, time) {
  clearTimeout(state.logoutTimerId);
  const logoutTimeout = setTimeout(() => {
    commit(types.LOGOUT);
  }, time);
  commit(types.SET_LOGOUT_TIMER_ID, logoutTimeout);
}

function cancelAutoLogout({ commit, state }) {
  clearTimeout(state.logoutTimerId);
  commit(types.SET_LOGOUT_TIMER_ID);
}

export default {
  login: createPromisedAction({
    action({ commit, state }, credentials) {
      return new Promise((resolve, reject) => {
        this.context.api.post('/account/sessions', credentials)
          .then(({ data }) => {
            const accessToken = data.accessToken;
            const refreshToken = data.refreshToken;
            const userData = parseJwtToken(accessToken);

            if (!userData) {
              return reject();
            }

            const payload = Object.assign(userData, { accessToken, refreshToken });
            commit(types.LOGIN, payload);

            setupAutoLogout({ commit, state }, payload.tokenExpires - Date.now());

            return resolve();
          })
          .catch(reject);
      });
    },
  }),

  logout: createPromisedAction({
    action({ commit, state }) {
      return new Promise((resolve) => {
        this.context.api.delete('/account/sessions')
          .then(() => {
            cancelAutoLogout({ commit, state });
            commit(types.LOGOUT);
            resolve();
          })
          .catch((err) => {
            // Logout anyway
            console.error(err);
            cancelAutoLogout({ commit, state });
            commit(types.LOGOUT);
            resolve();
          });
      });
    },
  }),

  refreshToken: createPromisedAction({
    action({ state, commit }) {
      return new Promise((resolve, reject) => {
        const refreshToken = state.refreshToken;

        this.context.api.put('/account/sessions', { refreshToken }, { _tokenRefreshing: true })
          .then(({ data }) => {
            const accessToken = data.accessToken;
            const userData = parseJwtToken(accessToken);

            if (!userData) {
              return reject();
            }

            const payload = Object.assign(userData, { accessToken, refreshToken });
            commit(types.UPDATE_AUTHORIZATION, payload);

            setupAutoLogout({ commit, state }, payload.tokenExpires - Date.now());

            return resolve();
          })
          .catch(() => {
            commit(types.LOGOUT);
            reject();
          });
      });
    },
  }),

  rememberEmail({ commit }, data) {
    commit(types.REMEMBER_USER_EMAIL, data);
  },
};
