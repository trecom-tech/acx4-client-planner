export const LOGIN = 'LOGIN';
export const LOGOUT = 'LOGOUT';
export const REMEMBER_USER_EMAIL = 'REMEMBER_USER_EMAIL';
export const UPDATE_AUTHORIZATION = 'UPDATE_AUTHORIZATION';
export const SET_LOGOUT_TIMER_ID = 'SET_LOGOUT_TIMER_ID';
