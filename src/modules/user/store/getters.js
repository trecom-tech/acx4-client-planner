export default {
  accessToken(state) {
    return state.accessToken;
  },

  isAuthenticated(state) {
    return !!state.accessToken;
  },

  fullName(state) {
    const fullName = [];
    if (state.firstName) fullName.push(state.firstName);
    if (state.lastName) fullName.push(state.lastName);
    return fullName.join(' ');
  },
};
