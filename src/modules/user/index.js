import routes from './router/routes';
import store from './store';

export default {
  name: 'user',
  router: {
    routes,
  },
  store: {
    module: store,
  },
};
