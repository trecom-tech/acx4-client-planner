import { authGuard } from '@/core/router/guards';

const Index = () => import(/* webpackChunkName: "modules/user" */ '@/modules/user/pages/Index.vue');

const NAMESPACE = 'User';

export default [
  {
    name: NAMESPACE,
    path: '/user',
    component: Index,
    // redirect: '/user/settings',
    meta: {
      guard: authGuard,
      asideNav: [],
    },
    children: [],
  },
];
