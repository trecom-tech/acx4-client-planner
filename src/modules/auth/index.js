import routes from './router/routes';

export default {
  name: 'auth',
  router: {
    routes,
  },
};
