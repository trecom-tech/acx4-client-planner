// Guards
import { notAuthGuard } from '@/core/router/guards';

// Layouts
const AuthLayout = () => import(/* webpackChunkName: "layouts/auth" */ '@/modules/auth/layouts/Auth.vue');

// Pages
const Index = () => import(/* webpackChunkName: "modules/auth" */ '@/modules/auth/pages/Index.vue');
const Login = () => import(/* webpackChunkName: "modules/auth" */ '@/modules/auth/pages/Login.vue');
const Recover = () => import(/* webpackChunkName: "modules/auth" */ '@/modules/auth/pages/Recover.vue');
const RecoverInitiate = () => import(/* webpackChunkName: "modules/auth" */ '@/modules/auth/pages/RecoverInitiate.vue');
const Signup = () => import(/* webpackChunkName: "modules/auth" */ '@/modules/auth/pages/Signup.vue');

const NAMESPACE = 'Auth';

export default [
  {
    name: NAMESPACE,
    path: '/auth',
    redirect: '/auth/login',
    meta: {
      guard: notAuthGuard,
      layout: AuthLayout,
    },
    component: Index,
    children: [
      {
        name: `${NAMESPACE}/Login`,
        path: 'login',
        component: Login,
      },
      {
        name: `${NAMESPACE}/Signup`,
        path: 'signup',
        component: Signup,
      },
      {
        name: `${NAMESPACE}/Recover/Initiate`,
        path: 'recover/initiate',
        component: RecoverInitiate,
      },
      {
        name: `${NAMESPACE}/Recover`,
        path: 'recover/code',
        component: Recover,
      },
    ],
  },
];
