import routes from './router/routes';

export default {
  name: 'sales',
  router: {
    routes,
  },
};
