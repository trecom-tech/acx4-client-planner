import { authGuard } from '@/core/router/guards';
import { warehouseGuard } from '@/modules/warehouse/router/guards';

import IconDashboard from '@/assets/icons/material/insert_chart.svg';
import IconDoc from '@/assets/icons/material/insert_drive_file.svg';
import IconGroup from '@/assets/icons/material/group.svg';
import IconPrice from '@/assets/icons/material/local_offer.svg';
import IconUpload from '@/assets/icons/material/cloud_upload.svg';

const Index = () => import(/* webpackChunkName: "modules/sales" */ '@/modules/sales/pages/Index.vue');
const Dashboard = () => import(/* webpackChunkName: "modules/sales" */ '@/modules/sales/pages/Dashboard.vue');
const CustomersIndex = () => import(/* webpackChunkName: "modules/sales" */ '@/modules/sales/pages/customers/Index.vue');
const CustomersList = () => import(/* webpackChunkName: "modules/sales" */ '@/modules/sales/pages/customers/List.vue');
const CustomersCreate = () => import(/* webpackChunkName: "modules/sales" */ '@/modules/sales/pages/customers/Create.vue');
const CustomersEdit = () => import(/* webpackChunkName: "modules/sales" */ '@/modules/sales/pages/customers/Edit.vue');
const PricesIndex = () => import(/* webpackChunkName: "modules/sales" */ '@/modules/sales/pages/prices/Index.vue');
const PricesList = () => import(/* webpackChunkName: "modules/sales" */ '@/modules/sales/pages/prices/List.vue');
const PricesEdit = () => import(/* webpackChunkName: "modules/sales" */ '@/modules/sales/pages/prices/Edit.vue');
const OrdersIndex = () => import(/* webpackChunkName: "modules/sales" */ '@/modules/sales/pages/orders/Index.vue');
const OrdersList = () => import(/* webpackChunkName: "modules/sales" */ '@/modules/sales/pages/orders/List.vue');
const OrdersCreate = () => import(/* webpackChunkName: "modules/sales" */ '@/modules/sales/pages/orders/Create.vue');
const OrdersEdit = () => import(/* webpackChunkName: "modules/sales" */ '@/modules/sales/pages/orders/Edit.vue');
const Import = () => import(/* webpackChunkName: "modules/sales" */ '@/modules/sales/pages/Import.vue');

const NAMESPACE = 'Sales';

export default [
  {
    name: NAMESPACE,
    path: '/sales',
    redirect: '/sales/dashboard',
    meta: {
      guard: [authGuard, warehouseGuard],
      asideNav: [
        {
          icon: IconDashboard,
          label: 'Dashboard',
          to: {
            name: `${NAMESPACE}/Dashboard`,
          },
        },
        {
          icon: IconDoc,
          label: 'Orders',
          to: {
            name: `${NAMESPACE}/Orders/List`,
          },
        },
        {
          icon: IconGroup,
          label: 'Customers',
          to: {
            name: `${NAMESPACE}/Customers/List`,
          },
        },
        {
          icon: IconPrice,
          label: 'Prices',
          to: {
            name: `${NAMESPACE}/Prices/List`,
          },
        },
        {
          icon: IconUpload,
          label: 'Import',
          to: {
            name: `${NAMESPACE}/Import`,
          },
        },
      ],
    },
    component: Index,
    children: [
      {
        path: 'customers',
        component: CustomersIndex,
        children: [
          {
            name: `${NAMESPACE}/Customers/Create`,
            path: 'new',
            component: CustomersCreate,
          },
          {
            name: `${NAMESPACE}/Customers/Edit`,
            path: ':customerId/edit',
            component: CustomersEdit,
          },
          {
            name: `${NAMESPACE}/Customers/List`,
            path: '',
            component: CustomersList,
          },
        ],
      },
      {
        path: 'orders',
        component: OrdersIndex,
        children: [
          {
            name: `${NAMESPACE}/Orders/Create`,
            path: 'new',
            component: OrdersCreate,
          },
          {
            name: `${NAMESPACE}/Orders/Edit`,
            path: ':orderId/edit',
            component: OrdersEdit,
          },
          {
            name: `${NAMESPACE}/Orders/List`,
            path: '',
            component: OrdersList,
          },
        ],
      },
      {
        path: 'prices',
        component: PricesIndex,
        children: [
          {
            name: `${NAMESPACE}/Prices/Edit`,
            path: ':priceId/edit',
            component: PricesEdit,
          },
          {
            name: `${NAMESPACE}/Prices/List`,
            path: '',
            component: PricesList,
          },
        ],
      },
      {
        name: `${NAMESPACE}/Dashboard`,
        path: 'dashboard',
        component: Dashboard,
      },
      {
        name: `${NAMESPACE}/Import`,
        path: 'import',
        component: Import,
      },
    ],
  },
];
