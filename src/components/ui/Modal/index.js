import Modal from './Modal.vue';
import ModalPortal from './ModalPortal.vue';

export {
  Modal,
  ModalPortal,
};
