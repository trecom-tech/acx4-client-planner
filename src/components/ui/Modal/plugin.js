import Modal from './Modal.vue';
import ModalPortal from './ModalPortal.vue';

export default {
  install(Vue) {
    Vue.component('Modal', Modal);
    Vue.component('ModalPortal', ModalPortal);
  },
};
