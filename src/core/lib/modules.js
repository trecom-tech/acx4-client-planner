import { deepAssignByShape } from '@/core/helpers/utils';

// TODO: unregister, remove router hooks and store
// TODO: async beforeRegistration ?

export function registerModule(module, context) {
  const { modules, router, store } = context;

  if (modules[module.name]) {
    console.error(`Module with name '${module.name}' already registered.`);
    return false;
  }

  if (module.beforeRegistration) {
    module.beforeRegistration(context);
  }

  if (module.router) {
    if (module.router.routes) {
      router.addRoutes(module.router.routes);
    }

    if (module.router.beforeEach) {
      router.beforeEach(module.router.beforeEach);
    }

    if (module.router.afterEach) {
      router.afterEach(module.router.afterEach);
    }
  }

  if (module.store) {
    if (module.store.module) {
      // Keep existing store state
      if (store.state[module.name]) {
        const state = module.store.module.state();
        module.store.module.state = () => deepAssignByShape(state, store.state[module.name]);
      }

      store.registerModule(module.name, module.store.module);
    }
  }

  modules[module.name] = module;

  if (module.afterRegistration) {
    module.afterRegistration(context);
  }

  return module;
}
