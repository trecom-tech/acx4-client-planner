import Vue from 'vue';

import { throttle, fillArray } from '@/core/helpers/utils';

const BREAKPOINTS = {
  XS: 0,
  SM: 560,
  MD: 768,
  LG: 992,
  XL: 1200,
};

const keys = Object.keys(BREAKPOINTS);

const constants = keys.reduce((acc, key) => {
  acc[key] = key;
  return acc;
}, {});

const flow = keys
  .map(key => ({ name: key, minWidth: BREAKPOINTS[key] }))
  .sort((a, b) => a.minWidth - b.minWidth);

export default new Vue({
  data() {
    return {
      matched: [],
      ...constants,
    };
  },
  computed: {
    current() {
      return this.matched[this.matched.length - 1];
    },
  },
  created() {
    if (typeof window === 'undefined') return;
    this.onResize();
    window.addEventListener('resize', this.throttledOnResize);
  },
  beforeDestroy() {
    window.removeEventListener('resize', this.throttledOnResize);
  },
  methods: {
    throttledOnResize: throttle(function onResize() {
      return this.onResize();
    }, 300),
    onResize() {
      const matched = [];
      flow.forEach((bp) => {
        if (window.matchMedia(`(min-width: ${bp.minWidth}px)`).matches) {
          matched.push(bp.name);
        }
      });

      if (matched.join(',') !== this.matched.join(',')) {
        fillArray(this.matched, matched);
        this.$emit('change', { current: this.current, matched });
      }
    },
    getFlowIndex(bp) {
      return flow.findIndex(item => item.name === bp);
    },
    // Only this breakpoint
    is(bp) {
      return bp === this.current;
    },
    // From the breakpoint and up
    isUp(bp) {
      if (!bp) return false;
      return this.matched.indexOf(bp) !== -1;
    },
    // From the breakpoint and down
    isDown(bp) {
      if (!bp) return false;
      const lastMatched = this.matched[this.matched.length - 1];
      if (!lastMatched) return false;

      const flowIndex = this.getFlowIndex(bp);
      if (flowIndex === -1) return false;

      const lastMatchedFlowIndex = this.getFlowIndex(lastMatched);

      return lastMatchedFlowIndex - flowIndex <= 0;
    },
  },
});
