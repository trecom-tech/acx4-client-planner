import converter from 'convert-units';

import UOM from '@/core/constants/uom';

const units = {
  [UOM.CENTIMETER]: 'cm',
  [UOM.CUBIC_CENTIMETER]: 'cm3',
  [UOM.CUBIC_FOOT]: 'ft3',
  [UOM.CUBIC_INCH]: 'in3',
  [UOM.CUBIC_METER]: 'cm3',
  [UOM.CUBIC_YARD]: 'yd3',
  [UOM.FOOT]: 'ft', // TODO: ft-us
  [UOM.GRAM]: 'g',
  [UOM.KILOGRAM]: 'kg',
  [UOM.KILOMETER]: 'km',
  [UOM.LITER]: 'l',
  [UOM.METER]: 'm',
  [UOM.METRIC_TON]: 'mt',
  [UOM.MILE]: 'mi',
  [UOM.OUNCE]: 'oz',
  [UOM.POUND]: 'lb',
  [UOM.YARD]: 'yd',
};

export default (value, from, to) => converter(value).from(units[from]).to(units[to]);
