import Vue from 'vue';

import { throttle } from '@/core/helpers/utils';

export default new Vue({
  data() {
    return {
      // Browser
      opera: false,
      firefox: false,
      safari: false,
      ie: false,
      edge: false,
      chrome: false,
      // Device
      iPhone: false,
      iPad: false,
      iPod: false,
      android: false,
      mac: false,
      win: false,
      ios: false,
      retina: false,
      touchScreen: false,
      // User input
      userTouching: false,
      userHovering: false,
      // Screen
      viewport: null,
    };
  },
  created() {
    if (typeof window === 'undefined') return;

    this.detectDevice();
    this.detectBrowser();
    this.detectScreen();
    this.detectViewport();
    this.appendClasses();

    // https://codeburst.io/the-only-way-to-detect-touch-with-javascript-7791a3346685
    window.addEventListener('mouseover', this.onMouseOver);
    window.addEventListener('touchstart', this.onTouchStart);
    window.addEventListener('resize', this.throttledOnResize);
  },
  beforeDestroy() {
    window.removeEventListener('touchstart', this.onTouchStart);
    window.removeEventListener('mouseover', this.onMouseOver);
    window.removeEventListener('resize', this.throttledOnResize);
  },
  methods: {
    detectDevice() {
      const userAgent = window.navigator.userAgent;
      const platform = window.navigator.platform;
      this.iPhone = userAgent.indexOf('iPhone') !== -1;
      this.iPad = userAgent.indexOf('iPad') !== -1;
      this.iPod = userAgent.indexOf('iPod') !== -1;
      this.android = /Android/.test(userAgent);
      this.mac = !this.iPhone && !this.iPad && !this.iPod && !this.android && platform.indexOf('Mac') !== -1;
      this.win = platform.indexOf('Win') !== -1;
      this.ios = this.iPhone || this.iPad || this.iPod;
    },
    detectBrowser() {
      this.opera = (!!window.opr && !!window.opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
      this.firefox = typeof InstallTrigger !== 'undefined';
      this.safari =
        /constructor/i.test(window.HTMLElement)
        || (p => p.toString() === '[object SafariRemoteNotification]')(!window.safari || window.safari.pushNotification);
      this.ie = /*@cc_on!@*/false || !!document.documentMode; // eslint-disable-line
      this.edge = !this.ie && !!window.StyleMedia;
      this.chrome = !!window.chrome && !!window.chrome.webstore;
    },
    detectScreen() {
      this.retina = window.devicePixelRatio > 1;
      this.touchScreen =
        !!('ontouchstart' in window || (window.DocumentTouch && document instanceof window.DocumentTouch));
    },
    detectViewport() {
      this.viewport = {
        w: document.documentElement.clientWidth,
        h: document.documentElement.clientHeight,
      };
    },
    appendClasses() {
      // Fix Windows Phone Viewport
      // https://timkadlec.com/2013/01/windows-phone-8-and-device-width/
      if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
        const msViewportStyle = document.createElement('style');
        msViewportStyle.appendChild(document.createTextNode('@-ms-viewport{width:auto!important}'));
        document.getElementsByTagName('head')[0].appendChild(msViewportStyle);
      }

      // Add empty attribute ontouchstart to body
      // http://stackoverflow.com/questions/3885018/active-pseudo-class-doesnt-work-in-mobile-safari
      if (this.touchScreen) {
        document.body.setAttribute('ontouchstart', '');
      }

      // Add classes

      const htmlClasses = [];

      if (this.mac) {
        htmlClasses.push('_mac');
      }
      if (this.win) {
        htmlClasses.push('_win');
      }
      if (this.ios) {
        htmlClasses.push('_ios');
      }
      if (this.android) {
        htmlClasses.push('_android');
      }
      if (this.iPad) {
        htmlClasses.push('_ipad');
      }
      if (this.iPhone) {
        htmlClasses.push('_iphone');
      }
      if (this.retina) {
        htmlClasses.push('_retina');
      }
      if (this.touchScreen) {
        htmlClasses.push('_touchscreen');
      }
      if (this.opera) {
        htmlClasses.push('_opera');
      }
      if (this.firefox) {
        htmlClasses.push('_firefox');
      }
      if (this.safari) {
        htmlClasses.push('_safari');
      }
      if (this.ie) {
        htmlClasses.push('_ie');
      }
      if (this.edge) {
        htmlClasses.push('_edge');
      }
      if (this.chrome) {
        htmlClasses.push('_chrome');
      }

      htmlClasses.forEach(className => document.documentElement.classList.add(className));
    },
    onMouseOver() {
      this.userHovering = true;
      document.documentElement.classList.add('_hover');
      window.removeEventListener('mouseover', this.onMouseOver);
    },
    onTouchStart() {
      this.userTouching = true;
      document.documentElement.classList.add('_touch');
      window.removeEventListener('touchstart', this.onTouchStart);
    },
    throttledOnResize: throttle(function detectViewport() {
      return this.detectViewport();
    }, 300),
  },
});
