import Vue from 'vue';

const DEFAULT_TTL = 10000;

export default () => new Vue({
  data() {
    return {
      cache: {},
    };
  },
  methods: {
    cleanExpired() {
      Object.keys(this.cache).forEach((entryId) => {
        if (!this.cache[entryId].fetching && this.cache[entryId].expires < Date.now()) {
          delete this.cache[entryId];
        }
      });
    },
    clean(entryId) {
      if (entryId) {
        delete this.cache[entryId];
      } else {
        Object.keys(this.cache).forEach((entryId) => {
          delete this.cache[entryId];
        });
      }
    },
    fetch(config = {}) {
      const {
        fetcher,
        entryId,
        ttl,
      } = config;

      return new Promise((resolve, reject) => {
        let entry = this.cache[entryId];

        // Entry fetching now, wait...
        if (entry && entry.fetching) {
          entry.promise.then(resolve).catch(reject);
          return;
        }

        // Entry fetched and actual, return
        if (entry && (entry.fetchedAt + (ttl || DEFAULT_TTL) > Date.now())) {
          resolve(entry.data);
          return;
        }

        // Entry is not exist
        if (!entry) {
          this.$set(this.cache, entryId, {
            promise: null,
            fetching: false,
            fetchedAt: 0,
            error: null,
            data: null,
          });
          entry = this.cache[entryId];
        }

        const promise = new Promise((resolve, reject) => {
          const onError = (err) => {
            entry.fetching = false;
            entry.data = null;
            entry.fetchedAt = 0;
            entry.error = err;
            reject(err);
          };

          fetcher()
            .then((data) => {
              entry.fetching = false;
              entry.fetchedAt = Date.now();
              entry.expires = entry.fetchedAt + (ttl || DEFAULT_TTL);
              entry.data = data;
              resolve(data);
            })
            .catch(onError);
        });

        entry.promise = promise;
        entry.fetching = true;
        entry.error = null;

        promise.then(resolve).catch(reject);
      });
    },
    destroy() {
      this.$destroy();
    },
  },
});

// TODO: Is there a way for HRM?
if (module.hot) {
  module.hot.decline();
}
