import { getPathValue } from 'pathval';
import deepAssignUtil from 'deep-assign';

export function isObject(value) {
  return value && typeof value === 'object' && Array.isArray(value) === false;
}

export function isNumber(value) {
  return !Number.isNaN(parseFloat(value)) && Number.isFinite(value);
}

// Strings

export function camelToKebab(str) {
  return str.replace(/([a-z])([A-Z])/g, '$1-$2').toLowerCase();
}

export function capitalize(str) {
  return str.charAt(0).toUpperCase() + str.toLowerCase().slice(1);
}

export function camelize(str) {
  const string =
    str.toLowerCase()
      .replace(/[^A-Za-z0-9]/g, ' ')
      .split(' ')
      .reduce((result, word) => result + capitalize(word.toLowerCase()));
  return string.charAt(0).toLowerCase() + string.slice(1);
}

export function toSnakeCase(str) {
  return str
    .match(/[A-Z]{2,}(?=[A-Z][a-z]+[0-9]*|\b)|[A-Z]?[a-z]+[0-9]*|[A-Z]|[0-9]+/g)
    .map(x => x.toLowerCase())
    .join('_');
}

/**
 * Declension of numerals
 * @param number
 * @param titles - ['найдена', 'найдено', 'найдены']
 * @returns {String}
 */
export function declOfNums(number, titles) {
  const cases = [2, 0, 1, 1, 1, 2];

  const index = (number % 100 > 4 && number % 100 < 20)
    ? 2
    : cases[(number % 10 < 5) ? number % 10 : 5];

  return titles[index];
}

// Numbers

/**
 * Like Number.toFixed()
 * @param value
 * @param decimals
 * @returns {number}
 */
export function round(value, decimals) {
  const factor = decimals ? 10 ** decimals : 1;
  return Math.round(value * factor) / factor;
}

// Objects

/**
 * Returns a deeply cloned object without reference.
 */
export function deepClone(obj) {
  if (Array.isArray(obj)) {
    return obj.map(deepClone);
  }

  if (obj && typeof obj === 'object') {
    const cloned = {};
    const keys = Object.keys(obj);
    for (let i = 0, l = keys.length; i < l; i++) {
      const key = keys[i];
      cloned[key] = deepClone(obj[key]);
    }
    return cloned;
  }

  return obj;
}

// TODO: replace deep-assign package
/**
 * Recursive Object.assign()
 */
export function deepAssign(...args) {
  return deepAssignUtil(...args);
}

// TODO: assignByShape

/**
 * deepAssign(), but only for properties existing in the target object
 */
export function deepAssignByShape(target, ...args) {
  if (!isObject(target)) {
    throw new TypeError('target must be an object');
  }

  args.forEach((from) => {
    if (!isObject(from)) return;

    Object.keys(target).forEach((key) => {
      // Skip prop if no value for it
      if (typeof from[key] === 'undefined') return;

      // Fill array with new elements
      // Not array value is ignored
      if (Array.isArray(target[key])) {
        const val = Array.isArray(from[key]) ? deepClone(from[key]) : [];
        target[key].splice(0, target[key].length, ...val);
        return;
      }

      // Fill object property
      // Set null or fill each inner property
      if (target[key] && typeof target[key] === 'object') {
        if (from[key] === null) {
          target[key] = null;
          return;
        }

        deepAssignByShape(target[key], from[key]);
        return;
      }

      target[key] = from[key];
    });
  });

  return target;
}

/**
 * Check if all two objects properties are equal
 */
export function areObjectsPropsEqual(a, b) {
  const isEqual = (a, b) => {
    const aType = typeof a;
    const bType = typeof b;

    // Different types
    if (aType !== bType) return false;

    // Null
    if (a === null || b === null) return a === b;

    // Array
    if (Array.isArray(a)) {
      if (!Array.isArray(b)) return false;
      if (a.length !== b.length) return false;
      return !a.some((el, i) => !isEqual(a[i], b[i]));
    }

    // Object
    if (aType === 'object') {
      return areObjectsPropsEqual(a, b);
    }

    // String, Number, Boolean
    return a === b;
  };

  return !Object.keys(a).some(key => !isEqual(a[key], b[key]));
}

// Arrays

export function sortArrayOfObjects(array, path, order) {
  const isDescending = order === -1;
  return [...array].sort((a, b) => {
    let sortA = getPathValue(a, path);
    let sortB = getPathValue(b, path);

    if (isDescending) {
      [sortA, sortB] = [sortB, sortA];
    }

    const intSortA = parseInt(sortA, 10);
    const intSortB = parseInt(sortB, 10);
    if (!Number.isNaN(intSortA) && !Number.isNaN(intSortB)) {
      return intSortA - intSortB;
    }

    if (sortA === null && sortB === null) {
      return 0;
    }

    [sortA, sortB] = [sortA, sortB].map(s => ((s || '').toString().toLocaleLowerCase()));

    if (sortA > sortB) return 1;
    if (sortA < sortB) return -1;

    return 0;
  });
}

export function removeFromArray(array, item) {
  if (!Array.isArray(array)) return false; // TODO: thrown error?

  const index = array.indexOf(item);
  if (index === -1) return array;

  array.splice(index, 1);

  return array;
}

export function fillArray(array, items = []) {
  if (!Array.isArray(array) || !Array.isArray(items)) return false; // TODO: thrown error?
  array.splice(0, array.length, ...items);
  return array;
}

// Functions

// TODO: leading / trailing
// https://css-tricks.com/debouncing-throttling-explained-examples/
// Now throttle is leading and debounce is trailing

/**
 * Call func at most once per every wait ms
 * @param func
 * @param ms
 * @returns {wrapper}
 */
export function throttle(func, ms = 100) {
  let isThrottled = false;
  let savedArgs;
  let savedThis;

  function wrapper(...args) {
    if (isThrottled) {
      savedArgs = args;
      savedThis = this;
      return;
    }

    func.apply(this, args);

    isThrottled = true;

    setTimeout(() => {
      isThrottled = false;

      if (savedArgs) {
        wrapper.apply(savedThis, savedArgs);
        savedArgs = null;
        savedThis = null;
      }
    }, ms);
  }

  return wrapper;
}

/**
 * Call function after `ms` have elapsed since the last time the debounced function call
 * @param func
 * @param ms
 * @returns {wrapper}
 */
export function debounce(func, ms = 100) {
  let timeout;

  function wrapper(...args) {
    clearTimeout(timeout);

    timeout = setTimeout(() => {
      func.apply(this, args);
    }, ms);
  }

  return wrapper;
}

// DOM

/**
 * Apply callback for each element in list.
 * @param elements
 * @param cb
 * @returns {*}
 */
export function loopElements(elements, cb) {
  let els;

  if (typeof elements === 'string') {
    els = document.querySelectorAll(elements);
  } else if (elements instanceof NodeList) {
    els = elements;
  } else {
    console.warn('elements param must be element selector or NodeList');
    return;
  }

  Array.prototype.forEach.call(els, el => cb(el));
}

/**
 * Find closest element
 * Require Element.prototype.matches.
 * @param element
 * @param checker
 * @param last
 * @returns {*}
 */
export function findClosestElement(element, checker, last = document.body) {
  let target = element;
  let lastReached = false;

  while (target && !lastReached) {
    if (typeof checker === 'string' && target.matches(checker)) {
      return target;
    }

    if (typeof checker === 'function' && checker(target)) {
      return target;
    }

    if (target === last) {
      lastReached = true;
    }

    target = target.parentNode;
  }

  return null;
}

/**
 * Clear value of input file
 * @param el
 */
export function clearInputFile(el) {
  if (el.value) {
    try {
      el.value = '';
    } catch (err) { /* nothing */ }

    if (el.value) {
      const form = document.createElement('form');
      const ref = el.nextSibling;
      const parent = el.parentNode;

      form.appendChild(el);
      form.reset();

      if (ref) {
        ref.parentNode.insertBefore(el, ref);
      } else {
        parent.appendChild(el);
      }
    }
  }
}

/**
 * Get element attribute and parse it value
 * @param el
 * @param attr
 * @returns {*}
 */
export function parseAttr(el, attr) {
  const value = el.getAttribute(attr);

  if (value === 'true') {
    return true;
  }

  if (value === 'false') {
    return false;
  }

  if (value === 'null') {
    return null;
  }

  // Number
  if (value === `${+value}`) {
    return +value;
  }

  // JSON
  if (/^(?:{[\w\W]*}|\[[\w\W]*])$/.test(value)) {
    try {
      return JSON.parse(value);
    } catch (err) { /* nothing */ }
  }

  return value;
}

/**
 * Fire event on element
 * @param el
 * @param e
 */
export function dispatchEvent(el, e) {
  if ('createEvent' in document) {
    const event = document.createEvent('HTMLEvents');
    event.initEvent(e, false, true);
    el.dispatchEvent(event);
  } else {
    el.fireEvent(`on${e}`);
  }
}

export function getScrollbarWidth() {
  const inner = document.createElement('p');
  inner.style.width = '100%';
  inner.style.height = '200px';

  const outer = document.createElement('div');
  outer.style.position = 'absolute';
  outer.style.top = '0px';
  outer.style.left = '0px';
  outer.style.visibility = 'hidden';
  outer.style.width = '200px';
  outer.style.height = '150px';
  outer.style.overflow = 'hidden';
  outer.appendChild(inner);

  document.body.appendChild(outer);
  const w1 = inner.offsetWidth;
  outer.style.overflow = 'scroll';
  let w2 = inner.offsetWidth;
  if (w1 === w2) w2 = outer.clientWidth;

  document.body.removeChild(outer);

  return w1 - w2;
}

export function getZIndex(el) {
  if (!el || el.nodeType !== Node.ELEMENT_NODE) return 0;
  const index = +window.getComputedStyle(el).zIndex;
  if (Number.isNaN(index)) return getZIndex(el.parentNode);
  return index;
}

// Other

export function isMapCoordsValid(coords) {
  if (!coords) return false;

  let lat;
  let lng;

  // [lat, lng]
  if (Array.isArray(coords)) {
    lat = coords[0];
    lng = coords[1];
  } else {
    lat = coords.lat;
    lng = coords.lng;
  }

  return lat && lng && lat >= -90 && lat <= 90 && lng >= -180 && lng <= 180;
}
