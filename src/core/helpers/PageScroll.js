import Vue from 'vue';

import { getScrollbarWidth } from '@/core/helpers/utils';

class PageScroll {
  constructor() {
    if (typeof window === 'undefined') return;

    this.eventBus = new Vue();
    this.paddingSet = false;
    this.lastScrollTop = null;
    this.lockers = new Set();
  }

  lock(lockId) {
    let scrollbarWidth = 0;
    if (document.body.scrollHeight > document.body.clientHeight) {
      scrollbarWidth = getScrollbarWidth();
      if (scrollbarWidth) {
        const isRTL = document.dir === 'rtl';
        document.body.style[isRTL ? 'paddingLeft' : 'paddingRight'] = `${scrollbarWidth}px`;
        this.paddingSet = true;
      }
    }
    this.lastScrollTop = window.window.pageYOffset;
    document.body.classList.add('_scroll-locked');

    if (lockId) {
      this.lockers.add(lockId);
    }

    this.eventBus.$emit('layout-locked', { scrollbarWidth });
  }

  unlock(lockId) {
    if (lockId) {
      this.lockers.delete(lockId);
    }

    if (this.lockers.size > 0) return;

    if (this.paddingSet) {
      const isRTL = document.dir === 'rtl';
      document.body.style[isRTL ? 'paddingLeft' : 'paddingRight'] = '';
      this.paddingSet = false;
    }
    document.body.classList.remove('_scroll-locked');
    if (this.lastScrollTop) {
      window.scrollTo(0, this.lastScrollTop);
    }
    this.eventBus.$emit('layout-unlocked');
  }

  unlockForce() {
    this.lockers.clear();
    this.unlockScroll();
  }
}

export default new PageScroll();
