import * as types from './mutation-types';

export default {
  [types.SET_ASIDE_EXPANDED](state, expanded) {
    state.asideExpanded = !!expanded;
  },

  [types.SET_LANG](state, lang) {
    state.lang = lang;
  },
};
