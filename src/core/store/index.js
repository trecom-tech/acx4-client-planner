import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate';
import { deepAssign } from '@/core/helpers/utils';
// import Cookies from 'js-cookie';

import mutations from './mutations';
import actions from './actions';
import getters from './getters';
import state from './state';
import modules from './modules';

Vue.use(Vuex);

export function createStore(context) {
  const plugins = [];

  if (!context.ssr) {
    plugins.push(createPersistedState({
      paths: [
        'asideExpanded',
        'lang',

        'warehouse.activeId',
        'ui',
        'user',
      ],
      // Save to cookies for SSR access
      // storage: {
      //   getItem: key => Cookies.get(key),
      //   setItem: (key, value) => Cookies.set(key, value, { expires: 30 }),
      //   removeItem: key => Cookies.remove(key),
      // },
    }));
  }

  const store = new Vuex.Store({
    state,
    actions,
    getters,
    mutations,
    modules,
    strict: process.env.NODE_ENV !== 'production',
    plugins,
  });

  // Set initial store state from SSR-result object or from Cookies
  let initialState = {};
  if (!context.ssr) {
    const STATE = window.__INITIAL_STATE__ || {};
    if (STATE.vuex) {
      initialState = STATE.vuex;
    }
  } if (context.cookies && context.cookies.vuex) {
    initialState = context.cookies.vuex;
  }
  store.replaceState(deepAssign({}, store.state, initialState));

  // Inject store in app context
  context.store = store;
  store.context = context;

  if (module.hot) {
    module.hot.accept(['./state', './actions', './mutations', './getters', './modules'], () => {
      store.hotUpdate({
        state: require('./state').default,
        actions: require('./actions').default,
        getters: require('./getters').default,
        mutations: require('./mutations').default,
        modules: require('./modules').default,
      });
    });
  }

  return store;
}
