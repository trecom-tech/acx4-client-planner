/**
 * Prevent calling the same action while previous pending
 * @param action
 * @param key
 * @returns {Function}
 */
export default function createPromisedAction({ action, key }) {
  const promises = {};

  return function callAction(store, params) {
    let actionKey;

    if (typeof key === 'function') {
      actionKey = key(params);
    }

    if (!actionKey) {
      actionKey = key ? key.toString() : '_default';
    }

    if (promises[actionKey]) {
      return promises[actionKey];
    }

    const actionPromise = action.call(this, store, params);

    if (!actionPromise || (!(actionPromise instanceof Promise) && (typeof actionPromise.then !== 'function'))) {
      return Promise.resolve(actionPromise);
    }

    promises[actionKey] = new Promise((resolve, reject) => {
      actionPromise
        .then((result) => {
          promises[actionKey] = null;
          return resolve(result);
        })
        .catch((err) => {
          promises[actionKey] = null;
          return reject(err);
        });
    });

    return promises[actionKey];
  };
}
