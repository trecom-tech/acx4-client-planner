import { languages } from '@/i18n';

export default () => ({
  asideExpanded: true,
  languages,
  lang: Object.keys(languages)[0],
});
