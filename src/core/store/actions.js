import * as types from './mutation-types';
import { setI18nLanguage } from '@/i18n';

export default {
  setAsideExpanded({ commit }, expanded) {
    commit(types.SET_ASIDE_EXPANDED, expanded);
  },

  setLang({ state, commit }, lang) {
    let code;

    if (typeof lang === 'string') {
      code = lang;
    } else if (lang && lang.code) {
      code = lang.code;
    }

    return new Promise((resolve, reject) => {
      if (!state.languages[code]) {
        code = Object.keys(state.languages)[0];
      }

      return setI18nLanguage(code)
        .then((lang) => {
          commit(types.SET_LANG, lang);
          resolve(lang);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
};
