export default {
  languages(state) {
    return Object.keys(state.languages).map((key) => {
      const language = state.languages[key];
      language.code = key;
      return language;
    });
  },

  language(state) {
    const language = state.languages[state.lang];
    language.code = state.lang;
    return language;
  },

  languageDir(state) {
    const language = state.languages[state.lang];
    return language.dir;
  },
};
