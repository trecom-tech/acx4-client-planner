import Vue from 'vue';
import PS from 'perfect-scrollbar';
import './style.scss';

const cache = new WeakMap();

export default {
  bind(el, binding) {
    if (typeof window === 'undefined') return;

    const defaults = {
      handlers: ['click-rail', 'drag-thumb', 'keyboard', 'wheel', 'touch'],
      wheelSpeed: 1,
      wheelPropagation: true,
      minScrollbarLength: null,
      maxScrollbarLength: null,
      scrollingThreshold: 1000,
      useBothWheelAxes: false,
      suppressScrollX: true,
      suppressScrollY: false,
      scrollXMarginOffset: 0,
      scrollYMarginOffset: 0,
    };

    // https://github.com/utatti/perfect-scrollbar/issues/792
    Vue.nextTick(() => {
      const ps = new PS(el, Object.assign(defaults, binding.value));
      let observer;

      if (window.MutationObserver) {
        observer = new MutationObserver(() => {
          ps.update();
        });
        observer.observe(el, {
          childList: true,
          subtree: true,
        });
      }

      cache.set(el, { ps, observer });
    });
  },
  componentUpdated(el) {
    const instance = cache.get(el);
    if (instance && instance.ps) {
      instance.ps.update();
    }
  },
  unbind(el) {
    const instance = cache.get(el);
    if (!instance) return;

    if (instance.observer) {
      instance.observer.disconnect();
    }
    instance.ps.update();
    cache.delete(el);
  },
};
