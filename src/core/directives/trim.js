const trim = (el) => {
  el.childNodes.forEach((node) => {
    if (node.nodeType !== Node.TEXT_NODE) return;

    if (node.data.trim() === '') {
      node.remove();
    }

    node.data = node.data.trim();
  });
};

export default {
  inserted: trim,
  componentUpdated: trim,
};
