const handle = (el, binding) => {
  if (binding.value || typeof binding.value === 'undefined') {
    el.focus();
    if (binding.modifiers.select) {
      el.select();
    }
  } else {
    el.blur();
  }
};

export default {
  inserted(el, binding) {
    handle(el, binding);
  },

  componentUpdated(el, binding) {
    if (Boolean(binding.value) === Boolean(binding.oldValue)) {
      return;
    }

    handle(el, binding);
  },
};
