// Based on https://github.com/silverspectro/vue-clickaway2

const HANDLER = '_vue_clickout_handler';

function unbind(el, binding) {
  document.documentElement.removeEventListener(binding.arg || 'click', el[HANDLER], false);
  delete el[HANDLER];
}

function bind(el, binding, vnode) {
  unbind(el, binding);

  const vm = vnode.context;
  const handler = binding.value;

  if (!handler) return;

  if (typeof handler !== 'function') {
    if (process.env.NODE_ENV !== 'production') {
      console.warn(`v-${binding.name}="${binding.expression}" expects a function value, got ${handler}`);
    }
    return;
  }

  // https://jakearchibald.com/2015/tasks-microtasks-queues-and-schedules/
  // https://github.com/simplesmiler/vue-clickaway/issues/8
  let initialMacrotaskEnded = false;
  setTimeout(() => {
    initialMacrotaskEnded = true;
  }, 0);

  el[HANDLER] = (e) => {
    const path = e.path || (e.composedPath ? e.composedPath() : undefined);

    if (initialMacrotaskEnded && (path ? path.indexOf(el) < 0 : !el.contains(e.target))) {
      return handler.call(vm, e);
    }

    return null;
  };

  document.documentElement.addEventListener(binding.arg || 'click', el[HANDLER], false);
}

export default {
  bind,
  update(el, binding, vnode) {
    if (binding.value === binding.oldValue) return;
    bind(el, binding, vnode);
  },
  unbind,
};
