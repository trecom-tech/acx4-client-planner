import Stickyfill from 'stickyfilljs';

export default {
  bind(el, binding) {
    if (typeof window === 'undefined') return;
    if (binding.value !== false) {
      Stickyfill.addOne(el);
    }
  },
  update(el, binding) {
    if (binding.value === binding.oldValue) return;

    if (binding.value !== false) {
      Stickyfill.addOne(el);
    } else {
      Stickyfill.removeOne(el);
    }
  },
  unbind(el) {
    Stickyfill.removeOne(el);
  },
};
