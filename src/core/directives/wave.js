import Vue from 'vue';
import TweenMax from 'gsap/umd/TweenMax';

const cache = new WeakMap();

class Wave {
  constructor(el, { color, center }) {
    this.el = el;
    this.color = color;
    this.center = center;

    this.onClickShim = (e) => {
      this.onClick(e);
    };

    const availablePositions = ['relative', 'absolute', 'fixed'];
    if (availablePositions.indexOf(window.getComputedStyle(el).position) === -1) {
      this.elPositionStyle = this.el.style.position;
      this.el.style.position = 'relative';
    }

    this.el.addEventListener('mousedown', this.onClickShim);
    // this.el.addEventListener('touchstart', this.onClickShim);
  }

  destroy() {
    this.el.style.position = this.elPositionStyle;
    this.el.removeEventListener('mousedown', this.onClickShim);
    // this.el.removeEventListener('touchstart', this.onClickShim);
  }

  onClick(e) {
    // Disable right click
    if (e.button === 2) {
      return;
    }

    const elSize = Math.round(Math.max(this.el.offsetWidth, this.el.offsetHeight));
    let waveTop;
    let waveLeft;

    if (this.center) {
      waveTop = this.el.offsetHeight / 2 - elSize;
      waveLeft = this.el.offsetWidth / 2 - elSize;
    } else {
      const elRect = this.el.getBoundingClientRect();
      const scrollTop = window.pageYOffset || document.documentElement.scrollTop;
      const elTop = elRect.top + scrollTop;
      const elLeft = elRect.left;
      let eventTop = e.pageY;
      let eventLeft = e.pageX;

      if (e.type === 'touchstart') {
        eventTop = e.changedTouches[0].pageY;
        eventLeft = e.changedTouches[0].pageX;
      }

      waveTop = eventTop - elTop - elSize;
      waveLeft = eventLeft - elLeft - elSize;
    }

    const wave = document.createElement('div');
    const waveWrap = document.createElement('div');

    waveWrap.style.borderRadius = window.getComputedStyle(this.el).borderRadius;
    wave.style.backgroundColor = this.color;

    // Safari border-radius + overflow: hidden + CSS transform fix
    waveWrap.style.webkitMaskImage = '-webkit-radial-gradient(white, black)';

    TweenMax.set(waveWrap, {
      position: 'absolute',
      pointerEvents: 'none',
      width: `${this.el.offsetWidth}px`,
      height: `${this.el.offsetHeight}px`,
      left: '0px',
      top: '0px',
      overflow: 'hidden',
    });

    TweenMax.set(wave, {
      position: 'absolute',
      width: `${elSize * 2}px`,
      height: `${elSize * 2}px`,
      borderRadius: '100%',
      scale: 0,
      autoAlpha: 0.9,
      top: `${waveTop}px`,
      left: `${waveLeft}px`,
    });

    waveWrap.appendChild(wave);
    this.el.appendChild(waveWrap);

    TweenMax.to(wave, 0.8, {
      scale: 1,
      autoAlpha: 0,
      force3D: true,
      onComplete: () => {
        waveWrap.parentNode.removeChild(waveWrap);
      },
    });
  }
}

function unbind(el) {
  const wave = cache.get(el);
  if (wave) {
    wave.destroy();
    cache.delete(el);
  }
}

function bind(el, binding) {
  unbind(el);

  Vue.nextTick(() => {
    let color = 'currentColor';
    const center = !!binding.modifiers.center;

    if (binding.value && binding.value.color) {
      color = binding.value.color;
    }

    const wave = new Wave(el, { color, center });
    cache.set(el, wave);
  });
}

function update(el, binding) {
  if (binding.value !== binding.oldValue) {
    bind(el, binding);
  }
}

export default {
  bind,
  update,
  unbind,
};
