import { removeFromArray } from '@/core/helpers/utils';

// TODO: check SSR
// TODO: don't use activeCatchers array

const FOCUSABLE = [
  'a[href]',
  'area[href]',
  'input:not([disabled])',
  'select:not([disabled])',
  'textarea:not([disabled])',
  'button:not([disabled])',
  '[tabindex="0"]',
];
const activeCatchers = [];

export default {
  props: {
    returnFocus: {
      type: Boolean,
      default: true,
    },
  },
  data() {
    return {
      focusable: [],
      focusableObserver: null,
      focusableElement: null,
      lastFocusedElement: null,
    };
  },
  methods: {
    catchFocus() {
      if (typeof window === 'undefined') return;
      if (this.returnFocus) {
        this.lastFocusedElement = document.activeElement;
      }
      document.addEventListener('keydown', this.onTab);
      document.addEventListener('focusin', this.onFocusIn);
      activeCatchers.push(this);
      this.collectFocusable();
      this.startFocusableObserver();

      const focusableElement = this.focusableElement || this.$el;
      const focusedElement = document.activeElement;
      if (
        !focusableElement
        || (focusedElement !== focusableElement && !focusableElement.contains(focusedElement))
      ) {
        this.grabfocus();
      }
      console.log(this.focusable);
    },
    releaseFocus() {
      if (typeof window === 'undefined') return;
      document.removeEventListener('keydown', this.onTab);
      document.removeEventListener('focusin', this.onFocusIn);
      removeFromArray(activeCatchers, this);
      this.stopFocusableObserver();
      if (this.lastFocusedElement) {
        this.lastFocusedElement.focus();
      }
    },
    grabfocus() {
      // TODO: how to focus if no focusableElement?
      const focusableElement = this.focusableElement || this.$el;

      if (!this.focusable.length) {
        focusableElement.focus();
        return;
      }

      this.focusable[0].focus();
    },
    collectFocusable() {
      const focusableElement = this.focusableElement || this.$el;
      const focusable = focusableElement
        ? focusableElement.querySelectorAll(FOCUSABLE.join(','))
        : [];
      this.focusable.splice(0, this.focusable.length, ...focusable);
    },
    startFocusableObserver() {
      this.stopFocusableObserver();
      const focusableElement = this.focusableElement || this.$el;
      if (window.MutationObserver && focusableElement) {
        this.focusableObserver = new MutationObserver(() => {
          this.collectFocusable();
        });
        this.focusableObserver.observe(focusableElement, {
          childList: true,
          subtree: true,
        });
      }
    },
    stopFocusableObserver() {
      if (this.focusableObserver) {
        this.focusableObserver.disconnect();
        this.focusableObserver = null;
      }
    },
    isCatchingFocus() {
      return activeCatchers.indexOf(this) === activeCatchers.length - 1;
    },
    onTab(e) {
      if (e.keyCode !== 9 || !this.isCatchingFocus()) return;

      const focusableElement = this.focusableElement || this.$el;
      const focusedElement = document.activeElement;
      if (
        !focusableElement
        || !focusedElement
        || (focusedElement !== focusableElement && !focusableElement.contains(focusedElement))
      ) {
        e.preventDefault();
        this.grabfocus();
        return;
      }

      const firstFocusable = this.focusable[0];
      const lastFocusable = this.focusable[this.focusable.length - 1];
      const isFirstFocusable = focusedElement === firstFocusable;
      const isLastFocusable = focusedElement === lastFocusable;

      // From first to last if shift pressed
      if (e.shiftKey && isFirstFocusable) {
        e.preventDefault();
        lastFocusable.focus();
        return;
      }

      // From last to first if shift not pressed
      if (!e.shiftKey && isLastFocusable) {
        e.preventDefault();
        firstFocusable.focus();
      }
    },
    onFocusIn(e) {
      if (!this.isCatchingFocus()) return;
      const focusableElement = this.focusableElement || this.$el;
      if (
        !focusableElement
        || (e.target !== focusableElement && !focusableElement.contains(e.target))
      ) {
        e.preventDefault();
        this.grabfocus();
      }
    },
  },
};
