import {
  deepClone,
  deepAssign,
  areObjectsPropsEqual,
} from '@/core/helpers/utils';

export default {
  props: {
    values: {
      type: Object,
      default: null,
    },
    validationPath: {
      type: String,
      default: '',
    },
  },
  data() {
    return {
      fields: {},
    };
  },
  computed: {
    // Trick to have correct oldValue in watcher
    computedValues() {
      return deepClone(this.values);
    },
    computedFields() {
      return deepClone(this.fields);
    },
  },
  validations: {},
  watch: {
    computedValues: {
      handler(val, oldVal) {
        if (areObjectsPropsEqual(val, oldVal)) return;
        this.fill(val);
      },
      deep: true,
    },
    computedFields: {
      handler(val, oldVal) {
        if (areObjectsPropsEqual(val, oldVal)) return;
        this.$emit('change', val);
      },
      deep: true,
    },
  },
  beforeMount() {
    this.fill(this.values);
  },
  methods: {
    getFieldErrorMessages(path) {
      return this.getParentFieldErrorMessages(`${this.validationPath}.${path}`);
    },
    isFieldValid(path) {
      return this.isParentFieldValid(`${this.validationPath}.${path}`);
    },
    isFieldInvalid(path) {
      return this.isParentFieldInvalid(`${this.validationPath}.${path}`);
    },
    fill(values) {
      if (values) {
        this.fillFormPartFields(this.fields, values);
      }
    },
    fillFormPartFields(fields, values) {
      deepAssign(fields, values);
    },
  },
  inject: {
    isParentFieldValid: {
      from: 'isFieldValid',
    },
    isParentFieldInvalid: {
      from: 'isFieldInvalid',
    },
    getParentFieldErrorMessages: {
      from: 'getFieldErrorMessages',
    },
  },
};
