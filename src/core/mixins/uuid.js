import shortid from 'shortid';

export default {
  data() {
    return {
      uuid: shortid.generate(),
    };
  },
};
