export default {
  props: {
    value: {
      type: [String, Number, Object, Array],
      default: () => ([]),
    },
    multiple: {
      type: Boolean,
      default: false,
    },
    trackBy: {
      type: String,
      default: '',
    },
    max: {
      type: [Number, Boolean],
      default: false,
    },
    limit: {
      type: Number,
      default: 999,
    },
    internalSearch: {
      type: Boolean,
      default: true,
    },
    searchable: {
      type: Boolean,
      default: true,
    },
    preserveSearch: {
      type: Boolean,
      default: false,
    },
    // Behaivor
    clearOnSelect: {
      type: Boolean,
      default: true,
    },
    hideSelected: {
      type: Boolean,
      default: false,
    },
    allowEmpty: {
      type: Boolean,
      default: true,
    },
    closeOnSelect: {
      type: Boolean,
      default: true,
    },
    preselectFirst: {
      type: Boolean,
      default: false,
    },
    // Tags
    taggable: {
      type: Boolean,
      default: false,
    },
    tagPlaceholder: {
      type: String,
      default: 'Press enter to create a tag',
    },
    // Groups
    groupValues: {
      type: String,
      default: '',
    },
    groupLabel: {
      type: String,
      default: '',
    },
    groupSelect: {
      type: Boolean,
      default: false,
    },
  },
};
