import { getZIndex } from '@/core/helpers/utils';

export default {
  data() {
    return {
      stackClass: '__stackable',
      stackMinZIndex: 1000,
      stackableElement: null,
    };
  },
  computed: {
    activeZIndex() {
      if (typeof window === 'undefined') return 0;

      const stackableElement = this.stackableElement || this.$el;

      if (!stackableElement) return null;

      const index = !this.isInStack
        ? getZIndex(stackableElement)
        : this.getMaxZIndex([stackableElement]) + 2;

      if (index === null) return index;
      return parseInt(index, 10);
    },
  },
  methods: {
    getMaxZIndex(exclude = []) {
      const stackableElement = this.stackableElement || this.$el;

      const zis = [this.stackMinZIndex, getZIndex(stackableElement)];
      const stackedElements = [...document.getElementsByClassName(this.stackClass)];

      stackedElements.forEach((element) => {
        if (!exclude.includes(element)) {
          zis.push(getZIndex(element));
        }
      });

      return Math.max(...zis);
    },
  },
};
