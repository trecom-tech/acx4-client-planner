/**
 * Common page layout components
 */

import Announcement from '@/core/components/Announcement.vue';
import Breadcrumbs from '@/core/components/Breadcrumbs.vue';
import ContentSection from '@/core/components/ContentSection.vue';
import Island from '@/core/components/Island.vue';

export default {
  components: {
    Announcement,
    Breadcrumbs,
    ContentSection,
    Island,
  },
};
