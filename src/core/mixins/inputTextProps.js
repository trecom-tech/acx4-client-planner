export default {
  props: {
    maxlength: {
      type: [String, Number],
      default: null,
    },
    autocomplete: {
      type: String,
      default: 'off',
    },
    mask: {
      type: Object,
      default: null,
      // mask
      // prepare: Function,
      // validate: Function,
      // commit: Function,

      // Pattern
      // placeholderChar: String,
      // lazy: Boolean,
      // definitions: Object,
      // blocks: Object,

      // Date
      // pattern: String,
      // format: Function,
      // parse: Function,

      // Number
      // radix: String,
      // thousandsSeparator: String,
      // mapToRadix: Array,
      // scale: Number,
      // signed: Boolean,
      // normalizeZeros: Boolean,
      // padFractionalZeros: Boolean,
      // min: [Number, Date],
      // max: [Number, Date],

      // Dynamic
      // dispatch: Function
    },
    unmask: {
      type: [Boolean, String],
      default: true,
      validator(value) {
        return value === 'typed' || typeof value === 'boolean';
      },
    },
    maskPlaceholder: {
      type: String,
      default: '',
    },
  },
};
