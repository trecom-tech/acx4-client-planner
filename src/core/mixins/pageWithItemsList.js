import jump from 'jump.js';
import { mapGetters } from 'vuex';

import DataTable from '@/core/components/DataTable.vue';
import Paginator from '@/core/components/Paginator.vue';
import {
  capitalize,
  deepClone,
  isNumber,
  debounce,
} from '@/core/helpers/utils';

const ITEMS_FETCH_TIMEOUT = 1000 * 10;
const COUNT_FETCH_TIMEOUT = 1000 * 20;

export default {
  components: {
    DataTable,
    Paginator,
  },
  data() {
    return {
      // Config to extend
      apiEndpoint: '',
      itemName: 'item',
      // Query params
      sortBy: 'createdAt',
      sortOrder: -1,
      search: '',
      pageNum: 1,
      pageSize: 25,
      filters: {},
      extraQueryParams: {}, // Params which add for each request
      // Items
      items: [],
      itemsCount: 0,
      // Time labels
      itemsRequestedAt: 0,
      itemsFetchedAt: 0,
      itemsCountFetchedAt: 0,
      // Flags
      itemDeactivating: false,
      itemsFetching: false,
      currentQuery: null,
      keepAlive: false,
      keepAliveActive: false,
    };
  },
  computed: {
    activeFilters() {
      const filters = Object.keys(this.filters);
      const active = [];
      filters.forEach((key) => {
        if (this.filters[key] || this.filters[key] === false) {
          active.push({
            key,
            value: this.filters[key],
          });
        }
      });
      return active;
    },
    isActiveFilter() {
      return this.activeFilters.length > 0;
    },
    pages() {
      return this.itemsCount ? Math.ceil(this.itemsCount / this.pageSize) : 0;
    },
    loading() {
      return this.itemsFetching || this.itemDeactivating;
    },
    activeWarehouse() {
      return this.$store.getters['warehouse/active'];
    },
    ...mapGetters({
      getPageSize: 'ui/getProperty',
    }),
  },
  watch: {
    $route() {
      if (!this.keepAliveActive) return;
      this.load();
    },
  },
  watchQuery: true, // Watch for all queries changes
  mounted() {
    if (this.keepAlive) return;
    this.load();
  },
  activated() {
    if (this.keepAlive) {
      this.load();
    }

    this.keepAlive = true;
    this.keepAliveActive = true;
  },
  deactivated() {
    this.keepAliveActive = false;
  },
  methods: {
    sortItems({ sortBy, sortOrder }) {
      this.pageNum = 1;
      this.sortBy = sortBy;
      this.sortOrder = sortOrder;
      this.updateRoute();
      this.fetchItems();
    },
    paginateItems(pageNum) {
      this.pageNum = pageNum;
      this.updateRoute();
      this.fetchItems();
      const scrollTop = window.pageYOffset || document.documentElement.scrollTop;
      const top = this.$el.getBoundingClientRect().top + scrollTop;
      if (scrollTop - top > 0) {
        jump(this.$el, { duration: 0 });
      }
    },
    changePageSize(pageSize) {
      this.pageSize = pageSize;
      this.updateRoute();
    },
    filterItems() {
      this.updateRoute();
      this.fetchItems(true);
    },
    resetFilters() {
      Object.keys(this.filters).forEach((key) => {
        // TODO: reset depend on type
        this.filters[key] = null;
      });
      this.updateRoute();
      this.fetchItems(true);
    },
    resetFilter(key) {
      // TODO: reset depend on type
      this.filters[key] = null;
      this.updateRoute();
      this.fetchItems(true);
    },
    searchItems: debounce(function searchItems() {
      this.updateRoute();
      this.fetchItems(true);
    }, 400),
    resetSearch() {
      this.search = '';
      this.updateRoute();
      this.fetchItems(true);
    },
    getRequestParams(fetchCount) {
      const params = {
        pageSize: this.pageSize,
        pageNum: this.pageNum,
      };

      if (this.sortBy) {
        params.sortBy = this.sortBy;
        params.sortOrder = this.sortOrder;
      }

      if (this.search) {
        params.search = this.search;
      }

      Object.keys(this.filters).forEach((key) => {
        if (this.filters[key] || this.filters[key] === false) {
          params[key] = this.filters[key];
        }
      });

      if (fetchCount) {
        params.pageCount = 1;
      }

      return Object.assign({}, params, this.extraQueryParams);
    },
    load() {
      const now = Date.now();

      // Remember previous query
      const lastQuery = deepClone(this.currentQuery);

      this.parseUrl();

      // Check if query changed
      let queryChanged = false;
      let refetchCount = false;

      // Fetch items first time and with the same query but after delay
      if (!lastQuery || (now - this.itemsFetchedAt) > ITEMS_FETCH_TIMEOUT) {
        queryChanged = true;
        refetchCount = true;
      } else {
        const params = Object.keys(this.currentQuery);

        let i = 0;
        const last = params.length - 1;

        while (i <= last && queryChanged !== true) {
          if (this.currentQuery[params[i]] !== lastQuery[params[i]]) {
            queryChanged = true;
            // Refetch items count if something other than pageNum changed
            if (params[i] !== 'pageNum' && params[i] !== 'pageSize') {
              refetchCount = true;
            }
          }
          i++;
        }
      }

      if (queryChanged) {
        refetchCount = (
          refetchCount || (now - this.itemsCountFetchedAt > COUNT_FETCH_TIMEOUT)
        );
        this.fetchItems(refetchCount);
      }
    },
    fetchItems(fetchCount) {
      if (!this.apiEndpoint) {
        console.warn(`API endpoint not set for ${this.$options.name} component`);
        return;
      }

      const itemsRequestedAt = Date.now();
      this.itemsRequestedAt = itemsRequestedAt;

      this.itemsFetching = true;

      const params = this.getRequestParams(fetchCount);

      this.$api.get(this.apiEndpoint, { params })
        .then(({ data }) => {
          if (itemsRequestedAt !== this.itemsRequestedAt) return;

          const now = Date.now();

          if (isNumber(data.count)) {
            this.itemsCount = parseInt(data.count, 10);
            this.itemsCountFetchedAt = now;
          }

          this.itemsFetchedAt = now;
          this.fillItems(data.data);
          this.itemsFetching = false;
          this.updateCurrentQuery();
        })
        .catch(() => {
          if (itemsRequestedAt !== this.itemsRequestedAt) return;
          this.itemsFetching = false;
          this.$toasts.push({
            theme: 'danger',
            title: 'Error',
            text: 'could not fetch items list',
          });
        });
    },
    fillItems(items) {
      this.items.splice(0, this.items.length, ...items);
    },
    deactivateItem(item) {
      if (this.itemDeactivating) return;

      // TODO: custom text template

      this.$confirm({
        text: `Deactivate ${this.itemName}?`,
        confirmLabel: this.$t('controls.deactivate'),
        onConfirm: () => {
          this.itemDeactivating = true;
          this.$api.delete(this.apiEndpoint + item.id)
            .then(() => {
              this.itemDeactivating = false;
              this.$toasts.push({
                theme: 'success',
                text: `${capitalize(this.itemName)} deactivated successfully`,
              });

              this.$nextTick(() => {
                this.fetchItems(true);
              });
            })
            .catch(() => {
              this.$toasts.push({
                theme: 'danger',
                title: 'Error',
                text: `Could not deactivate ${this.itemName}`,
              });
              this.itemDeactivating = false;
            });
        },
      });
    },
    parseUrl() {
      const query = this.$route.query;
      const pageNum = parseInt(query.pageNum, 10);
      const sortOrder = parseInt(query.sortOrder, 10);

      this.pageNum = !Number.isNaN(pageNum) ? pageNum : 1;
      this.sortBy = query.sortBy || this.sortBy;
      this.sortOrder = !Number.isNaN(sortOrder) ? sortOrder : this.sortOrder;
      this.search = query.search || '';

      Object.keys(query).forEach((key) => {
        if (typeof this.filters[key] !== 'undefined') {
          let value = query[key];

          // TODO: smart parsing to Number, Boolean, Array

          if (value === 'true') {
            value = true;
          } else if (value === 'false') {
            value = false;
          }

          this.filters[key] = value;
        }
      });

      this.updateCurrentQuery();
    },
    updateRoute() {
      const {
        name,
        hash,
        params,
      } = this.$route;

      const query = deepClone(this.$route.query);
      query.pageNum = this.pageNum;

      if (this.search) {
        query.search = this.search;
      } else {
        delete query.search;
      }

      if (this.sortBy) {
        query.sortBy = this.sortBy;
        query.sortOrder = this.sortOrder;
      } else {
        delete query.sortBy;
        delete query.sortOrder;
      }

      Object.keys(this.filters).forEach((key) => {
        if (this.filters[key] || this.filters[key] === false) {
          query[key] = this.filters[key];
        } else {
          delete query[key];
        }
      });

      this.$router.replace({
        name,
        hash,
        params,
        query,
      });
    },
    updateCurrentQuery() {
      this.currentQuery = {
        pageSize: this.pageSize,
        pageNum: this.pageNum,
        sortBy: this.sortBy,
        sortOrder: this.sortOrder,
        search: this.search,
        ...this.filters,
        ...this.extraQueryParams,
      };
    },
  },
};
