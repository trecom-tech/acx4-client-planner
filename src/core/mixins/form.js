import { getPathValue } from 'pathval';
import jump from 'jump.js';
import { withParams } from 'vuelidate/lib/validators/common';

import ButtonsGroup from '@/core/components/ButtonsGroup.vue';
import ControlsGroup from '@/core/components/ControlsGroup.vue';
import Checkbox from '@/core/components/Checkbox.vue';
import CheckboxGroup from '@/core/components/CheckboxGroup.vue';
import FieldLabel from '@/core/components/FieldLabel.vue';
import {
  InputDate,
  InputDateTime,
  InputTime,
} from '@/core/components/InputDateTime';
import InputNumber from '@/core/components/InputNumber.vue';
import InputPassword from '@/core/components/InputPassword.vue';
import InputText from '@/core/components/InputText.vue';
import Multiselect from '@/core/components/Multiselect.vue';
import MultiselectAjax from '@/core/components/MultiselectAjax.vue';
import Notice from '@/core/components/Notice.vue';
import Radio from '@/core/components/Radio.vue';
import RadioGroup from '@/core/components/RadioGroup.vue';
import VButton from '@/core/components/VButton.vue';
import VFieldset from '@/core/components/VFieldset.vue';
import {
  deepClone,
  deepAssignByShape,
  findClosestElement,
  isObject,
  areObjectsPropsEqual,
} from '@/core/helpers/utils';

const SCROLL_TO_INVALID_OFFSET = -150;
const FIELDS_PATH = 'fields';
const ERROR_RESPONSE_CODE_PATH = 'response.data.code';
const ERROR_RESPONSE_MESSAGE_PATH = 'response.data.message';
const ERROR_RESPONSE_DETAILS_PATH = 'response.data.details';
const ERROR_RESPONSE_STATUS_PATH = 'response.status';
const INVALID_FIELD_SELECTOR = '._invalid';

export default {
  props: {
    // Initial (mean saved) form fields state
    values: {
      type: Object,
      default: null,
    },
  },
  components: {
    ButtonsGroup,
    ControlsGroup,
    Checkbox,
    CheckboxGroup,
    FieldLabel,
    InputDate,
    InputDateTime,
    InputNumber,
    InputPassword,
    InputText,
    InputTime,
    Multiselect,
    MultiselectAjax,
    Notice,
    Radio,
    RadioGroup,
    VButton,
    VFieldset,
  },
  data() {
    return {
      fields: {},
      fieldsBeforeEdit: null,
      sending: false,
      serverError: null,
    };
  },
  computed: {
    valid() {
      const { validated, valid } = this.getFormValidationState();
      return validated && valid;
    },
    invalid() {
      return !this.valid;
    },
    dirty() {
      if (!this.fieldsBeforeEdit) {
        return false;
      }
      return !areObjectsPropsEqual(this.fields, this.fieldsBeforeEdit);
    },
    // Trick to have correct oldValue in watcher
    computedValues() {
      return deepClone(this.values);
    },
    computedFields() {
      return deepClone(this.fields);
    },
  },
  validations: {},
  watch: {
    computedValues: {
      handler(val, oldVal) {
        if (areObjectsPropsEqual(val, oldVal)) return;
        this.fill(val);
      },
      deep: true,
    },
    computedFields: {
      handler(val, oldVal) {
        if (areObjectsPropsEqual(val, oldVal)) return;
        this.$emit('change', val);
      },
      deep: true,
    },
    dirty(val, oldVal) {
      if (val === oldVal) return;
      this.$emit('dirty', val);
    },
  },
  beforeMount() {
    this.fill(this.values);
  },
  methods: {
    createValidationRule(validator, message) {
      return withParams({ type: 'validationRule', message }, validator);
    },
    getFieldValidationState(path) {
      // Field valid from client side if it exist in validation model
      // and pass validation or if it doesn't exist
      const field = getPathValue(this.$v, `${FIELDS_PATH}.${path}`);
      const clientValid = (field && field.$anyDirty && !field.$invalid) || !field;

      // Field valid from server side if we have no server errors for this field
      const serverValid = (
        !this.serverError
        || !this.serverError.details
        || !this.serverError.details[path]
      );

      return {
        validated: (field && field.$anyDirty) || !serverValid,
        valid: clientValid && serverValid,
      };
    },
    getFormValidationState() {
      return {
        validated: this.$v.$dirty || !this.serverError,
        valid: !this.$v.$invalid && !this.serverError,
      };
    },
    getFieldErrorMessages(path) {
      const messages = [];

      // Add server errors first
      const serverError = (
        this.serverError
        && this.serverError.details
        && this.serverError.details[path]
      );

      if (serverError) {
        if (Array.isArray(serverError)) {
          messages.push(...serverError);
        } else {
          messages.push(serverError);
        }
      }

      // Skip valid fields
      const field = getPathValue(this.$v, `${FIELDS_PATH}.${path}`);
      if (!field || !field.$anyDirty) return messages;

      // All field validators must be created with validationRule helper
      // Walk trough field.$params to get all validators and their messages
      Object.keys(field.$params).forEach((key) => {
        // Skip valid fields
        if (field[key]) return;

        //  Skip fields without message
        if (!field.$params[key].message) return;

        const params = field.$params[key];
        const message = params.message;

        // Return string message
        if (typeof message === 'string') {
          messages.push(message);
          return;
        }

        if (typeof message === 'function') {
          const messageParams = {};

          // Original validators placed in $sub property
          if (params.$sub) {
            // Walk true these validators and collect their params to use with message function
            // Only 1 level allowed
            params.$sub.forEach((sub) => {
              Object.keys(sub).forEach((key) => {
                messageParams[key] = sub[key];
              });
            });
          }

          messages.push(message(messageParams));
        }
      });
      return messages;
    },
    isFieldValid(path) {
      const { validated, valid } = this.getFieldValidationState(path);
      return validated && valid;
    },
    isFieldInvalid(path) {
      const { validated, valid } = this.getFieldValidationState(path);
      return validated && !valid;
    },
    focusFirstInvalidField() {
      const firstInvalidField = this.$el.querySelector(INVALID_FIELD_SELECTOR);
      if (firstInvalidField) {
        const scrollableParent = findClosestElement(firstInvalidField, (el) => {
          if (el === document.body) return true;

          const overflow = window.getComputedStyle(el).overflowY;

          return overflow === 'scroll' || overflow === 'auto';
        });

        if (scrollableParent) {
          if (scrollableParent === document.body) {
            jump(firstInvalidField, {
              offset: SCROLL_TO_INVALID_OFFSET,
              callback: () => {
                firstInvalidField.focus();
              },
            });
          } else {
            // TODO: smooth scroll, focus on complete
            firstInvalidField.focus();
          }
        }
      }
    },
    parseServerErrorResponse(err) {
      const code = getPathValue(err, ERROR_RESPONSE_CODE_PATH);
      const details = getPathValue(err, ERROR_RESPONSE_DETAILS_PATH);
      let message = getPathValue(err, ERROR_RESPONSE_MESSAGE_PATH);

      // Clear error for field when values changes
      if (isObject(details)) {
        Object.keys(details).forEach((path) => {
          const unwatch = this.$watch(`${FIELDS_PATH}.${path}`, () => {
            this.serverError.details[path] = null;
            unwatch();
          });
        });
      }

      if (!message) {
        const status = getPathValue(err, ERROR_RESPONSE_STATUS_PATH) || 500;

        if (status === 500) {
          message = 'Internal server error';
        }
        // TODO: other statuses
        // TODO: i18n
      }

      this.serverError = {
        code,
        message,
        details,
      };

      this.$nextTick(() => {
        this.focusFirstInvalidField();
      });
    },
    validate() {
      this.serverError = null;
      this.$v.$touch();

      if (!this.valid) {
        this.$nextTick(() => {
          this.focusFirstInvalidField();
        });
        return false;
      }

      return true;
    },
    fill(values) {
      if (values) {
        this.fillFormFields(this.fields, values);
      }

      this.fieldsBeforeEdit = deepClone(this.fields);
    },
    fillFormFields(fields, values) {
      deepAssignByShape(fields, values);
    },
    submit() {
      if (this.sending) return;
      if (!this.validate()) return;

      this.$emit('submit');
      this.sending = true;

      // Make request...

      // on success:
      // this.$emit('success');
      // this.fill(data);

      // on error:
      // this.$emit('fail');

      this.sending = false;
    },
    reset() {
      this.fill(this.fieldsBeforeEdit);
    },
    cancel() {
      this.$emit('cancel');
    },
  },
  provide() {
    return {
      isFieldValid: this.isFieldValid,
      isFieldInvalid: this.isFieldInvalid,
      getFieldErrorMessages: this.getFieldErrorMessages,
    };
  },
};
