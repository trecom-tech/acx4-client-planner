export default {
  props: {
    name: {
      type: String,
      default: null,
    },
    placeholder: {
      type: String,
      default: '',
    },
    required: {
      type: Boolean,
      default: false,
    },
    disabled: {
      type: Boolean,
      default: false,
    },
    readonly: {
      type: Boolean,
      default: false,
    },
    invalid: {
      type: Boolean,
      default: false,
    },
    loading: {
      type: Boolean,
      default: false,
    },
    errors: {
      type: Array,
      default: () => ([]),
    },
    label: {
      type: String,
      default: '',
    },
    advice: {
      type: [String, Number],
      default: '',
    },
    autofocus: {
      // TODO: support .select modifier
      type: Boolean,
      default: false,
    },
  },
};
