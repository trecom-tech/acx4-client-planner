/**
 * Ask before update/leave route or close/reload page if form is dirty
 */

export default {
  data() {
    return {
      oldOnBeforeUnload: null,
      dirty: false,
      leaveAllowed: false, // in case of redirect
    };
  },
  mounted() {
    this.oldOnBeforeUnload = window.onbeforeunload;
    window.onbeforeunload = this.onBeforeUnload;
  },
  beforeDestroy() {
    window.onbeforeunload = this.oldOnBeforeUnload;
  },
  beforeRouteUpdate(to, from, next) {
    this.allowRouteNavigation(to, from, next);
  },
  beforeRouteLeave(to, from, next) {
    this.allowRouteNavigation(to, from, next);
  },
  methods: {
    onFormDirty(dirty) {
      this.dirty = dirty;
    },
    onBeforeUnload() { // eslint-disable-line consistent-return
      if (this.dirty) {
        return '';
      }
    },
    allowRouteNavigation(to, from, next) {
      if (this.leaveAllowed || !this.dirty) {
        next();
        return;
      }

      this.$confirm({
        text: 'Leave page? <br>Changes that you made may not be saved.',
        confirmLabel: 'Leave',
        onConfirm: () => {
          this.leaveAllowed = true;
          next();
        },
        onCancel: () => {
          next(false);
        },
      });
    },
  },
};
