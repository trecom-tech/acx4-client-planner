export default {
  props: {
    activeClass: {
      type: String,
      default: '',
    },
    append: {
      type: Boolean,
      default: false,
    },
    disabled: {
      type: Boolean,
      default: false,
    },
    exact: {
      type: Boolean,
      default: undefined,
    },
    exactActiveClass: {
      type: String,
      default: '',
    },
    href: {
      type: [String, Object],
      default: '',
    },
    to: {
      type: [String, Object],
      default: '',
    },
    replace: {
      type: Boolean,
      default: false,
    },
    tag: {
      type: String,
      default: '',
    },
    target: {
      type: String,
      default: '',
    },
  },
  methods: {
    click() {},
    generateRouteLink() {
      let exact = this.exact;
      let tag;

      const data = {
        attrs: { disabled: this.disabled },
        class: this.classes,
        props: {},
        [this.to ? 'nativeOn' : 'on']: {
          ...this.$listeners,
          click: this.click,
        },
      };

      if (typeof this.exact === 'undefined') {
        exact = this.to === '/' ||
          (this.to === Object(this.to) && this.to.path === '/');
      }

      if (this.to) {
        const activeClass = this.activeClass;
        const exactActiveClass = this.exactActiveClass || activeClass;

        tag = 'router-link';

        Object.assign(data.props, {
          to: this.to,
          exact,
          activeClass,
          exactActiveClass,
          append: this.append,
          replace: this.replace,
        });
      } else {
        tag = (this.href && 'a') || this.tag || 'a';

        if (tag === 'a' && this.href) {
          data.attrs.href = this.href;
        }
      }

      if (this.target) {
        data.attrs.target = this.target;
      }

      return { tag, data };
    },
  },
};
