import FieldDrop from '@/core/components/FieldDrop.vue';

const props = {
  dropMaxHeight: {
    type: Number,
    default: 240,
  },
  options: {
    type: Array,
    default: null,
  },
  optionsLoading: {
    type: Boolean,
    default: false,
  },
  optionsError: {
    type: Boolean,
    default: false,
  },
  optionsErrorMessage: {
    type: String,
    default: '',
  },
  optionLabel: {
    type: [String, Function],
    default: null,
  },
  optionValue: {
    type: [String, Function],
    default: null,
  },
  optionsLimit: {
    type: Number,
    default: 1000,
  },
};

export const onlyProps = { props };

export default {
  components: {
    FieldDrop,
  },
  props,
  data() {
    return {
      focused: false,
      dropActive: false,
      dropAbove: false,
      dropContentEl: null,
      pointer: 0,
    };
  },
  computed: {
    classes() {
      return {
        /* eslint-disable quote-props */
        '_focused': this.focused || this.dropActive,
        '_disabled': this.disabled,
        '_invalid': this.invalid,
        '_readonly': this.readonly,
        '_drop-below': this.dropActive && !this.dropAbove,
        '_drop-above': this.dropActive && this.dropAbove,
        'input-text_textarea': this.type === 'textarea',
        /* eslint-enable quote-props */
      };
    },
    hasDrop() {
      if (this.$slots.drop) {
        return true;
      }

      return (
        (this.options && this.options.length > 0)
        || this.optionsLoading
        || this.optionsError
      );
    },
  },
  watch: {
    hasDrop(val, oldVal) {
      if (val === oldVal) return;

      this.$nextTick(() => {
        if (this.focused && !this.dropActive) {
          this.activateDrop();
        }
      });
    },
  },
  beforeDestroy() {
    this.deactivateDrop();
  },
  methods: {
    // Options
    getOptionLabel(option) {
      if (!this.optionLabel) {
        return option;
      }

      if (typeof this.optionLabel === 'function') {
        return this.optionLabel(option);
      }

      return option[this.optionLabel];
    },
    getOptionValue(option) {
      if (!this.optionValue) {
        return option;
      }

      if (typeof this.optionValue === 'function') {
        return this.optionValue(option);
      }

      return option[this.optionValue];
    },
    selectOption(option) {
      this.mutableValue = this.getOptionValue(option);
      this.deactivateDrop();
      this.$emit('input', this.mutableValue);
    },
    getOptionClasses(option, index) {
      return {
        /* eslint-disable quote-props */
        '_highlight': index === this.pointer,
        '_disabled': option.$disabled,
        /* eslint-enable quote-props */
      };
    },
    // Drop
    getDropInnerEl() {
      // TODO: find better way to access element
      return this.$refs.drop
        ? this.$refs.drop.$refs.contentInner
        : null;
    },
    activateDrop() {
      if (this.dropActive || !this.focused) return;
      this.dropActive = true;
      this.pointerReset();
    },
    deactivateDrop() {
      if (!this.dropActive) return;
      this.dropActive = false;
    },
    adjustDropScroll() {
      const list = this.$refs.options;
      const drop = this.getDropInnerEl();

      if (!list || !drop) return;

      const dropHeight = drop.offsetHeight;
      const scroll = drop.scrollTop;
      const options = Array.prototype.slice.call(list.children);

      // Collect highlighted options height and list height before
      let beforeHeight = 0;
      for (let i = 0; i < this.pointer; i++) {
        beforeHeight += options[i].offsetHeight;
      }

      const highlightedHeight = options[this.pointer].offsetHeight;

      if (scroll + dropHeight <= beforeHeight + highlightedHeight) {
        drop.scrollTop = (beforeHeight + highlightedHeight) - dropHeight;
        return;
      }

      if (scroll >= beforeHeight) {
        drop.scrollTop = beforeHeight;
      }
    },
    adjustDrop() {
      // Tricky, but...
      if (this.$refs.drop && typeof this.$refs.drop.adjustContent === 'function') {
        this.$refs.drop.adjustContent();
      }
    },
    onDropAdjust(position) {
      this.dropAbove = position === 'above';
    },
    onDropClickOut(e) {
      if (!this.dropActive) return;
      const path = e.path || (e.composedPath ? e.composedPath() : undefined);

      if (path ? path.indexOf(this.$refs.body) < 0 : !this.$refs.body.contains(e.target)) {
        this.deactivateDrop();
      }
    },
    onDropActivate() {
      this.$emit('drop-activate');
    },
    onDropActivated() {
      this.$emit('drop-activated');
    },
    onDropDeactivate() {
      this.$emit('drop-deactivate');
    },
    onDropDeactivated() {
      this.$emit('drop-deactivated');
    },
    onFocus(e) {
      this.focused = true;
      if (this.hasDrop) {
        this.activateDrop();
      }
      this.$emit('focus', e);
    },
    onBlur(e) {
      this.focused = false;
      this.$emit('blur', e);

      // We have e.relatedTarget if after blur focus come to any element.
      // In this case we can check if target inside drop element.
      // Otherwise, if we just click somewhere, onDropClickOut handler will manage it.
      if (this.dropActive && e.relatedTarget) {
        const el = this.getDropInnerEl();
        if (el && !el.contains(e.relatedTarget)) {
          this.deactivateDrop();
        }
      }
    },
    onUp(e) {
      if (!this.dropActive || !this.options) return;
      e.preventDefault();
      this.pointerBackward();
    },
    onDown(e) {
      if (!this.dropActive || !this.options) return;
      e.preventDefault();
      this.pointerForward();
    },
    onEnter(e) {
      if (!this.dropActive) return;
      e.preventDefault();
      e.stopPropagation();
      this.addPointerElement();
    },
    onEsc(e) {
      if (!this.dropActive) return;
      e.preventDefault();
      e.stopPropagation();
      this.unfocus();
      this.deactivateDrop();
    },
    // Pointer
    pointerSet(index) {
      this.pointer = index;
    },
    pointerForward() {
      if (this.pointer >= this.options.length - 1) return;

      const tryPointer = (pointer) => {
        if (!this.options[pointer].$disabled) {
          this.pointer = pointer;
          this.adjustDropScroll();
          return;
        }

        if (pointer < this.options.length - 1) {
          tryPointer(pointer + 1);
        }
      };

      tryPointer(this.pointer + 1);
    },
    pointerBackward() {
      if (this.pointer <= 0) return;

      const tryPointer = (pointer) => {
        if (!this.options[pointer].$disabled) {
          this.pointer = pointer;
          this.adjustDropScroll();
          return;
        }

        if (pointer > 0) {
          tryPointer(pointer - 1);
        }
      };

      tryPointer(this.pointer - 1);
    },
    pointerReset() {
      this.pointer = 0;
      const el = this.getDropInnerEl();
      if (el) {
        el.scrollTop = 0;
      }
    },
    addPointerElement() {
      if (!this.dropActive) return;
      if (this.options && this.options.length > 0) {
        this.selectOption(this.options[this.pointer]);
      }
    },
  },
};
