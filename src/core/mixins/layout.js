import { compile } from 'path-to-regexp';

export default {
  computed: {
    routerViewKey() {
      // If current route has children
      if (this.$route.matched.length > 1) {
        return compile(this.$route.matched[0].path)(this.$route.params);
      }

      return this.$route.path;
    },
    loading() {
      return this.$app.loading;
    },
    loadingProgress() {
      return this.$app.loadingProgress;
    },
    loadingFailed() {
      return this.$app.loadingFailed;
    },
  },
};
