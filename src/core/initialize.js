import account from '@/modules/account';
import auth from '@/modules/auth';
import dashboard from '@/modules/dashboard';
import design from '@/modules/design';
import hauler from '@/modules/hauler';
import logistic from '@/modules/logistic';
import purchasing from '@/modules/purchasing';
import sales from '@/modules/sales';
import ui from '@/modules/ui';
import user from '@/modules/user';
import warehouse from '@/modules/warehouse';
import { registerModule } from '@/core/lib/modules';

/**
 * This function runs before App mount and first route navigation
 */

export default async function initialize(context) {
  const { store, router } = context;

  const modules = [
    account,
    auth,
    dashboard,
    hauler,
    logistic,
    purchasing,
    sales,
    ui,
    user,
    warehouse,
  ];

  if (process.env.NODE_ENV === 'development') {
    modules.push(design);
  }

  // Register modules
  modules.forEach(module => registerModule(module, context));

  // TODO: skip parts depend on render side
  // TODO: check error

  const fetchWarehouses = async () => store.dispatch('warehouse/fetchList');

  // Subscribe mutations
  store.subscribe((mutation) => {
    if (mutation.type === 'user/LOGIN') {
      fetchWarehouses().catch((err) => {
        // TODO: how to handle?
        console.log(err);
      });
      return;
    }

    if (mutation.type === 'user/LOGOUT') {
      // TODO: clear store
      router.push({
        name: 'Auth',
      });
    }
  });

  // Set language
  // const lang = (
  //   store.state.i18n.lang || (ssrContext ? ssrContext.lang : navigator.language)
  // );
  await store.dispatch('setLang', 'en');

  // Update user authorization
  if (store.state.user.refreshToken) {
    try {
      await store.dispatch('user/refreshToken');
    } catch (err) {
      // Resolve anyway, we don't need to break render here
      return Promise.resolve();
    }
  }

  if (!store.state.user.accessToken) {
    return Promise.resolve();
  }

  await fetchWarehouses();

  return Promise.resolve();
}
