import Vue from 'vue';

import PageScroll from '@/core/helpers/PageScroll';
import LoadingComponent from './Loading.vue';

// TODO: ssr

const TRANSITION_DURATION = 300;

const Loading = Vue.extend(LoadingComponent);

const insertDom = (parent, el, binding) => {
  // Set position relative to parent element
  if (el.originalPosition !== 'absolute' && el.originalPosition !== 'fixed') {
    parent.style.position = 'relative';
  }

  // Lock page scroll
  if (binding.modifiers.fullscreen && binding.modifiers.lock) {
    PageScroll.lock('Loading');
  }

  if (!el.domInserted) {
    parent.appendChild(el.Loading);
  }

  Vue.nextTick(() => {
    if (el.instance.hiding) {
      el.instance.$emit('after-leave');
    } else {
      el.instance.visible = true;
    }
  });

  el.domInserted = true;
};

const toggleLoading = (el, binding) => {
  if (binding.value) {
    Vue.nextTick(() => {
      const parent = binding.modifiers.fullscreen ? document.body : el;
      const originalPosition = window.getComputedStyle(parent).position;
      el.originalPosition = originalPosition !== 'static' ? originalPosition : '';
      insertDom(parent, el, binding);
    });
    return;
  }

  let afterLeaveCalled = false;

  const afterLeaveCallback = () => {
    if (afterLeaveCalled) return;
    afterLeaveCalled = true;

    // Restore parent position style, unlock scroll
    const parent = binding.modifiers.fullscreen ? document.body : el;
    parent.style.position = el.originalPosition;
    if (binding.modifiers.fullscreen && binding.modifiers.lock) {
      PageScroll.unlock('Loading');
    }

    el.instance.hiding = false;
  };

  el.instance.$once('after-leave', afterLeaveCallback);

  // Just in case
  setTimeout(() => {
    afterLeaveCallback();
  }, TRANSITION_DURATION + 100);

  el.instance.visible = false;
  el.instance.hiding = true;
};

export default {
  bind(el, binding) {
    let size = '';

    if (binding.modifiers.small) {
      size = 'small';
    }

    const instance = new Loading({
      el: document.createElement('div'),
      data: {
        fullscreen: !!binding.modifiers.fullscreen,
        size,
      },
    });

    el.instance = instance;
    el.Loading = instance.$el;

    if (binding.value) {
      toggleLoading(el, binding);
    }
  },

  update(el, binding) {
    if (binding.oldValue !== binding.value) {
      toggleLoading(el, binding);
    }
  },

  unbind(el, binding) {
    if (el.domInserted) {
      if (el.Loading && el.Loading.parentNode) {
        el.Loading.parentNode.removeChild(el.Loading);
      }
      toggleLoading(el, { value: false, modifiers: binding.modifiers });
    }
  },
};
