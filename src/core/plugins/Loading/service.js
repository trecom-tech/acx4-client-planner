import Vue from 'vue';

import PageScroll from '@/core/helpers/PageScroll';
import LoadingComponent from './Loading.vue';

const TRANSITION_DURATION = 300;

const Loading = Vue.extend(LoadingComponent);

class LoadingService {
  constructor() {
    this.Loading = null;
    this.originalOptions = null;
  }

  show(options) {
    if (Vue.prototype.$isServer) return;

    if (this.Loading) return;

    this.originalOptions = Object.assign({ lock: true }, options);

    this.Loading = new Loading({
      el: document.createElement('div'),
      data: this.originalOptions,
    });

    if (this.originalOptions.lock) {
      PageScroll.lock('Loading');
    }

    document.body.appendChild(this.Loading.$el);

    Vue.nextTick(() => {
      this.Loading.visible = true;
    });
  }

  hide() {
    let afterLeaveCalled = false;

    const afterLeaveCallback = () => {
      if (afterLeaveCalled) return;
      afterLeaveCalled = true;

      if (this.originalOptions.lock) {
        PageScroll.unlock('Loading');
      }

      if (this.Loading.$el && this.Loading.$el.parentNode) {
        this.Loading.$el.parentNode.removeChild(this.Loading.$el);
      }

      this.Loading.$destroy();
      this.Loading = null;
    };

    this.Loading.$once('after-leave', afterLeaveCallback);

    // Just in case
    setTimeout(() => {
      afterLeaveCallback();
    }, TRANSITION_DURATION + 100);

    this.Loading.visible = false;
  }
}

export default new LoadingService();
