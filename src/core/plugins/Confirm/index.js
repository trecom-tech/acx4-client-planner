import uuid from 'uuid';

import eventBus from './eventBus';
import Confirm from './Confirm.vue';

export default {
  install(Vue) {
    Vue.component('Confirm', Confirm);
    Vue.prototype.$confirm = (config) => {
      const item = Object.assign({}, config, {
        id: uuid.v1(),
      });
      eventBus.$emit('push-confirm', item);
      return item;
    };
  },
};
