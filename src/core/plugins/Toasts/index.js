import uuid from 'uuid';

import eventBus from './eventBus';
import Toasts from './Toasts.vue';

export default {
  install(Vue) {
    Vue.component('Toasts', Toasts);
    Vue.prototype.$toasts = {
      push(toast) {
        const item = Object.assign({}, toast, {
          id: uuid.v1(),
        });
        eventBus.$emit('push-toast', item);
        return item;
      },
      remove(toast) {
        eventBus.$emit('remove-toast', toast);
      },
    };
  },
};
