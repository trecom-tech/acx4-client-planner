import { DateTime } from 'luxon';

import InputText from '@/core/components/InputText.vue';
import inputProps from '@/core/mixins/inputProps';
import TIMEZONES from '@/core/constants/timezones';

// TODO: optional displayFormat
// TODO: mask, maskPlaceholder, displayPlaceholder depend on displayFormat
// TODO: convert value to used format
// TODO: allowed / disabled dates

export default {
  components: {
    InputText,
  },
  mixins: [inputProps],
  props: {
    value: {
      type: [String, Number],
      default: null,
      // TODO: validate
    },
    valueType: {
      type: String,
      default: 'timestamp',
      validator: value => !value || ['string', 'timestamp', 'iso'].includes(value),
    },
    timezone: {
      type: String,
      default: '',
      validator: value => !value || TIMEZONES.includes(value),
    },
  },
  data() {
    return {
      displayFormat: 'dd.MM.yyyy',
      mutableValue: '',
      inputValue: '',
      focused: false,
    };
  },
  computed: {
    isStringValueType() {
      return this.valueType !== 'timestamp' && this.valueType !== 'iso';
    },
    isTimestampValueType() {
      return this.valueType === 'timestamp';
    },
    isIsoValueType() {
      return this.valueType === 'iso';
    },
    isTimezoneValid() {
      return !this.timezone || TIMEZONES.includes(this.timezone);
    },
    mask() {
      return {
        mask: '00.00.0000',
      };
    },
    maskPlaceholder() {
      return 'DD.MM.YYYY';
    },
    displayPlaceholder() {
      return this.placeholder || this.maskPlaceholder;
    },
    displayValue() {
      if (this.isStringValueType) {
        return this.mutableValue;
      }

      if (this.focused) {
        return this.inputValue;
      }

      return this.stringValue;
    },
    stringValue() {
      if (!this.mutableValue) {
        return '';
      }

      const dateTime = this.valueToDateTime(this.mutableValue);
      // TODO: if invalid?
      return dateTime.toFormat(this.displayFormat);
    },

  },
  watch: {
    value(val, oldVal) {
      if (val === oldVal) return;
      this.parseValue();
    },
  },
  beforeMount() {
    this.parseValue();
  },
  methods: {
    focus() {
      this.$refs.input.focus();
    },
    focusAndSelect() {
      this.$refs.input.focusAndSelect();
    },
    parseValue() {
      // TODO: validate value, convert to valueType format if it's possible

      // String type is simple
      if (this.isStringValueType) {
        this.mutableValue = this.value;
        return;
      }

      if (!this.value) {
        // We had mutableValue -> we had finished input
        // otherwise it means we have unfinished input
        // and can keep it as is for future
        if (this.mutableValue) {
          this.inputValue = '';
        }
        this.mutableValue = '';
        return;
      }

      const dateTime = this.valueToDateTime(this.value);

      if (dateTime.invalid) {
        const type = this.isTimestampValueType ? 'timestamp' : 'ISO 8601';
        console.error(`[${this.$options.name}] Invalid value '${this.value}', ${type} expected.`);
        this.mutableValue = '';
        this.inputValue = '';
        return;
      }

      this.mutableValue = this.value;
      this.inputValue = dateTime.toFormat(this.displayFormat);
    },
    valueToDateTime(value) {
      let dateTime;
      if (this.isTimestampValueType) {
        dateTime = DateTime.fromMillis(parseInt(value, 10));
      } else {
        dateTime = DateTime.fromISO(value);
      }

      if (dateTime.invalid) {
        return dateTime;
      }

      if (this.timezone && dateTime.zoneName !== this.timezone && this.isTimezoneValid) {
        return dateTime.setZone(this.timezone);
      }

      return dateTime;
    },
    emitInput() {
      this.$emit('input', this.mutableValue);
    },
    onFocus(e) {
      this.focused = true;
      this.$emit('focus', e);
    },
    onBlur(e) {
      this.focused = false;
      this.$emit('blur', e);
    },
  },
};
