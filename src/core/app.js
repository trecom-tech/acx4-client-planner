import 'custom-event-polyfill';
import 'es6-promise/auto';
import 'es6-object-assign/auto';
import 'es6-set/implement';
import 'es6-map/implement';
import 'is-nan/shim';
import 'array.prototype.find/shim';
import 'array.prototype.findindex/shim';
import 'array-includes/shim';
import 'element.prototype.matches';
import 'svgxuse';
import Vue from 'vue';
import Meta from 'vue-meta';
import Vuelidate from 'vuelidate';
import { sync } from 'vuex-router-sync';

import '@/core/helpers/ENV';
import Confirm from '@/core/plugins/Confirm';
import Loading from '@/core/plugins/Loading';
import Toasts from '@/core/plugins/Toasts';
import App from '@/core/components/App.vue';
import { Grid, GridCol, Container } from '@/core/components/Grid';
import InProgressHolder from '@/core/components/InProgressHolder.vue';
import NoSSR from '@/core/components/NoSSR.vue';

// TODO: move to plugins directory
import ModalPlugin from '@/components/ui/Modal/plugin';

import * as filters from '@/core/filters';
import * as directives from '@/core/directives';

import { camelToKebab } from '@/core/helpers/utils';
import { createApi } from '@/core/api';
import { createStore } from '@/core/store';
import { createRouter } from '@/core/router';
import { i18n } from '@/i18n';
import initialize from '@/core/initialize';

import '@/assets/styles/main.scss'; // Global styles

function formatQuery(query) {
  return Object.keys(query)
    .sort()
    .map((key) => {
      const val = query[key];
      if (val == null) {
        return '';
      }
      if (Array.isArray(val)) {
        return val.slice().map(val2 => [key, '=', val2].join('')).join('&');
      }
      return `${key}=${val}`;
    })
    .filter(Boolean)
    .join('&');
}

function formatUrl(url, query) {
  const index = url.indexOf('://');
  let protocol;
  if (index !== -1) {
    protocol = url.substring(0, index);
    url = url.substring(index + 3);
  } else if (url.indexOf('//') === 0) {
    url = url.substring(2);
  }

  let parts = url.split('/');
  let result = (protocol ? `${protocol}://` : '//') + parts.shift();

  let path = parts.filter(Boolean).join('/');
  let hash;
  parts = path.split('#');
  if (parts.length === 2) {
    path = parts[0];
    hash = parts[1];
  }

  result += path ? `/${path}` : '';

  if (query && JSON.stringify(query) !== '{}') {
    result += (url.split('?').length === 2 ? '&' : '?') + formatQuery(query);
  }
  result += hash ? `#${hash}` : '';

  return result;
}

// Config
Vue.config.productionTip = false;
Vue.config.errorHandler = function errorHandler(err, vm, info) {
  console.error(err);
  console.error(info);
};

// Install plugins
Vue.use(Meta, { keyName: 'meta' });
Vue.use(Vuelidate);
Vue.use(Confirm);
Vue.use(Loading);
Vue.use(Toasts);
Vue.use(ModalPlugin);

// Register global components
Vue.component('Container', Container);
Vue.component('Grid', Grid);
Vue.component('GridCol', GridCol);
Vue.component('InProgressHolder', InProgressHolder);
Vue.component('NoSSR', NoSSR);

// Register global filters
Object.keys(filters).forEach((key) => {
  Vue.filter(key, filters[key]);
});

// Register global directives
Object.keys(directives).forEach((key) => {
  Vue.directive(camelToKebab(key), directives[key]);
});

export function createApp(ssrContext) {
  const context = {
    ssr: !!ssrContext,
    req: ssrContext && ssrContext.req,
    res: ssrContext && ssrContext.res,
    next: ssrContext ? ssrContext.next : location => context.router.push(location),
    redirect(status, path, query) {
      if (!status) return;
      let pathType = typeof path;

      if (typeof status !== 'number' && (pathType === 'undefined' || pathType === 'object')) {
        query = path || {};
        path = status;
        pathType = typeof path;
        status = 302;
      }

      if (pathType === 'object') {
        path = context.router.resolve(path).href;
      }

      // "/absolute/route", "./relative/route" or "../relative/route"
      if (/(^[.]{1,2}\/)|(^\/(?!\/))/.test(path)) {
        context.next({
          path,
          query,
          status,
        });
      } else {
        path = formatUrl(path, query);

        if (ssrContext) {
          context.next({
            path,
            status,
          });
          return;
        }

        window.location.replace(path);
        throw new Error('ERR_REDIRECT');
      }
    },
    setError(error) {
      if (!error) {
        context.error = null;
        return;
      }

      // TODO: need to log only Syntax and Reference errors
      if ((!error.code || error.code === 500) && error.original) {
        console.error(error.original);
      }

      context.error = {
        code: error.code || 500,
        message: error.message,
        details: error.details,
      };
    },
    createError(data) {
      const error = new Error(data.message);
      error.code = data.code;
      error.details = data.details;
      return error;
    },
    modules: {},
  };

  // SSR: Save cookies in context
  if (ssrContext && ssrContext.cookies) {
    try {
      context.cookies = JSON.parse(ssrContext.cookies);
    } catch (err) { /* nothing */ }
  }

  // Trick to start router only after app async initialization
  // https://github.com/vuejs/vue-router/issues/1377
  let initComplete;
  const initPromise = new Promise((resolve) => {
    initComplete = resolve;
  });

  const store = createStore(context);
  const api = createApi(context);
  const router = createRouter(context, initPromise);

  sync(store, router);

  const MAX_LOADING_TIME = 5000;
  const LOADING_INTERVAL = 100;
  let loadingInterval;

  const app = new Vue({
    router,
    store,
    i18n,
    data() {
      return {
        error: null,
        canRender: false,
        loading: false,
        loadingProgress: 0,
        loadingFailed: false,
      };
    },
    async created() {
      await initialize(context);
      initComplete();
    },
    mounted() {
      this.canRender = true;
    },
    methods: {
      setError(error) {
        if (!error) {
          this.error = null;
          return;
        }
        this.error = {
          code: error.code || 500,
          message: error.message,
          details: error.details,
        };
      },
      // Loading indicator
      startLoading() {
        if (loadingInterval) {
          clearInterval(loadingInterval);
        }

        this.loading = true;
        this.loadingFailed = false;
        this.loadingProgress = 0;

        const step = (LOADING_INTERVAL / MAX_LOADING_TIME) * 100;

        loadingInterval = setInterval(() => {
          this.increaseLoading(step * Math.random());
          if (this.loadingProgress > 95) {
            this.finishLoading();
          }
        }, LOADING_INTERVAL);
      },
      finishLoading() {
        this.loadingProgress = 100;
        clearInterval(loadingInterval);
        loadingInterval = null;
        setTimeout(() => {
          this.loading = false;
          Vue.nextTick(() => {
            this.loadingProgress = 0;
          });
        }, 500);
      },
      pauseLoading() {
        clearInterval(loadingInterval);
      },
      increaseLoading(percent) {
        this.loadingProgress = Math.min(100, this.loadingProgress + percent);
      },
      failLoading() {
        this.loadingFailed = true;
        this.finishLoading();
      },
    },
    render(h) {
      // Render empty App if SSR disabled for this route
      if (ssrContext && ssrContext.noSSR) {
        return h('div', {
          attrs: {
            id: 'app',
          },
        });
      }

      // TODO: check, before use: if (this.$isServer || this.canRender)
      return h(App);
    },
  });

  // Inject app in context
  context.app = app;
  app.context = context;

  // Define global access to app elements
  Vue.prototype.$app = app;
  Vue.prototype.$api = api;

  return app;
}
