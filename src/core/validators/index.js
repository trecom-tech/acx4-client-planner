import { getPathValue } from 'pathval';
import { withParams } from 'vuelidate/lib/validators/common';

export function regexp(pattern) {
  return withParams({ type: 'regexp', pattern }, value => (value ? pattern.test(value) : true));
}

export function notEqual(ref) {
  return withParams({ type: 'notEqual', ref }, (value, parentVm) => {
    const refValue = getPathValue(parentVm, ref);
    return value !== refValue;
  });
}

export function equal(ref, vm) {
  return withParams({ type: 'equal', ref }, (value, parentVm) => {
    const refValue = getPathValue(vm || parentVm, ref);
    return value === refValue;
  });
}

export function passwordStrength(value) {
  return !value || /^(?=.*[0-9])(?=.*[!@#$%^&*_])(?=.*[A-Z]).{8,}$/.test(value);
}
