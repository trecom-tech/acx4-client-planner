export default {
  INVALID_REFRESH_TOKEN: 'invalid_refresh_token',
  ACCOUNT_NOT_CONFIRMED: 'not_confirmed',
};
