import axios from 'axios';

import responseCodes from '@/core/api/responseCodes';

const REFRESH_TOKEN_BEFORE = 2 * 60 * 1000; // 2 min

export function createApi(context) {
  const store = context.store;
  const api = axios.create({
    baseURL: process.env.API_URL,
    headers: {
      'Content-Type': 'application/json',
    },
  });

  let isTokenRefreshing;
  let tokenRefreshingPromise;

  // Refresh access token if needed
  api.interceptors.request.use((config) => {
    // Do nothing if it is refresh request
    if (config._tokenRefreshing) return config;

    // Do nothing if user not authenticated
    if (!store.state.user.accessToken) return config;

    let refreshInitiator = false;

    // Refresh token if it expires soon
    if (
      !isTokenRefreshing
      && store.state.user.tokenExpires - Date.now() < REFRESH_TOKEN_BEFORE
    ) {
      refreshInitiator = true;
      isTokenRefreshing = true;
      tokenRefreshingPromise = store.dispatch('user/refreshToken');
    }

    // Wait token refreshing
    if (isTokenRefreshing) {
      return tokenRefreshingPromise
        .then(() => {
          if (refreshInitiator) {
            isTokenRefreshing = false;
          }
          return Promise.resolve(config);
        })
        .catch(() => {
          if (refreshInitiator) {
            isTokenRefreshing = false;
          }
          // Resolve anyway, allow requests to make they job
          return Promise.resolve();
        });
    }

    return config;
  }, err => Promise.reject(err));

  // Add access token to each request
  api.interceptors.request.use((config) => {
    const accessToken = store.getters['user/accessToken'];

    if (accessToken) {
      config.headers.Authorization = accessToken;
    }

    return config;
  }, err => Promise.reject(err));

  // Handle all api responses and try refresh token
  api.interceptors.response.use(response => response, (err) => {
    // Do nothing if user not authenticated
    if (!store.state.user.accessToken) return Promise.reject(err);

    const response = err && !axios.isCancel(err) && err.response;
    const unauthorized = (
      response
      && (response.status === 401 || response.data.code === responseCodes.INVALID_REFRESH_TOKEN)
    );

    if (unauthorized) {
      // Token refreshing failed, logout user
      if (err.config._tokenRefreshing) {
        store.dispatch('user/logout');
        return Promise.reject(err);
      }

      let refreshInitiator = false;

      if (!isTokenRefreshing) {
        isTokenRefreshing = true;
        refreshInitiator = true;
        tokenRefreshingPromise = store.dispatch('user/refreshToken');
      }

      // Wait before token will be refreshed and try again
      return tokenRefreshingPromise
        .then(() => {
          if (refreshInitiator) {
            isTokenRefreshing = false;
          }
          return api(err.config)
            .then(result => Promise.resolve(result))
            .catch(err => Promise.reject(err));
        })
        .catch(() => {
          if (refreshInitiator) {
            isTokenRefreshing = false;
          }
          return Promise.reject(err);
        });
    }

    return Promise.reject(err);
  });

  // Inject api in app context
  context.api = api;

  return api;
}
