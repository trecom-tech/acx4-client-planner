export default function (value, decimals = 0) {
  if (!value && value !== 0) return '';

  const number = parseFloat(value);
  if (Number.isNaN(number)) return value;
  return number.toFixed(decimals);
}
