export default function (array, length) {
  const count = parseInt(length, 10);

  if (Number.isNaN(count)) {
    return array;
  }

  if (count <= 0) {
    return [];
  }

  return array.slice(0, count);
}
