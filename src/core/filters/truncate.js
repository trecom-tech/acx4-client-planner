export default function (str, length) {
  if (!str || typeof str !== 'string') return '';

  str = str.trim();

  if (!length || str.length <= length) return str;

  str = str.substring(0, length).trim();

  return `${str}...`;
}
