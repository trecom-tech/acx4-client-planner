import URI from 'urijs';

export default function (string) {
  if (!string) return '';

  const uri = new URI(string);

  if (!uri.protocol()) {
    uri.protocol('http');
  }

  return uri.normalize().toString();
}
