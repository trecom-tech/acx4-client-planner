export default function (value, decimals = 0) {
  if (!value) return '';

  const number = parseInt(value, 10);
  if (Number.isNaN(number)) return value;

  const factor = decimals ? 10 ** decimals : 1;
  return Math.round(value * factor) / factor;
}
