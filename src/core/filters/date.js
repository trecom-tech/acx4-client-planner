import { DateTime } from 'luxon';

// dd.MM.yyyy HH:mm

export default function (value, format = 'dd.MM.yyyy', timezone) {
  if (!value) return '';

  let date = DateTime.fromJSDate(new Date(value));

  if (date.invalid) {
    return '';
  }


  if (timezone && date.zoneName !== timezone) {
    date = date.setZone(timezone);
  }

  return date.toFormat(format);
}
