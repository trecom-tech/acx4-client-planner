export function authGuard(context, next) {
  if (!context.store.getters['user/isAuthenticated']) {
    return next({
      name: 'Auth',
      query: {
        return: context.route.query.return || context.route.fullPath,
      },
    });
  }
  return next();
}

export function notAuthGuard(context, next) {
  if (context.store.getters['user/isAuthenticated']) {
    return next('/');
  }
  return next();
}
