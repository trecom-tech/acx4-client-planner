import Vue from 'vue';
import Router from 'vue-router';

import routes from '@/core/router/routes';
import { setContext } from '@/utils';

Vue.use(Router);

export function createRouter(context, promise) {
  const router = new Router({
    mode: 'history',
    linkActiveClass: '',
    linkExactActiveClass: '',
    scrollBehavior(to, from, savedPosition) {
      if (savedPosition) {
        return savedPosition;
      }

      if (to.hash && document.querySelector(to.hash)) {
        return { selector: to.hash };
      }

      return { x: 0, y: 0 };
    },
  });

  // Wait app initialization before run router
  const removeGuard = router.beforeEach(async (to, from, next) => {
    await promise;

    // Insert core routes after all modules registered
    // to use redirects by name
    router.addRoutes(routes);

    removeGuard();

    // Run navigation again because right now we have full routes list
    next(Object.assign({}, to, { replace: true }));
  });

  // Prepare to navigation: clear app render context, start loading indicator
  router.beforeEach((to, from, next) => {
    const app = context.app || { context }; // In case if app not ready yet
    setContext(app, { to, from });
    if (app.startLoading) {
      app.startLoading();
    }
    next();
  });

  // TODO: end loading afterEach?

  // Guards
  router.beforeEach(async (to, from, next) => {
    // Find guard in 'route or in parent routes
    const guards = [];

    const addGuard = (guard) => {
      if (typeof guard === 'function' && !guards.includes(guard)) {
        guards.push(guard);
      }
    };

    const promisifyGuard = (guard, context) => new Promise((resolve, reject) => {
      // Last argument - callback
      const cb = (redirect) => {
        if (redirect || redirect === false) {
          return reject(redirect);
        }

        return resolve();
      };

      const result = guard(context, cb);

      // If function returns Promise = just wrap it
      if (result instanceof Promise && typeof result.then === 'function') {
        result
          .then(resolve)
          .catch(reject);
        return;
      }

      // If function is not async
      if (typeof result !== 'undefined') {
        if (result === null || result === true) {
          resolve();
        } else {
          reject(result);
        }
      }
    });

    // Collect all guards from parent to current route
    for (let i = 0; i < to.matched.length; i++) {
      const guard = to.matched[i].meta.guard;

      if (Array.isArray(guard)) {
        guard.forEach(item => addGuard(item));
      } else {
        addGuard(guard);
      }
    }

    if (!guards.length) {
      return next();
    }

    // Recursive loop guards

    let index = 0;

    const runGuard = async () => {
      const guard = guards[index];
      let result;

      try {
        result = await promisifyGuard(guard, context);
      } catch (err) {
        result = err;
      }

      const redirect = result && (typeof result === 'string' || typeof result === 'object');

      if (result === false) {
        context.app.failLoading();
      }

      // Redirect to the current page
      if (redirect && router.currentRoute.fullPath === router.match(result).fullPath) {
        context.app.finishLoading();
      }

      // Run router hook callback after last guard or if guard returns redirect / abort
      if (redirect || result === false || index === guards.length - 1) {
        return next(result);
      }

      index++;
      return runGuard();
    };

    return runGuard();
  });

  router.onError((err) => {
    router.context.app.setError(err);
    router.context.app.failLoading();
  });

  // Inject router in app context
  context.router = router;
  router.context = context;

  return router;
}
