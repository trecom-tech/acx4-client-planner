import Vue from 'vue';

export function applyAsyncData(Component, asyncData) {
  const noopData = () => ({});

  const componentData = Component.options.data || noopData;

  // Prevent calling this method for each request on SSR context
  if (!componentData && Component.options.hasAsyncData) {
    return;
  }

  Component.options.hasAsyncData = true;
  /* eslint-disable-next-line func-names */
  Component.options.data = function () {
    const data = componentData.call(this);

    // TODO: move out! (to the server entry)
    if (this.$ssrContext) {
      asyncData = this.$ssrContext.asyncData[Component.cid];
    }

    return { ...data, ...asyncData };
  };

  if (Component._Ctor && Component._Ctor.options) {
    Component._Ctor.options.data = Component.options.data;
  }
}

export function getMatchedComponents(route) {
  const components = (
    route.matched.map(m => Object.keys(m.components).map(key => m.components[key]))
  );
  return Array.prototype.concat.apply([], components);
}

export function getMatchedComponentsInstances(route) {
  const instances = (
    route.matched.map(m => Object.keys(m.instances).map(key => m.instances[key]))
  );

  return Array.prototype.concat.apply([], instances);
}

export function flatMapComponents(route, fn) {
  const components = (
    route.matched.map((m, index) => Object.keys(m.components).reduce((promises, key) => {
      if (m.components[key]) {
        promises.push(fn(m.components[key], m, key, index));
      } else {
        delete m.components[key];
      }
      return promises;
    }, []))
  );
  return Array.prototype.concat.apply([], components);
}

export function sanitizeComponent(Component) {
  if (Component.options && Component._Ctor === Component) {
    return Component;
  }

  if (!Component.options) {
    Component = Vue.extend(Component);
  }

  Component._Ctor = Component;

  return Component;
}

export function setContext(app, context = {}) {
  app.context.from = context.from;
  app.context.to = context.to;
  app.context.route = context.to;
  app.context.next = context.next;
  app.context.error = context.error;

  return app.context;
}

export function resolveComponents(route) {
  return Promise.all(flatMapComponents(route, async (Component, m, key) => {
    if (typeof Component === 'function' && !Component.options) {
      Component = await Component();
    }

    m.components[key] = sanitizeComponent(Component);

    return m.components[key];
  }));
}
