import Vue from 'vue';
import { compile } from 'path-to-regexp';

import { createApp } from '@/core/app';
import {
  applyAsyncData,
  getMatchedComponents,
  flatMapComponents,
  resolveComponents,
  sanitizeComponent,
  setContext,
} from '@/utils';

const app = createApp();
const router = app.$router;
const STATE = window.__INITIAL_STATE__ || {};

let pathChanged;
let queryChanged;
let queryDiff;
let lastPaths;
let renderPath;

function getPath(base, mode) {
  let path = window.location.pathname;

  if (mode === 'hash') {
    return window.location.hash.replace(/^#\//, '');
  }

  if (base && path.indexOf(base) === 0) {
    path = path.slice(base.length);
  }

  return (path || '/') + window.location.search + window.location.hash;
}

function getQueryDiff(toQuery, fromQuery) {
  const diff = {};
  const queries = { ...toQuery, ...fromQuery };

  Object.keys(queries).forEach((key) => {
    if (String(toQuery[key]) !== String(fromQuery[key])) {
      diff[key] = true;
    }
  });

  return diff;
}

function normalizeComponents(route) {
  if (app.context.error) return; // Skip if error page rendered
  flatMapComponents(route, (Component, match, key) => {
    if (typeof Component === 'object' && !Component.options) {
      Component = Vue.extend(Component);
      Component._Ctor = Component;
      match.components[key] = Component;
    }
    return Component;
  });
}

function waitRouter() {
  return new Promise((resolve, reject) => {
    router.onReady(resolve, reject);
  });
}

async function resolveInitialComponents() {
  const path = getPath(router.options.base, router.options.mode);
  const route = router.match(path);
  const components = await Promise.all(flatMapComponents(route, async (Component, m, key, i) => {
    // load async component
    if (typeof Component === 'function' && !Component.options) {
      Component = await Component();
    }

    Component = sanitizeComponent(Component);

    // apply async data if SSR used
    if (STATE.serverRendered && STATE.data && STATE.data[i]) {
      Component = applyAsyncData(Component, STATE.data[i]);
      Component._Ctor = Component;
    }

    m.components[key] = Component;
    return Component;
  }));
  return components;
}

async function loadComponents(to, from, next) {
  try {
    pathChanged = !!app.context.error || from.path !== to.path;
    queryChanged = JSON.stringify(to.query) !== JSON.stringify(from.query);
    queryDiff = queryChanged ? getQueryDiff(to.query, from.query) : [];

    if (pathChanged && !app.loading) {
      app.startLoading();
    }

    const components = await resolveComponents(to);

    // Break render chain if another render started
    if (renderPath !== to.fullPath) {
      return next(false);
    }

    if (!pathChanged && queryChanged) {
      const startLoading = components.some((Component) => {
        const watchQuery = Component.options.watchQuery;
        if (watchQuery === true) return true;
        if (Array.isArray(watchQuery)) {
          return watchQuery.some(key => queryDiff[key]);
        }
        return false;
      });

      if (startLoading && !app.loading) {
        app.startLoading();
      }
    }
    return next();
  } catch (err) {
    if (renderPath !== to.fullPath) {
      return next(false);
    }

    app.context.setError(err); // set error for hooks after
    return next(); // anyway go to the route and render error page
  }
}

async function render(to, from, next) {
  if (renderPath !== to.fullPath) {
    return next(false);
  }

  // If error - render error page
  if (app.context.error) {
    app.setError(app.context.error);
    // app.context.setError(null);
    app.failLoading();
    return next();
  }

  if (to === from) {
    lastPaths = [];
  } else {
    const matched = getMatchedComponents(from);
    lastPaths = matched.map((Component, i) => compile(from.matched[i].path)(from.params));
  }

  let nextCalled = false;

  setContext(app, {
    to,
    from,
    next: (path) => {
      if (from.path === path.path) {
        app.finishLoading();
      }

      if (from.path !== path.path) {
        app.pauseLoading();
      }

      if (nextCalled) return;
      nextCalled = true;
      next(path);
    },
  });

  const components = getMatchedComponents(to);

  if (!components.length) {
    const errorData = {
      code: 404,
      message: 'Page not found',
    };
    app.context.setError(errorData);
    app.setError(errorData);
    app.failLoading();
    return next();
  }

  // Update ._data and other properties if hot reloaded
  components.forEach((Component) => {
    if (Component._Ctor && Component._Ctor.options) {
      Component.options.asyncData = Component._Ctor.options.asyncData;
    }
  });

  try {
    // Prepare components data
    await Promise.all(components.map((Component, i) => {
      if (!Component.options.asyncData || typeof Component.options.asyncData !== 'function') {
        return Promise.resolve();
      }

      const componentPath = compile(to.matched[i].path)(to.params);

      Component._dataRefresh = false;

      if ((pathChanged && queryChanged) || componentPath !== lastPaths[i]) {
        Component._dataRefresh = true;
      } else if (!pathChanged && queryChanged) {
        const watchQuery = Component.options.watchQuery;

        if (watchQuery === true) {
          Component._dataRefresh = true;
        }

        if (Array.isArray(watchQuery)) {
          Component._dataRefresh = watchQuery.some(key => queryDiff[key]);
        }
      }

      if (!app.context.error && this._isMounted && !Component._dataRefresh) {
        return Promise.resolve();
      }

      return Component.options.asyncData(app.context).then((result) => {
        applyAsyncData(Component, result);
        app.increaseLoading(20);
      });
    }));

    if (renderPath !== to.fullPath) {
      return next(false);
    }

    // Wait async layout loading
    const layout = to.meta.layout;
    if (layout && typeof layout === 'function') {
      await layout();
    }

    if (nextCalled || renderPath !== to.fullPath) {
      return next(false);
    }

    app.setError(null); // clear previous error and render page
    app.finishLoading();
    return next();
  } catch (err) {
    if (nextCalled || renderPath !== to.fullPath) {
      return next(false);
    }

    const error = err || {};

    const errorData = {
      code: error.code || (error.response && error.response.status) || 500,
      message: error.message,
      details: error.details,
      original: err,
    };

    app.context.setError(errorData); // set error for hooks after
    app.setError(errorData); // render error page
    app.failLoading();
    return next();
  }
}

async function mountApp(app) {
  const mount = () => {
    app.$mount('#app');
  };

  await waitRouter(); // TODO: catch possible error

  // Add router hooks
  router.beforeResolve((to, from, next) => {
    renderPath = to.fullPath;
    next();
  });
  router.beforeResolve(loadComponents.bind(app));
  router.beforeResolve(render.bind(app));
  router.afterEach(normalizeComponents.bind(app));

  // SSR returned error page - just mount
  if (STATE.serverRendered && STATE.error) {
    app.setError(STATE.error);
    mount();
    return;
  }

  // Resolve components and fill with ssr data
  if (!app.context.error) {
    await resolveInitialComponents();
  }

  // Mount already rendered app
  if (STATE.serverRendered) {
    mount();
    return;
  }

  // Render app first time
  renderPath = router.currentRoute.fullPath;
  render.call(app, router.currentRoute, router.currentRoute, (path) => {
    // If redirected
    if (path) {
      return router.push(path, () => mount(), (err) => {
        console.error(err);
        if (!err) {
          mount();
        }
      });
    }

    normalizeComponents(router.currentRoute);
    return mount();
  });
}

mountApp(app).catch((err) => {
  if (err.message === 'ERR_REDIRECT') {
    return; // Wait for browser to redirect...
  }

  console.error('Error while initializing app', err);
});

// TODO: Wait nuxt update and implement serverPrefetch
// https://github.com/nuxt/nuxt.js/pull/5118
// https://github.com/nuxt/rfcs/issues/27
// TODO: validateRoute()
// TODO: thrown error if redirect loop
