export default {
  connectionStatus(state) {
    return state.connectionStatus;
  },

  notifications(state) {
    return state.notifications;
  },

  unreadNotifications() {
    return [];
  },
};
