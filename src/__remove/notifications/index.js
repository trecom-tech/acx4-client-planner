import mutations from './mutations';
import actions from './actions';
import getters from './getters';

export default {
  state: () => ({
    notifications: [],
    connectionStatus: null, // CONNECTING, CONNECTED, ERROR
  }),
  actions,
  getters,
  mutations,
  namespaced: true,
};
