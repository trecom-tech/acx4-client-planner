import * as types from '../../mutation-types';

export default {
  [types.SET_NOTIFICATION_CONNECTION_STATUS](state, status) {
    state.connectionStatus = status;
  },
};
