import * as types from '@/store/mutation-types';

export default {
  setConnectionState({ state, commit }, status) {
    if (state.connectionStatus !== status) {
      commit(types.SET_NOTIFICATION_CONNECTION_STATUS, status);
    }
  },
};
