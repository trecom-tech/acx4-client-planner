export default {
  props: {
    value: {
      type: null,
      default: () => ([]),
    },
    multiple: {
      type: Boolean,
      default: false,
    },
    trackBy: {
      type: String,
      default: '',
    },
    label: {
      type: String,
      default: '',
    },
    customLabel: {
      type: Function,
      default(option, label) {
        if (
          (!option && option !== 0)
          || (Array.isArray(option) && option.length === 0)
        ) {
          return '';
        }

        return label ? option[label] : option;
      },
    },
    max: {
      type: [Number, Boolean],
      default: false,
    },
    optionsLimit: {
      type: Number,
      default: 1000,
    },
    limit: {
      type: Number,
      default: 999,
    },
    maxHeight: {
      type: Number,
      default: 240,
    },
    // Behaivor
    clearOnSelect: {
      type: Boolean,
      default: true,
    },
    hideSelected: {
      type: Boolean,
      default: false,
    },
    allowEmpty: {
      type: Boolean,
      default: true,
    },
    closeOnSelect: {
      type: Boolean,
      default: true,
    },
    preselectFirst: {
      type: Boolean,
      default: false,
    },
    blockKeys: {
      type: Array,
      default: () => ([]),
    },
    // Tags
    taggable: {
      type: Boolean,
      default: false,
    },
    tagPlaceholder: {
      type: String,
      default: 'Press enter to create a tag',
    },
    tagPosition: {
      type: String,
      default: 'top',
    },
    // Groups
    groupValues: {
      type: String,
      default: '',
    },
    groupLabel: {
      type: String,
      default: '',
    },
    groupSelect: {
      type: Boolean,
      default: false,
    },
    // Pointer
    showPointer: {
      type: Boolean,
      default: true,
    },
    optionHeight: {
      type: Number,
      default: 40,
    },
  },
};
