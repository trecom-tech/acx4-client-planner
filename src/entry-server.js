import Vue from 'vue';
import { stringify } from 'query-string';
import sprite from 'svg-sprite-loader/runtime/sprite.build'; // eslint-disable-line

import { createApp } from '@/App';
import {
  applyAsyncData,
  getMatchedComponents,
  promisify,
  urlJoin,
} from '@/utils';

const noopApp = () => new Vue({ render: h => h('div') });

const createNext = ssrContext => (opts) => {
  ssrContext.redirected = opts;
  opts.query = stringify(opts.query);
  opts.path += opts.query ? `?${opts.query}` : '';

  const routerBase = process.env.ROUTER_BASE || '/';

  if (
    opts.path.indexOf('http') !== 0
    && (routerBase !== '/' && opts.path.indexOf(routerBase) !== 0)
  ) {
    opts.path = urlJoin(routerBase, opts.path);
  }

  // Avoid loop redirect
  if (opts.path === ssrContext.url) {
    ssrContext.redirected = false;
    return;
  }

  ssrContext.res.writeHead(opts.status, {
    Location: opts.path,
  });
  ssrContext.res.end();
};

export default async (ssrContext) => {
  ssrContext.next = createNext(ssrContext);
  ssrContext.state.serverRendered = true;

  const { app, store } = await createApp(ssrContext);

  // Setup meta
  const meta = app.$meta();
  ssrContext.META = () => {
    const m = meta.inject();
    return {
      HTML_ATTRS: `${m.htmlAttrs.text()} data-vue-meta-server-rendered`,
      HEAD: `
        ${m.meta.text()}
        ${m.title.text()}
        ${m.link.text()}
        ${m.style.text()}
        ${m.script.text()}
        ${m.noscript.text()}
        <base href="${process.env.ROUTER_BASE}">
      `,
      ICONS: sprite.stringify(),
      BODY: `
        ${m.script.text({ body: true })}
        ${m.noscript.text({ body: true })}
      `,
    };
  };

  const renderErrorPage = async () => {
    await app.loadLayout('error');
    app.setLayout('error');
    ssrContext.state.layout = 'error';
    return app;
  };

  const render404Page = async () => {
    app.setError({ statusCode: 404, message: 'Page not found' });
    return renderErrorPage();
  };

  const route = app.context.route;

  if (!route.matched.length) return render404Page();

  // Redirect in router
  if (route.fullPath !== ssrContext.url) {
    app.context.redirect(route.fullPath);
    return noopApp();
  }

  // Components are already resolved by setAppContext()
  const Components = getMatchedComponents(route);

  if (!Components.length) return render404Page();

  // Call validateRoute() on components matched by the route
  let isValid = true;
  Components.forEach((Component) => {
    if (!isValid) return;

    if (typeof Component.options.validateRoute !== 'function') return;

    isValid = Component.options.validateRoute({
      params: route.params || {},
      query: route.query || {},
    });
  });
  if (!isValid) return render404Page();

  // SSR disabled for this route - skip asyncData / asyncFetch and inject minimal meta
  if (route.meta.noSSR) {
    ssrContext.noSSR = true;
    ssrContext.state = {};
    ssrContext.META = () => ({
      HTML_ATTRS: '',
      HEAD: `<title>${process.env.DEFAULT_TITLE}</title><base href="${process.env.ROUTER_BASE}">`,
      ICONS: '',
      BODY: '',
    });
    return app;
  }

  // Set layout
  let layout = Components[0].options.layout;
  if (typeof layout === 'function') {
    layout = layout();
  }
  await app.loadLayout(layout);
  app.setLayout(layout);
  ssrContext.state.layout = layout;

  ssrContext.state.vuex = store.state;

  // // Keep asyncData for each matched component in ssrContext (used in utils/applyAsyncData)
  ssrContext.asyncData = {};

  const asyncData = await Promise.all(Components.map((Component) => {
    const promises = [];

    if (Component.options.asyncData && typeof Component.options.asyncData === 'function') {
      const promise = promisify(Component.options.asyncData, app.context);

      promise.then((asyncDataResult) => {
        ssrContext.asyncData[Component.cid] = asyncDataResult;
        applyAsyncData(Component, asyncDataResult);
        return asyncDataResult;
      });
      promises.push(promise);
    }

    if (Component.options.asyncFetch && typeof Component.options.asyncFetch === 'function') {
      const promise = promisify(Component.options.asyncFetch, app.context);
      promises.push(promise);
    }

    return Promise.all(promises);
  }));

  ssrContext.state.data = asyncData ? asyncData.map(d => d[0] || {}) : [];

  if (ssrContext.redirected) return noopApp();
  if (ssrContext.state.error) return renderErrorPage();

  return app;
};
