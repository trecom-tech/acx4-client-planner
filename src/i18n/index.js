import Vue from 'vue';
import VueI18n from 'vue-i18n';

import en from './en';

Vue.use(VueI18n);

const loadedLanguages = ['en'];

export const i18n = new VueI18n({
  locale: loadedLanguages[0],
  fallbackLocale: loadedLanguages[0],
  messages: { en },
});

export function setI18nLanguage(lang) {
  return new Promise((resolve, reject) => {
    if (i18n.locale === lang) {
      return resolve(lang);
    }

    if (loadedLanguages.includes(lang)) {
      i18n.locale = lang;
      return resolve(lang);
    }

    return import(/* webpackChunkName: "lang/[request]" */ `@/i18n/${lang}`).then((msgs) => {
      i18n.setLocaleMessage(lang, msgs.default);
      loadedLanguages.push(lang);
      i18n.locale = lang;
      resolve(lang);
    }).catch((err) => {
      reject(err);
    });
  });
}

export const languages = {
  en: {
    name: 'English',
    dir: 'ltr',
  },
};
