import UOM from '@/core/constants/uom';

export default {
  // TODO: refactore
  controls: {
    activate: 'Activate',
    cancel: 'Cancel',
    close: 'Close',
    deactivate: 'Deactivate',
    delete: 'Delete',
    edit: 'Edit',
    ok: 'Ok',
    remove: 'Remove',
    reject: 'Reject',
    save: 'Save',
    send: 'Send',
  },
  common: {
    copyright: '© 2019. AxionCore Technologies',
  },
  forms: {
    labels: {},
    placeholders: {},
    validations: {
      required: 'Required field',
      email: 'Must be a valid e-mail',
      passwordStrength: 'Password must contain a minimum of 8 characters, one capital letter, one number and one special character',
    },
  },
  entities: {
    customers: {
      askDelete: 'Delete customer {name}?',
    },
    navigation: {
      account: {
        billing: 'Billing',
        invitations: 'Invitations',
        settings: 'Settings',
        users: 'Users',
        warehouses: 'Warehouses',
      },
      dashboard: {
        root: 'Dashboard',
      },
      hauler: {
        root: 'Hauler',
        carriers: 'Carriers',
        jobs: 'Carrier jobs',
        planner: 'Route planner',
        shipments: 'Shipments',
      },
      logistic: {
        root: 'Logistics',
        carriers: 'Carriers',
        jobs: 'Carrier jobs',
        planner: 'Route planner',
        shipments: 'Shipments',
      },
      purchasing: {
        root: 'Purchasing',
        orders: 'Purchase orders',
        suppliers: 'Suppliers',
      },
      sales: {
        root: 'Sales',
        customers: 'Customers',
        orders: 'Sale orders',
        prices: 'Prices',
        import: 'Import data',
      },
      warehouse: {
        root: 'Warehouse',
        dashboard: 'Dashboard',
        inbounds: 'Inbounds',
        import: 'Import data',
        loading: 'Loading bay',
        picks: 'Picks',
        products: 'Products',
        putaways: 'Putaways',
        stock: 'Stock',
        zones: 'Zones',
      },
      user: {
        root: 'User',
        settings: 'Settings',
      },
    },
    products: {
      askDelete: 'Delete product {sku}?',
    },
    salesOrders: {
      askDelete: 'Delete order {orderNumber}?',
    },
    warehouses: {
      askDelete: 'Delete warehouse {name}?',
    },
  },
  components: {
    DataTable: {
      empty: 'No items to display',
    },
    FieldLocation: {
      emptySearch: 'Type to start search',
      geocoderError: 'Address geocoding failed',
      noResult: 'Location not found',
      searchMinLength: 'Input at least {length} characters',
    },
    FormAccountInvitation: {
      labels: {
        partnerType: 'Partner type',
        contact: {
          name: 'Partner name',
          type: 'Send invite via',
          email: 'Email',
          phone: 'Phone',
        },
        note: 'Message to partner',
      },
    },
    FormLogin: {
      controls: {
        submit: 'Login',
      },
      labels: {
        password: 'Password',
        username: 'E-mail',
      },
      unconfirmedAccount: 'Account is not confirmed. Activate your account by clicking on the link we sent to your email.',
    },
    FormPurchasingSupplier: {
      fieldsets: {
        addresses: 'Addresses',
        contacts: 'Contacts',
      },
    },
    FormPurchasingSupplierAddress: {
      labels: {
        address: 'Address',
        country: 'Country (select to limit search results)',
        default: 'Use as default',
        type: {
          billing: 'Billing',
          shipping: 'Shipping',
        },
      },
    },
    FormPurchasingSupplierContact: {
      addContact: 'Add contact',
      fieldsets: {
        contacts: 'Contacts',
        person: 'Person',
      },
      labels: {
        contacts: {
          label: 'Note',
          type: 'Type',
          value: 'Value',
        },
        default: 'Use as default',
        firstName: 'First name',
        jobTitle: 'Job title',
        lastName: 'Last name',
      },
    },
    FormResetPassword: {
      controls: {
        submit: 'Continue',
      },
      labels: {
        username: 'E-mail',
        newPassword: 'New password',
        confirmationCode: 'Confirmation code',
      },
    },
    FormResetPasswordInitiate: {
      controls: {
        submit: 'Continue',
      },
      labels: {
        username: 'E-mail',
      },
    },
    FormSalesCustomer: {
      fieldsets: {
        addresses: 'Addresses',
        contacts: 'Contacts',
      },
    },
    FormSalesCustomerAddress: {
      labels: {
        address: 'Address',
        country: 'Country (select to limit search results)',
        default: 'Use as default',
        type: {
          billing: 'Billing',
          shipping: 'Shipping',
        },
      },
    },
    FormSalesCustomerContact: {
      addContact: 'Add contact',
      fieldsets: {
        contacts: 'Contacts',
        person: 'Person',
      },
      labels: {
        contacts: {
          label: 'Note',
          type: 'Type',
          value: 'Value',
        },
        default: 'Use as default',
        firstName: 'First name',
        jobTitle: 'Job title',
        lastName: 'Last name',
      },
    },
    FormSignup: {
      controls: {
        submit: 'Signup',
      },
      labels: {
        name: 'Company name',
        password: 'Password',
        contact: {
          firstName: 'First name',
          lastName: 'Last name',
          email: 'E-mail',
        },
        confirmPassword: 'Confirm password',
        invitation: 'Invitation code',
      },
      validations: {
        confirmPassword: {
          equal: 'Passwords do not match',
        },
      },
    },
    FormWarehouse: {
      labels: {
        name: 'Name',
        address: 'Address',
        country: 'Country (select to limit search results)',
      },
    },
    FormWarehouseProduct: {
      fieldsets: {
        size: 'Size',
        codes: 'Codes',
        misc: 'Misc',
      },
    },
    FormWarehouseZone: {
      labels: {
        code: 'Code',
        name: 'Name',
        type: 'Type',
      },
    },
    Multiselect: {
      loading: 'Loading...',
      loadingError: 'An error occurred while getting options',
      noResult: 'No options found',
      noOptions: 'Options are empty',
      andMore: 'and {count} more...',
      maxElements: 'Maximum of {max} options selected',
    },
    NotificationsWidget: {
      empty: 'No notifications',
      statusText: {
        error: 'Connection error',
        connecting: 'Connecting...',
        connected: 'Connected',
      },
    },
  },
  pages: {
    account: {
      Billing: {
        meta: {
          title: 'Account - Billing',
        },
      },
      Invitations: {
        meta: {
          title: 'Invitations',
        },
      },
      Settings: {
        meta: {
          title: 'Account settings',
        },
      },
      users: {
        Create: {
          meta: {
            title: 'Create user',
          },
        },
        Edit: {
          meta: {
            title: 'Edit user {userName}',
          },
        },
        List: {
          meta: {
            title: 'Users',
          },
        },
      },
      Warehouses: {
        meta: {
          title: 'Warehouses',
        },
      },
    },
    auth: {
      Index: {
        meta: {
          title: 'Authorization',
        },
      },
      Login: {
        meta: {
          title: 'Login',
        },
      },
      Recover: {
        meta: {
          title: 'Reset password',
        },
        successMessage: 'Your password changed successfully. Please, login with new password.',
      },
      RecoverInitiate: {
        meta: {
          title: 'Reset password',
        },
      },
      Signup: {
        meta: {
          title: 'Signup',
        },
        successMessage: 'Activate your account by clicking on the link we sent to your email.',
      },
    },
    Dashboard: {
      meta: {
        title: 'Dashboard',
      },
    },
    hauler: {
      drivers: {
        Create: {
          meta: {
            title: 'Create driver',
          },
        },
        Edit: {
          meta: {
            title: 'Edit driver {name}',
          },
        },
        List: {
          meta: {
            title: 'Drivers',
          },
        },
      },
      trucks: {
        Create: {
          meta: {
            title: 'Create truck',
          },
        },
        Edit: {
          meta: {
            title: 'Edit truck {name}',
          },
        },
        List: {
          meta: {
            title: 'Trucks',
          },
        },
      },
    },
    logistic: {
      Index: {
        meta: {
          title: 'Logistic',
        },
      },
      carriers: {
        Create: {
          meta: {
            title: 'Create carrier',
          },
        },
        Edit: {
          meta: {
            title: 'Edit carrier {name}',
          },
        },
        List: {
          meta: {
            title: 'Carriers',
          },
        },
      },
      jobs: {
        Create: {
          meta: {
            title: 'Create carrier job',
          },
        },
        Edit: {
          meta: {
            title: 'Edit carrier job {number}',
          },
        },
        List: {
          meta: {
            title: 'Carrier jobs',
          },
        },
      },
      RoutePlanner: {
        meta: {
          title: 'Route planner',
        },
      },
      shipments: {
        Create: {
          meta: {
            title: 'Create shipment',
          },
        },
        Edit: {
          meta: {
            title: 'Edit shipment {number}',
          },
        },
        List: {
          meta: {
            title: 'Shipments',
          },
        },
      },
    },
    purchasing: {
      orders: {
        Create: {
          meta: {
            title: 'Create purchase order',
          },
        },
        Edit: {
          meta: {
            title: 'Edit purchase order {number}',
          },
        },
        List: {
          meta: {
            title: 'Purchase orders',
          },
        },
      },
      suppliers: {
        Create: {
          meta: {
            title: 'Create supplier',
          },
        },
        Edit: {
          meta: {
            title: 'Edit supplier {name}',
          },
        },
        List: {
          meta: {
            title: 'Suppliers',
          },
        },
      },
    },
    sales: {
      customers: {
        Create: {
          meta: {
            title: 'Create customer',
          },
        },
        Edit: {
          meta: {
            title: 'Edit customer {name}',
          },
        },
        List: {
          meta: {
            title: 'Customers',
          },
        },
      },
      orders: {
        Create: {
          meta: {
            title: 'Create sale order',
          },
        },
        Edit: {
          meta: {
            title: 'Edit sale order {number}',
          },
        },
        List: {
          meta: {
            title: 'Sale orders',
          },
        },
      },
      prices: {
        Edit: {
          meta: {
            title: 'Edit prices for {productName}',
          },
        },
        List: {
          meta: {
            title: 'Prices',
          },
        },
      },
    },
    user: {
      Index: {
        meta: {
          title: 'User',
        },
      },
      Settings: {
        meta: {
          title: 'User settings',
        },
      },
    },
    warehouse: {
      Index: {
        meta: {
          title: 'Warehouse',
        },
      },
      inbounds: {
        Create: {
          meta: {
            title: 'Create inbound delivery',
          },
        },
        Edit: {
          meta: {
            title: 'Edit inbound delivery {number}',
          },
        },
        List: {
          meta: {
            title: 'Inbound deliveries',
          },
          labels: {
            search: '',
          },
        },
        Receive: {
          meta: {
            title: 'Receive inbound delivery {number}',
          },
        },
      },
      loading: {
        Edit: {
          meta: {
            title: 'Loading list for carrier job {number}',
          },
        },
        List: {
          meta: {
            title: 'Loading bay',
          },
          labels: {
            search: '',
          },
        },
      },
      picks: {
        Edit: {
          meta: {
            title: 'Edit pick job {number}',
          },
        },
        List: {
          meta: {
            title: 'Pick jobs',
          },
        },
        Manage: {
          meta: {
            title: 'Manage pick job {number}',
          },
        },
      },
      products: {
        Create: {
          meta: {
            title: 'Create product',
          },
        },
        Edit: {
          meta: {
            title: 'Edit product {sku}',
          },
        },
        List: {
          meta: {
            title: 'Products',
          },
          labels: {
            search: '',
          },
        },
        Stock: {
          meta: {
            title: 'Product stock {sku}',
          },
        },
      },
      putaways: {
        Edit: {
          meta: {
            title: 'Edit putaway job {number}',
          },
        },
        List: {
          meta: {
            title: 'Putaway jobs',
          },
        },
        Manage: {
          meta: {
            title: 'Manage putaway job {number}',
          },
        },
      },
    },
  },
  uom: {
    [UOM.CENTIMETER]: 'Centimeter',
    [UOM.CUBIC_CENTIMETER]: 'Cubic Centimeter',
    [UOM.CUBIC_FOOT]: 'Cubic Foot',
    [UOM.CUBIC_INCH]: 'Cubic Inch',
    [UOM.CUBIC_METER]: 'Cubic Meter',
    [UOM.CUBIC_YARD]: 'Cubic Yard',
    [UOM.FOOT]: 'Foot',
    [UOM.GRAM]: 'Gram',
    [UOM.INCH]: 'Inch',
    [UOM.KILOGRAM]: 'Kilogram',
    [UOM.KILOMETER]: 'Kilometer',
    [UOM.LITER]: 'Liter',
    [UOM.METER]: 'Meter',
    [UOM.METRIC_TON]: 'Metric Ton',
    [UOM.MILE]: 'Mile',
    [UOM.OUNCE]: 'Ounce',
    [UOM.POUND]: 'Pound',
    [UOM.YARD]: 'Yard',
  },
};
